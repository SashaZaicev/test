import React, {ReactNode} from 'react'
import Head from 'next/head'
import style from '../../styles/MainContainer.module.scss'
import Header from '../Header'
import Footer from '../Footer'

const MainContainer: React.FC<MainLayoutProps> = ({
                                                      children,
                                                      title,
                                                      description,
                                                      keywords,
                                                  }) => (
    <>
        <Head>
            <title>{title || 'HeadTitle'}</title>
        </Head>
        <div className={style.mainContainer}>
            <Header/>
            <main className={style.content}>
                {children}
            </main>
            <Footer/>
        </div>
    </>
)
export default MainContainer

interface MainLayoutProps {
    title?: string
    description?: string
    keywords?: string
    children?: any
}

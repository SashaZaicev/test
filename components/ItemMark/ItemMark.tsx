import React, {ReactNode} from 'react';
import style from "../../styles/ItemMark.module.scss";
import cn from "classnames";

type ItemMarkTypes = {
    classNames: string
    children: ReactNode
}

const ItemMark = ({classNames, children}) => {
    return (
        <div>
            <div className={style.notes}>
                {/*{newItem && <div className={cn(style[type] style.newBlock)}>{children}</div>}*/}
                {/*{recipe && <div className={cn(style.notes_el, style.recipe)}>{t('sliders:recipe')}</div>}*/}
                {/*{child && <div className={cn(style.notes_el, style.children)}>{t('sliders:children')}</div>}*/}
            </div>
        </div>
    );
};

export default ItemMark;

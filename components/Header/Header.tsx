import React, {useEffect, useState} from 'react'
import Image from 'next/image'
import {useRouter} from "next/router";
import cn from "classnames";
import useTranslation from "next-translate/useTranslation";

import styles from '../../styles/Header.module.scss'
import Logo from '../Logo'
import LanguageSelect from '../LanguageSelect'
import NavBar from '../NavBar'
import Button from '../Button'
import CustomLink from "../CustomLink";
import SearchField from "../SearchField/SearchField";
import SearchResult from "../SearchResult/SearchResult";
import Search from '../../public/icons/search.svg'
import OpenMenu from '../../public/icons/mobMenu.svg'
import CloseMenu from '../../public/icons/closeMenu.svg'
import Phone from "../../public/icons/phone.svg";
import {setCallbackForm} from "../../store/ducks/common/reducer";
import {useAppDispatch} from "../../store/hooks";

const Header = () => {
  const [burgerMenu, setBurgerMenu] = useState(false)
  const [findMenu, setFindMenu] = useState(false)
  const [searchMenu, setSearchMenu] = useState(false)
  const [result, setResult] = useState(false)
  const [activeBtn, setActiveBtn] = useState("ru")
  const router = useRouter()
  const {t} = useTranslation('')
  const dispatch = useAppDispatch()

  const changeBurger = () => {
    setBurgerMenu(!burgerMenu)
    if (!burgerMenu) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflowY = 'scroll'
    }
  }
  const changeFind = () => {
    setFindMenu(!findMenu)
    if (!findMenu) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflowY = 'scroll'
    }
  }
  const filterHandle = () => {
    // burgerMenu
      setSearchMenu(!searchMenu)
      // : (
      //   setFindMenu(!findMenu),
      //     setBurgerMenu(false)
      // )
    // if (!findMenu) {
    //   document.body.style.overflow = 'hidden'
    // } else {
    //   document.body.style.overflowY = 'scroll'
    // }
  }
  const clickHandle = (e) => {
    console.log('e', e.keyCode === 13)
    if (e.keyCode === 13) {
      setResult(!result)
    }
  }
  useEffect(() => {
    if (searchMenu === false) {
      setResult(false)
    }
  }, [searchMenu])
  const handleSelectId = (value: string) => {
    router.push(router.asPath, '/', {locale: value})
    setActiveBtn(value)
  }

  return (
    <header className={styles.header}>
      <div className={cn(styles.container, burgerMenu && styles.fixedStyle)}>
        <Logo/>
        {findMenu || !burgerMenu &&
          <div className={styles.searchBtnBox}>
            <Button
              onClickFunc={() => filterHandle()}
              icon={
                <Search/>
              }
              className={styles.searchBtn}
            />
            <SearchField onClick={(e) => clickHandle(e)}
                         classNames={cn(styles.inputSearch, searchMenu && styles.active)}/>
            {result && <SearchResult/>}
          </div>}
        <div className={styles.centerBlock}>
          <NavBar styleClass={styles.centerBlockStyle}/>
        </div>
        <div className={styles.rightBlock}>
          <Button
            className={styles.bottleBtn}
            icon={<Image width='24' height='24' src="/icons/bottle.png" alt="symbol"/>}
          >
            <a href={'/pharmacovigilance'}> Фармаконадзор </a>
          </Button>
          <LanguageSelect/>
        </div>
        {!findMenu ?
          <div onKeyDown={changeBurger}
               onClick={changeBurger}
               className={styles.mobMenu}
          >
            {
              burgerMenu ? <CloseMenu/> : <OpenMenu/>
            }
          </div> :
          <div onKeyDown={changeFind}
               onClick={changeFind}
               className={styles.mobMenu}
          >
            {
              findMenu ? <CloseMenu/> : <OpenMenu/>
            }
          </div>
        }

        <div className={cn(styles.mobileMenu, burgerMenu && styles.mobileMenuActive)}>
          <div className={styles.topContainer}>
            <div className={cn(styles.searchBtnBox, styles.mobileActive)}>
              <Button
                icon={
                  <Search/>
                }
                className={cn(styles.searchBtn)}
              />
            </div>
            <div className={styles.languageContainer}>
              {router?.locales?.map((item) => (
                <div
                  className={cn(styles.selectItem, activeBtn === item && styles.active)}
                  key={item}
                  onClick={() => handleSelectId(item)}
                >
                  {
                    // eslint-disable-next-line no-nested-ternary
                    (item === 'ru')
                      ? <Image width='20' height='15' src="/icons/ru.png" alt="flag"
                               className={styles.flag}/>
                      : (item === 'en')
                        ? <Image width='20' height='15' src="/icons/en.png" alt="flag"
                                 className={styles.flag}/>
                        : <Image width='20' height='15' src="/icons/bel.png" alt="flag"
                                 className={styles.flag}/>
                  }
                  {item}
                </div>
              ))}</div>
          </div>
          <div className={cn(styles.mobileMenuContainer, styles.mobileActive)}>
            <div className={styles.link}>
              <CustomLink backImage={false}
                          icon={<Image width='9' height='16' src={t('menu:menuSymbol')}
                                       alt="symbol"/>}
                          className={styles.linkStyle}
                          text={t('menu:catalog-menu-title')} href="/catalog"/>
            </div>
            <div className={styles.link}>
              <CustomLink backImage={false}
                          icon={<Image width='9' height='16' src={t('menu:menuSymbol')}
                                       alt="symbol"/>}
                          className={styles.linkStyle}
                          text={t('menu:company-menu-title')} href="/company"/>
            </div>
            <div className={styles.link}>
              <CustomLink backImage={false}
                          icon={<Image width='9' height='16' src={t('menu:menuSymbol')}
                                       alt="symbol"/>}
                          className={styles.linkStyle}
                          text={t('menu:news-menu-title')} href="/news"/>
            </div>
            <div className={styles.link}>
              <CustomLink backImage={false}
                          icon={<Image width='9' height='16' src={t('menu:menuSymbol')}
                                       alt="symbol"/>}
                          className={styles.linkStyle}
                          text={t('menu:partners-menu-title')}
                          href="/partners"/>
            </div>
            <div className={styles.link}>
              <CustomLink backImage={false}
                          icon={<Image width='9' height='16' src={t('menu:menuSymbol')}
                                       alt="symbol"/>}
                          className={styles.linkStyle}
                          text={t('menu:contacts-menu-title')}
                          href="/contacts"/>
            </div>
          </div>
          <div className={styles.bottomMenuContainer}>
            <Button
              className={styles.bottleBtn}
              icon={<Image width='24' height='24' src="/icons/bottle.png" alt="symbol"/>}
            >
              <a href={'/pharmacovigilance'}> Фармаконадзор </a>
            </Button>
            <Button className={styles.btnCallback} onClickFunc={() => dispatch(setCallbackForm(true))}
                    icon={<Phone/>}>{t('footer:buttonCallBack')}
            </Button>

          </div>
        </div>
        {/*/////////////////FIND MENU*/}
        <div className={cn(styles.mobileMenu, findMenu && styles.findMenu)}>
          <div className={styles.topContainer}>
            <SearchField onClick={(e) => clickHandle(e)}
                         classNames={cn(styles.inputSearch, searchMenu && styles.active)}/>
          </div>
          {result && <SearchResult/>}
          <div className={styles.bottomMenuContainer}>
            <Button
              className={styles.bottleBtn}
              icon={<Image width='24' height='24' src="/icons/bottle.png" alt="symbol"/>}
            >
              Фармаконадзор
            </Button>
            <Button className={styles.btnCallback}
                    icon={<Phone/>}>{t('footer:buttonCallBack')}
            </Button>

          </div>
        </div>
      </div>
    </header>
  )
}

export default Header

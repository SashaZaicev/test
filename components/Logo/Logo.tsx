import Image from 'next/image'
import Link from 'next/link'
import style from '../../styles/Logo.module.scss'

const Logo = () => (
  <div className={style.blockLogo}>
    <Link href="/">
      <a className={style.logo}>
        <Image
          src="/icons/logo.svg"
          alt="ekzon logo"
          width={150}
          height={41}
          className={style.logo_img}
        />
        <div className={`${style.logoTitle}`}>
          <Image
            src="/images/inHealth.svg"
            alt="ekzon logo"
            width={119}
            height={38}
            className={style.logoTitle_title}
          />
        </div>
      </a>
    </Link>
  </div>
)

export default Logo

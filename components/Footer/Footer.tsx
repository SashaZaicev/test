import React, {useCallback, useEffect, useState} from 'react'
import cn from 'classnames'
import Link from 'next/link'
import useTranslation from 'next-translate/useTranslation'
import Image from 'next/image'
import {hasCookie} from 'cookies-next';

import styles from '../../styles/Footer.module.scss'
import Instaram from '../../public/icons/social/instagram.svg'
import FB from '../../public/icons/social/facebook.svg'
import VK from '../../public/icons/social/vk.svg'
import ODN from '../../public/icons/social/odnoklassniki.svg'
import Phone from '../../public/icons/phone.svg'
import Mail from '../../public/icons/mail.svg'
import Time from '../../public/icons/time.svg'
import Button from '../Button'
import {Modal} from "../Modal/Modal";
import ModalCallBack from "../Modals/ModalCallBack";
import {useAppDispatch, useAppSelector} from "../../store/hooks";
import {selectCallbackForm, selectCallbackSuc, selectQuizInfoSuc} from "../../store/ducks/common/selectors";
import {setCallback, setCallbackForm} from "../../store/ducks/common/reducer";

export default function Footer() {
  const {t} = useTranslation()
  const dispatch = useAppDispatch()

  const [quizInfo, setQuizInfo] = useState(false)
  const resultCallback = useAppSelector(selectCallbackSuc)
  const openCallbackForm = useAppSelector(selectCallbackForm)

  useEffect(() => {
    dispatch(setCallback(false))
    dispatch(setCallbackForm(false))
  }, [dispatch])

  useEffect(() => {
    hasCookie('QuizInfo') && setQuizInfo(true)
  }, [])

  const handleCloseIsOpenCallBack = useCallback(() => {
    dispatch(setCallbackForm(false))
    dispatch(setCallback(false))
  }, [dispatch,])

  return (
    <footer className={styles.footer}>
      <div className={styles.container}>
        <div className={styles['footer--inner']}>
          <div className={styles['footer--inner__left']}>
            <Link href="/">
              <a>
                <div className="logo-site">
                  <Image width="181" height="76" src="/icons/footerLogo.svg" alt="flag"
                         className={styles.logo}/>
                </div>
              </a>
            </Link>
            <div className={styles.nameCompany}>
              <div className={styles.nameCompany_title}>
                ОАО «Экзон»
              </div>
              <div className={styles.nameCompany_reserved}>
                <p>© 2020-2022 ОАО ЭКЗОН.</p>
                <p>Все права защищены.</p>
              </div>
            </div>
            <div className={cn(styles.social_media, styles['footer-item'])}>
              <h2 className={styles.title}>Мы в социальных сетях</h2>
              <div className={styles.footer_icons}>
                <a className={cn(styles.socialLink)} href="https://www.instagram.com" target="_blank"
                   rel="noreferrer">
                  <Instaram/>
                </a>
                <a className={cn(styles.socialLink)} href="https://vk.com" target="_blank"
                   rel="noreferrer">
                  <VK/>
                </a>
                <a className={cn(styles.socialLink)} href="https://facebook.com" target="_blank"
                   rel="noreferrer">
                  <FB/>
                </a>
                <a className={cn(styles.socialLink)} href="https://ok.ru" target="_blank"
                   rel="noreferrer">
                  <ODN/>
                </a>
              </div>
            </div>
          </div>
          <div className={cn(styles['footer-services'])}>
            <div className={cn(styles.services, styles['footer-item'])}>
              <h2 className={cn(styles.title, styles['js-footer-title'])}>
                {t('footer:company')}
              </h2>
              <ul className="footer_ul">
                <li><a href="#">{t('footer:information')}</a></li>
                <li><a href="/certificates">{t('footer:certificatesOfConformity')}</a></li>
                <li><a href="#">{t('footer:salesGeography')}</a></li>
                <li><a href="/video">{t('footer:videoFootage')}</a></li>
                <li><a href="#">{t('footer:informationForShareholders')}</a></li>
                <li><a href="#">{t('footer:anti-corruption')}</a></li>
                <li><a href="#">{t('footer:genderStatistics')}</a></li>
                <li><a href="#">{t('footer:applicant')}</a></li>
              </ul>
            </div>
            <div className={cn(styles.services, styles['footer-item'])}>
              <h2 className={cn(styles.title, styles['js-footer-title'])}>
                {t('footer:catalog')}
              </h2>
              <ul className={cn(styles.footer_ul)}>
                <li><a href="#">{t('footer:medicines')}</a></li>
                <li><a href="#">{t('footer:biologicallyActiveAdditives')}</a></li>
                <li><a href="#">{t('footer:foodProducts')}</a></li>
              </ul>
            </div>
            <div className={cn(styles.services, styles['footer-item'])}>
              <h2 className={cn(styles.title, styles['js-footer-title'])}>
                {t('footer:contacts')}
              </h2>
              <div className={styles.contactBox}>
                <ul className={cn(styles.footer_ul, styles.contacts)}>
                  <li className={styles.contactElement}>
                    <Phone/>
                    <a href="tel:+375164420002">+375 1644 2-00-02</a>
                  </li>
                  <li className={styles.contactElement}>
                    <Phone/>
                    <a href="tel:+375164420002">+375 1644 2-00-04</a>
                  </li>
                  <li className={styles.contactElement}>
                    <Mail/>
                    <a href="mailto:bm@ekzon.by">bm@ekzon.by</a>
                  </li>
                </ul>
                <ul className={cn(styles.footer_ul, styles.contacts)}>
                  <li className={cn(styles.contactElement, styles.time)}>
                    <Time/>
                    <div className={styles.time__block}>
                      <p className={styles.blockTitle}>{t('footer:workDays')}</p>
                      <p className={styles.blockDescription}>{t('footer:workTime')}</p>
                    </div>
                  </li>
                  <li className={cn(styles.contactElement, styles.time)}>
                    <Time/>
                    <div className={styles.time__block}>
                      <p className={styles.blockTitle}>{t('footer:lunch')}</p>
                      <p className={styles.blockDescription}>{t('footer:lunchTime')}</p>
                    </div>
                  </li>
                </ul>
              </div>
              <Modal isOpen={openCallbackForm} close={handleCloseIsOpenCallBack} styleModalWindow={'modalSize'}>
                <ModalCallBack/>
              </Modal>
              {
                resultCallback &&
                <Modal isOpen={resultCallback} close={handleCloseIsOpenCallBack} styleModalWindow={'modalSizeSuc'}>
                  <div className={'sucMsg'}>Заявка <span>оформлена!</span></div>
                </Modal>
              }
              <Button className={styles.btnCallback} onClickFunc={() => dispatch(setCallbackForm(true))}
                      icon={<Phone/>}>{t('footer:buttonCallBack')}</Button>
              <div className={styles.legalAddress}>
                <div className={styles.legalAddress_field}>
                  <span>
                    {t('footer:UNP')}
                    :
                    {' '}
                  </span>
                  {t('footer:UNP-Number')}
                </div>
                <div className={styles.legalAddress_field}>
                  <span>
                    {t('footer:C/A')}
                    :
                    {' '}
                  </span>
                  {t('footer:C/A-Number')}
                </div>
                <div className={styles.legalAddress_field}>
                  {t('footer:C/A-Address')}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

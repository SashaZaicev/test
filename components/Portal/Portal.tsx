import ReactDOM from 'react-dom'
import React, {useEffect, useMemo} from 'react'
import dynamic from "next/dynamic";

type PortalPropsType = {
  children?: any
}

export const Portal: React.FC<PortalPropsType> = (props) => {
  const root = useMemo(() => document.createElement('div'), [])
  useEffect(() => {
    document.body.appendChild(root)
    return () => {
      if (root) {
        document.body.removeChild(root)
      }
    }
  }, [root])
  return ReactDOM.createPortal(props.children, root)
}

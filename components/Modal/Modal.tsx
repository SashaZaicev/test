import {FC, useEffect} from 'react'
import cn from 'classnames'

import style from './Modal.module.scss'
// import {Portal} from '../Portal/Portal'
import dynamic from "next/dynamic";

type ModalPropsType = {
  close?: () => void
  isOpen: boolean
  styleModalWindow?: string
  children?: any
}
const Portal = dynamic(import('../Portal/Portal').then(mod => mod.Portal),
  {ssr: false}
)
export const Modal: FC<ModalPropsType> = ({
                                            styleModalWindow, children, close, isOpen,
                                          }) => {
  useEffect(() => {
    if (isOpen) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = ''
    }
    return () => {
      document.body.style.overflow = ''
    }
  }, [isOpen])

  return (
    <>
      {isOpen && (
        <Portal>
          <div className={cn(style.modal)}>
            {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
            <div className={style.bg} onClick={close}/>
            <div className={cn(style.content, styleModalWindow)}>
              <button type="button" onClick={close} className={style.successModalClose}>
                {CloseBTN}
              </button>
              {children}
            </div>
          </div>
        </Portal>
      )}
    </>
  )
}

const CloseBTN = (<svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M23.9062 2.8125C12.2569 2.8125 2.8125 12.2569 2.8125 23.9062C2.8125 35.5556 12.2569 45 23.9062 45C35.5556 45 45 35.5556 45 23.9062C45 12.2569 35.5556 2.8125 23.9062 2.8125ZM23.9062 42.1875C13.8262 42.1875 5.625 33.9863 5.625 23.9062C5.625 13.8262 13.8262 5.625 23.9062 5.625C33.9863 5.625 42.1875 13.8262 42.1875 23.9062C42.1875 33.9863 33.9863 42.1875 23.9062 42.1875ZM31.6912 14.1328L23.9597 21.8644L17.0269 14.1328C16.7617 13.8767 16.4064 13.7349 16.0377 13.7381C15.669 13.7413 15.3163 13.8892 15.0556 14.1499C14.7948 14.4107 14.6469 14.7634 14.6437 15.1321C14.6405 15.5008 14.7823 15.856 15.0384 16.1212L21.9741 23.8528L14.1356 31.6912C13.8795 31.9565 13.7377 32.3117 13.7409 32.6804C13.7441 33.0491 13.892 33.4018 14.1528 33.6626C14.4135 33.9233 14.7662 34.0712 15.1349 34.0744C15.5036 34.0776 15.8588 33.9358 16.1241 33.6797L23.8556 25.9481L30.7912 33.6797C31.0565 33.9358 31.4117 34.0776 31.7804 34.0744C32.1491 34.0712 32.5018 33.9233 32.7626 33.6626C33.0233 33.4018 33.1712 33.0491 33.1744 32.6804C33.1776 32.3117 33.0358 31.9565 32.7797 31.6912L25.8412 23.9597L33.6797 16.1212C33.9358 15.856 34.0776 15.5008 34.0744 15.1321C34.0712 14.7634 33.9233 14.4107 33.6626 14.1499C33.4018 13.8892 33.0491 13.7413 32.6804 13.7381C32.3117 13.7349 31.9565 13.8767 31.6912 14.1328V14.1328Z"
      fill="url(#paint0_linear_347_16365)"/>
    <defs>
      <linearGradient id="paint0_linear_347_16365" x1="2.8125" y1="23.9062" x2="45" y2="23.9062" gradientUnits="userSpaceOnUse">
        <stop stopColor="#006B54"/>
        <stop offset="1" stopColor="#18AD89"/>
      </linearGradient>
    </defs>
  </svg>
)

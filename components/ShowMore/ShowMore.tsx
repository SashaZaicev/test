import React, {FC, useEffect, useState} from 'react';
import style from '../../styles/ShowMore.module.scss'
import cn from "classnames";
import ArrowDown from '../../public/icons/moreArrow.svg'
import {element} from "prop-types";

type ShowMoreTypes = {
    title: string
    description: string
    showMoreText?: string
    classNameContainer?: string
    classNameTitle?: string
    classNameDescription?: string
}

const ShowMore: FC<ShowMoreTypes> = ({
                                         title,
                                         description,
                                         showMoreText = "Читать полностью",
                                         classNameContainer,
                                         classNameTitle,
                                         classNameDescription
                                     }) => {
    const [moreInfo, setMoreInfo] = useState(false)
    const [height, setHeight] = useState()
    const showMoreInfo = () => {
        setMoreInfo(!moreInfo)
    }

    return (
        <div className={cn(style.showMoreContainer, classNameContainer)}>
            <div className={cn(style.title, classNameTitle)}>{title}</div>
            <div className={cn(style.description, classNameDescription, moreInfo && style.showMore)}>{description}</div>
            <button type="button" onClick={() => showMoreInfo()} className={style.button}>
                {showMoreText}
                <ArrowDown/>
            </button>
        </div>
    );
};

export default ShowMore;

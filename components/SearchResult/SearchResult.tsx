import React from 'react';
import style from '../../styles/SearchResult.module.scss'
import ItemBlockSearch from "../ItemBlock/ItemBlockSearch";

const SearchResult = () => {
    const catalog = [
        {
            id: 1,
            title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
            description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
            image: '/images/slider2-1.jpg',
            newItem: true,
            recipe: true,
            child: true,
        },
        {
            id: 2,
            title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
            description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
            image: '/images/slider2-2.jpg',
            newItem: true,
            recipe: false,
            child: true,
        },
        {
            id: 3,
            title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
            description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
            image: '/images/slider2-3.jpg',
            newItem: true,
            recipe: false,
            child: true,
        }
    ]
    return (
        <div className={style.searchResultContainer}>
            {catalog?.map(good => {
                return (
                    <ItemBlockSearch
                        classNames={style.resultBlock}
                        classNamesImage={style.resultSecondBlock}
                        key={good.id}
                        newItem={good.newItem}
                        recipe={good.recipe}
                        child={good.child}
                        description={good.description}
                        title={good.title}
                        image={good.image}/>)
            })}

        </div>
    );
};

export default SearchResult;

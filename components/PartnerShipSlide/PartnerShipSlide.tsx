import React, {FC} from 'react';
import style from '../../styles/PartnerShipSlide.module.scss'

type PartnerShipSlideTypes = {
    image: string
}

const PartnerShipSlide: FC<PartnerShipSlideTypes> = ({image}) => {
    return (
        <div className={style.partnerShipSlide}>
            <img src={image} alt="`${image}`"/>
        </div>
    );
};

export default PartnerShipSlide;

import React, {FC} from 'react';
import cn from "classnames";
import style from '../../styles/ItemBlock.module.scss'
import Button from "../Button";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

type ItemBlockSearch = {
    classNames?: string
    classNamesImage?: string
    classNamesDescription?: string
    image?: string
    href?: string
    title?: string
    description?: string
    newItem?: boolean
    recipe?: boolean
    child?: boolean
    id?: any
}

const ItemBlockSearch: FC<ItemBlockSearch> = ({
                                                  classNames,
                                                  classNamesImage,
                                                  classNamesDescription,
                                                  image,
                                                  href,
                                                  title,
                                                  description,
                                                  newItem,
                                                  recipe,
                                                  child,
                                                  id
                                              }) => {
    const {t} = useTranslation('')

    return (
        <div className={cn(style.itemBlockSearch, style.container, classNames)}>
            <div className={cn(style.imageBlock, classNamesImage)}>
                <img src={image} alt='alt name'/>
            </div>
            <div className={cn(style.descriptionBlock, classNamesDescription)}>

                <div className={cn(style.notes, style.mobileNotes)}>
                    {newItem && <div className={cn(style.notes_el, style.newBlock)}>{t('sliders:new')}</div>}
                    {recipe && <div className={cn(style.notes_el, style.recipe)}>{t('sliders:recipe')}</div>}
                    {child && <div className={cn(style.notes_el, style.children)}>{t('sliders:children')}</div>}
                </div>
                <div className={style.title}>{title}</div>
                <div className={style.description}>{description}</div>
            </div>
            <Link href={href || '/catalog/[item]'} as={`/catalog/${id}`}>
                <a className={style.btnSizeBlock}>
                    <Button className={style.btnSize} type={"secondary"}>Узнать подробнее</Button>
                </a>
            </Link>
        </div>
    );
};

export default ItemBlockSearch;

import {
  FC, useCallback, useMemo, useState,
} from 'react'
import { useField, useFormikContext } from 'formik'
import cn from 'classnames'
import { IMaskInput } from 'react-imask'

import style from '../FieldFormik/FieldFormik.module.scss'

type FieldFormikProps = {
  name: string
  type?: string
  placeholder?: string
  label?: string
  disabled?: boolean
  className?: string
  mask?: string
  autocomplete?: 'off' | 'on'
}

export const FieldFormik: FC<FieldFormikProps> = ({
  autocomplete = 'on', disabled, className, name, type = 'text', label, placeholder, mask = '',
}) => {
  const [field, meta] = useField(name)
  const { setFieldValue } = useFormikContext<any>()
  const [isFocused, setIsFocused] = useState<boolean>(false)

  const isError = useMemo(() => !isFocused && meta.error && meta.touched, [isFocused, meta.error, meta.touched])

  const handleFocus = useCallback(() => {
    setIsFocused(true)
  }, [])
  const handleBlur = useCallback((e) => {
    setIsFocused(false)
    field.onBlur(e)
  }, [field])
  return (
    <div className={cn(style.wrapper, className)}>
      {/* eslint-disable-next-line jsx-a11y/label-has-for */}
      <label>
        {label && <div className={style.label}>{label}</div>}

        {mask ? (
          <IMaskInput
            onAccept={(e) => { setFieldValue(name, e) }}
            className={cn(style.input, isError && style.input_error)}
            type={type}
            placeholder={placeholder}
            onFocus={handleFocus}
            onBlur={handleBlur}
            name={name}
            mask={mask}
            value={field.value}
            disabled={disabled}
          />
        ) : (
          <input
            {...field}
            className={cn(style.input, isError && style.input_error)}
            type={type}
            placeholder={placeholder}
            onFocus={handleFocus}
            onBlur={handleBlur}
            disabled={disabled}
            autoComplete={autocomplete}
          />
        )}
        {isError && <div className={style.error}>{meta.error}</div>}
      </label>
    </div>
  )
}

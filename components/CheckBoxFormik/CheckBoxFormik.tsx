import {
  FC, ReactNode,
} from 'react'
import cn from 'classnames'

import style from '../../styles/CheckBoxFormik.module.scss'

type CheckBoxFormikProps = {
  name: string
  type?: string
  label?: string | ReactNode
  className?: string
  // value?: string | number
  value?: string | number
  onChange?: (value?: any) => void
  checked?: boolean
  disabled?: boolean
}

export const CheckBoxFormik: FC<CheckBoxFormikProps> = ({
  className, name, type = 'checkbox', label, value, onChange, checked, disabled,
}) => (
  <div className={cn(style.box, className)}>
    <label className={cn(style.custom_checkbox, disabled && style.disabled)}>
      <input
        className={style.custom_checkbox}
        type={type}
        name={name}
        value={value}
        onChange={onChange}
        checked={checked}
        disabled={disabled}
      />
      <span>
        {' '}
        {label}
      </span>
    </label>
  </div>
)

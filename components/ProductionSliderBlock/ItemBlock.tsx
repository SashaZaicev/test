import React, {FC} from 'react';
import cn from "classnames";
import style from '../../styles/ProductionSliderBlock.module.scss'
import Button from "../Button";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";

type ItemBlockTypes = {
    classNames?: string
    image?: string
    href?: string
    title?: string
    description?: string
    newItem?: boolean
    recipe?: boolean
    child?: boolean
}

const ItemBlock: FC<ItemBlockTypes> = ({
                                           classNames,
                                           image,
                                           href,
                                           title,
                                           description,
                                           newItem,
                                           recipe,
                                           child
                                       }) => {
    const {t} = useTranslation('')

    return (
        <div className={cn(style.container, classNames)}>
            <div className={style.notes}>
                {newItem && <div className={cn(style.notes_el, style.newBlock)}>{t('sliders:new')}</div>}
                {recipe && <div className={cn(style.notes_el, style.recipe)}>{t('sliders:recipe')}</div>}
                {child && <div className={cn(style.notes_el, style.children)}>{t('sliders:children')}</div>}
            </div>
            <div className={style.imageBlock}>
                <img src={image} alt='alt name'/>
            </div>
            <div className={style.title}>{title}</div>
            <div className={style.description}>{description}</div>
            <Link href={href || '/'}>
                <Button className={style.btnSize} type={"secondary"}>Узнать подробнее</Button>
            </Link>
        </div>
    );
};

export default ItemBlock;

import React, {FC, useMemo} from 'react';
import * as Yup from 'yup'
import useTranslation from "next-translate/useTranslation";
import {Form, Formik} from 'formik';
import cn from "classnames";

import styles from "../../styles/MailingList.module.scss";
import Timer from "../../public/icons/tick.svg";
import Mail from '../../public/images/mail-document.svg'
import {email} from "../../lib/regex";
import {FieldFormik} from "../FieldFormik/FieldFormik";
import Button from "../Button";
import {validateForm} from "../messages/validateForm";
import {postMailSubscribe} from "../../store/ducks/news/thunks";
import {useAppDispatch, useAppSelector} from "../../store/hooks";
import {selectSubscribe} from "../../store/ducks/news/selectors";

type MailingListTypes = {
  backImage: string
  logoImage: string
}

const MailingList: FC<MailingListTypes> = ({backImage, logoImage}) => {
  const {t} = useTranslation('validate')
  const dispatch = useAppDispatch()

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .matches(email, validateForm.email)
      .required(validateForm.required),
  })

  const initialValues = useMemo(() => ({
    emails: '',
  }), [])

  return (
    <div className={styles.mailingListContainer}>
      <div className={styles.imageBox}
           style={{backgroundImage: `linear-gradient(80.87deg, rgba(0, 107, 84, 0.3) 0%, rgba(0, 107, 84, 0) 100%), url('${backImage}')`}}
      >

        <div className={styles.imageBox__in}>
          <img src={logoImage} alt="logo"/>
        </div>
      </div>
      <Formik<typeof initialValues>
        initialValues={initialValues}
        onSubmit={({emails}) => {
          dispatch(postMailSubscribe(emails))
        }}
        validationSchema={validationSchema}
        enableReinitialize
      >
        <Form className={styles.box}>
          <div className={styles.box__top}>
            <Mail/><span className={styles.title}>Подпишитесь на нашу <span>Email-рассылку</span></span>
          </div>
          <div className={styles.box__bottom}>
            <FieldFormik
              placeholder='Укажите Ваш Email'
              name='email'
              // eslint-disable-next-line max-len
              className={cn(styles.active)}
            />
            <Button htmlType={"submit"} className={styles.btnCallback} icon={<Timer/>}>{t('news:subscribe')}</Button>

          </div>
        </Form>

      </Formik>
    </div>
  );
};

export default MailingList;

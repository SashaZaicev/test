import React, {FC, ReactNode} from 'react';
import cn from "classnames";

import style from '../InfoField/InfoField.module.scss'

const InfoField: FC<InfoFieldTypes> = ({
                                           title,
                                           icon,
                                           label,
                                           type = 'text',
                                           styleCustomTextBlock,
                                           styleCustomIcon,
                                           styleCustomContainer,
                                           styleCustomLabel,
                                           styleCustomTitle,
                                           mail,
                                           tel,
                                           link
                                       }) => {
    return (
        <>
            {
                type === 'mail' ? (
                    <div className={cn(style.infoFieldContainer, style[type], styleCustomContainer)}>
                        <div className={cn(style.infoFieldContainer_icon, styleCustomIcon)}>
                            {icon}
                        </div>
                        <main className={cn(style.infoFieldContainer_textBlock, styleCustomTextBlock)}>
                            <div
                                className={cn(style.infoFieldContainer_textBlock_label, styleCustomLabel)}>{label}</div>
                            <a
                                href={`mailto: ${mail}`}
                                className={cn(style.infoFieldContainer_textBlock_title, styleCustomTitle)}>{title}</a>
                        </main>
                    </div>
                ) : (type === 'phone') ?
                    (
                        <div className={cn(style.infoFieldContainer, style[type], styleCustomContainer)}>
                            <div className={cn(style.infoFieldContainer_icon, styleCustomIcon)}>
                                {icon}
                            </div>
                            <main className={cn(style.infoFieldContainer_textBlock, styleCustomTextBlock)}>
                                <div
                                    className={cn(style.infoFieldContainer_textBlock_label, styleCustomLabel)}>{label}</div>
                                <a
                                    href={`tel: ${tel}`}
                                    className={cn(style.infoFieldContainer_textBlock_title, styleCustomTitle)}>{title}</a>
                            </main>
                        </div>
                    )
                    : (type === 'link') ?
                        (
                            <div className={cn(style.infoFieldContainer, style[type], styleCustomContainer)}>
                                <div className={cn(style.infoFieldContainer_icon, styleCustomIcon)}>
                                    {icon}
                                </div>
                                <main className={cn(style.infoFieldContainer_textBlock, styleCustomTextBlock)}>
                                    <div
                                        className={cn(style.infoFieldContainer_textBlock_label, styleCustomLabel)}>{label}</div>
                                    <a target={'_blank'}
                                       href={`${link}`}
                                       className={cn(style.infoFieldContainer_textBlock_title, styleCustomTitle)}>{title}</a>
                                </main>
                            </div>
                        ) : (
                            <div className={cn(style.infoFieldContainer, style[type], styleCustomContainer)}>
                                <div className={cn(style.infoFieldContainer_icon, styleCustomIcon)}>
                                    {icon}
                                </div>
                                <main className={cn(style.infoFieldContainer_textBlock, styleCustomTextBlock)}>
                                    <div
                                        className={cn(style.infoFieldContainer_textBlock_label, styleCustomLabel)}>{label}</div>
                                    <div
                                        className={cn(style.infoFieldContainer_textBlock_title, styleCustomTitle)}>{title}</div>
                                </main>
                            </div>
                        )
            }
        </>
    )
        ;
};

export default InfoField;

type InfoFieldTypes = {
    title: ReactNode
    label?: string
    icon?: ReactNode
    styleCustomLabel?: string
    styleCustomTitle?: string
    styleCustomTextBlock?: string
    styleCustomContainer?: string
    styleCustomIcon?: string
    type?: 'mail' | 'phone' | 'text' | 'link'
    mail?: string
    tel?: string
    link?: string
}

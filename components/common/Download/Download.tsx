import React, {FC} from 'react';
import style from "../Download/Download.module.scss";
import Empty from "../../../public/icons/cloud-data-download.svg";
import cn from "classnames";

type DownloadTypes = {
    fileUrl: any
    handleInputChange: any
    values: any
    name: any
    className?: string
    label?: string
}

const Download: FC<DownloadTypes> = ({
                                         fileUrl,
                                         handleInputChange,
                                         values,
                                         name,
                                         className,
                                         label
                                     }) => {
    return (
        <div className={cn(style.downloadPhoto, className)}>
            {/*{fileUrl*/}
            {/*    ? <img className={style.beforeImage} src={fileUrl} alt="" />*/}
            {/*    : <Empty className={style.emptyBeforeImage} />}*/}
            <label className={style.download}>
                <div className={style.label}>
                    {label}
                </div>
                <input
                    id="secondFile"
                    name="cosmetologistQualification"
                    type="file"
                    accept="image/*"
                    style={{display: 'none'}}
                    onChange={handleInputChange}
                />
                <div className={cn(style.icon)}>
                    <span
                        className={style.icon_solution}
                    >
                        <svg className={style.iconDisplay} width="24" height="24" viewBox="0 0 24 24" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M11.5206 12V21.4013L9.45997 19.3406L8.78125 20.0194L12.0006 23.2387L15.22 20.0194L14.5412 19.3406L12.4806 21.4013V12H11.5206Z"
                            fill="black"/>
                        <path
                            d="M19.6785 8.25601C19.7019 7.53 19.4503 6.82194 18.9743 6.27333C18.4982 5.72473 17.8326 5.37598 17.1105 5.2968C16.7945 3.92481 16.0217 2.7009 14.9188 1.82573C13.8159 0.950563 12.4484 0.476057 11.0405 0.480005C7.59983 0.480005 4.80047 3.27936 4.80047 6.72C4.80047 6.90432 4.81007 7.092 4.82927 7.2864C2.35343 7.73088 0.480469 9.92113 0.480469 12.48C0.480469 15.3912 2.84927 17.76 5.76047 17.76H9.60047V16.8H5.76047C3.37823 16.8 1.44047 14.8618 1.44047 12.48C1.44047 10.2475 3.18335 8.35777 5.40863 8.17825L5.93231 8.136L5.84303 7.61856C5.79008 7.32193 5.76246 7.02132 5.76047 6.72C5.76047 3.8088 8.12927 1.44 11.0405 1.44C13.6291 1.44 15.8165 3.29376 16.2417 5.84736L16.3113 6.26544L16.7347 6.24816C16.7569 6.24848 16.779 6.24573 16.8005 6.24C17.8594 6.24 18.7205 7.10112 18.7205 8.16001C18.7205 8.29057 18.7061 8.42544 18.6768 8.5608L18.564 9.08688L19.0992 9.1392C20.0488 9.2295 20.9303 9.6715 21.5707 10.3784C22.2111 11.0854 22.5642 12.0061 22.5605 12.96C22.5605 15.0773 20.8377 16.8 18.7205 16.8H14.4005V17.76H18.7205C21.3672 17.76 23.5205 15.6067 23.5205 12.96C23.5246 11.8521 23.1437 10.7772 22.4429 9.91918C21.7421 9.06112 20.7649 8.47321 19.6785 8.25601Z"
                            fill="black"/>
                    </svg>
                         <div className={style.download_image}>
                             {!fileUrl.name ? 'Загрузить файл' : 'Сменить файл'}
                             <span className={style.fileName}>{fileUrl.name}</span>
                         </div>
                    </span>

                </div>

            </label>
        </div>
    );
};

export default Download;

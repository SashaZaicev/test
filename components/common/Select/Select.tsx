import {FC, useMemo} from 'react'
import cn from 'classnames'
import ReactSelect from 'react-select'

import style from './Select.module.scss'

type SelectProps = {
    name?: string
    placeholder?: string
    label?: string
    className?: string
    options: Array<{
        value: any
        label: any
    }>
    onChange: (e: any) => void
    onBlur?: (e: any) => void
    onFocus?: (e: any) => void
    value?: any
    text?: string
    error?: any
    type?: 'input'
}

export const Select: FC<SelectProps> = ({
                                            text,
                                            name,
                                            label,
                                            placeholder,
                                            options,
                                            onChange,
                                            className,
                                            value,
                                            error,
                                            onFocus,
                                            onBlur,
                                            type,
                                        }) => {
    const stylesSelect = useMemo(() => ({
        container: (base: any) => (
            (type !== 'input'
                ? {
                    ...base,
                    flex: '1 100%',
                    fontSize: '14',
                    cursor: 'pointer',
                    height: '100%',
                } : {
                    ...base,
                    flex: '1 100%',
                    fontSize: '14',
                    cursor: 'pointer',
                })),
        input: (styles: any) => ({
            ...styles,
            fontSize: '14px',
        }),
        placeholder: (styles: any) => (
            (type !== 'input'
                ? {
                    ...styles,
                    fontWeight: '400',
                    fontSize: '15px',
                    lineHeight: '15px',
                    color: '#707070',
                }
                : {
                    ...styles,
                    fontSize: '15px',
                    fontWeight: 'normal',
                })
        ),
        dropdownIndicator: (base: any) => ({
            ...base,
            color: '#006B54',
        }),
        singleValue: (styles: any) => ({
            ...styles,
            fontSize: '14px',
            lineHeight: '14px',
        }),
        valueContainer: (styles: any) => ({
            ...styles,
            height: '100%',
        }),
        indicatorsContainer: (styles: any) => (
            (type !== 'input'
                ? {...styles}
                : {
                    ...styles,
                    display: 'none',
                })),
        control: (styles: any) => (
            (type !== 'input'
                ? {
                    ...styles,
                    backgroundColor: 'white',
                    fontWeight: '600',
                    height: '100%',
                    boxShadow: 'none',
                    border: '1px solid #525252',
                    '&:hover': {
                        borderColor: '#006B54',
                    },
                    borderRadius: '4px',
                    cursor: 'pointer',
                }
                : {
                    ...styles,
                    backgroundColor: 'white',
                    fontWeight: '600',
                    // height: '100%',
                    boxShadow: '0 1px 45px rgba(0, 0, 0, 0.05)',
                    padding: '13.5px 30px',
                    border: 'none',
                    '&:hover': {
                        borderColor: '#006B54',
                    },
                    borderRadius: '4px',
                    cursor: 'text',
                })),
        option: (styles: any) => ({
            ...styles,
            transition: '.3s',
            cursor: 'pointer',
            fontSize: '14px',
            border: 'none',
            '&:hover': {
                background: '#FFFFFF',
                color: '#006B54',
            },
            '&:blur': {
                background: '#FFFFFF',
                color: '#006B54',
            },

        }),
        menu: (styles: any) => ({
            ...styles,
            marginTop: '0px',
            marginBottom: '0px',
            borderRadius: '0px',
            boxShadow: '0px 4px 35px rgba(0, 0, 0, 0.1)',
        }),
        menuList: (styles: any) => ({
            ...styles,
            padding: '0px',
        }),
    }), [type])

    return (
        <div className={cn(style.wrapper, className)}>
      <pre>
        {label && <div className={style.label}>{label}</div>}
          <ReactSelect
              placeholder={placeholder}
              value={value}
              options={options}
              styles={stylesSelect}
              onChange={onChange}
              onBlur={onBlur}
              onFocus={onFocus}
              name={name}
              className={cn(error && style.input_error)}
              noOptionsMessage={text ? (() => text) : (() => 'Выберите валидное значение')}
          />
          {error && <div className={style.error}>{error}</div>}
      </pre>
        </div>
    )
}

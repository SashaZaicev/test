import {
  FC, useCallback, useMemo, useState,
} from 'react'
import { useFormikContext } from 'formik'
import cn from 'classnames'
import {Select} from "./Select";


type SelectFormikProps = {
  name: string
  options: Array<{
    value: any
    label: any
  }>
  placeholder: string
  label?: string
  text?: string
  className?: string
  type?: 'input'
}

export const SelectFormik: FC<SelectFormikProps> = ({
  name, options, placeholder, label, text, className, type,
}) => {
  const {
    setFieldValue, errors, touched, values,
  } = useFormikContext<any>()

  const [isFocused, setIsFocused] = useState<boolean>(false)
  const handleFocus = useCallback(() => {
    setIsFocused(true)
  }, [])
  const handleBlur = useCallback(() => {
    setIsFocused(false)
  }, [])
  const isError = useMemo(() => !isFocused && errors[name] && touched[name], [errors, isFocused, name, touched])

  return (
    <>
      <Select
        value={values[name]}
        onChange={(e) => setFieldValue(name, e)}
        className={cn(className)}
        options={options}
        placeholder={placeholder}
        label={label}
        text={text}
        error={isError ? errors[name] : undefined}
        onBlur={handleBlur}
        onFocus={handleFocus}
        type={type}
      />

    </>
  )
}

import React, {FC, useEffect, useRef} from 'react'
import {Litepicker} from 'litepicker'
import cn from 'classnames'

import style from './Datepicker.module.scss'
import Calendar from '../../../public/icons/date.svg'
// import dynamic, {DynamicOptions} from "next/dynamic";

type RegisterDatepickerTypes = {
    date?: { newDate: string }
    classNamesInput?: string,
    className?: string,
    placeholder?: string,
    label?: string
    setDate: any,
    name?: string
}
export const RegisterDatepicker: FC<RegisterDatepickerTypes> = ({
                                                                    date,
                                                                    setDate,
                                                                    placeholder,
                                                                    label,
                                                                    className,
                                                                    classNamesInput,
                                                                    name
                                                                }) => {
    const litepickerRef = useRef<any>()

    useEffect(() => {
        new Litepicker({
            element: litepickerRef.current,
            // startDate: new Date(),
            maxDate: new Date(),
            format: 'YYYY-MM-DD',
            lang: 'ru',
            resetButton: false,
            singleMode: true,
            tooltipText: {
                one: 'день',
                other: 'дней',
                // @ts-ignore
                few: 'дня',
                many: 'дней',
            },
            scrollToDate: true,
            dropdowns: {
                minYear: 1920, maxYear: null, months: true, years: true,
            },
            setup: ((picker: any) => {
                picker.on('selected', (date1: any) => {
                    setDate('birthDate', date1.format('YYYY-MM-DD'))
                })
            }),
        })
    }, [])

    return (
        <div className={cn(style.registerPickerContainer, className)}>
            {<Calendar/> ? <Calendar className={style.calendarStyle}/> : ''}
            <label>
                {label && <div className={style.label}>{label}</div>}
                <input
                    autoComplete="off"
                    name="name"
                    className={cn(style.registerPickerContainer_block, classNamesInput)}
                    type="text"
                    ref={litepickerRef}
                    placeholder={placeholder}
                />
            </label>
        </div>
    )
}

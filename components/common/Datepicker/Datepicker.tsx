import { Litepicker } from 'litepicker'
import { FC, useEffect, useRef } from 'react'

import style from './Datepicker.module.scss'

type DatepickerProps = {
  date: {
    startDate: string
    endDate: string
  }
  setDate: (data: {
    startDate: string
    endDate: string
  }) => void
  maxDays?: number
  maxDate?: string
  minDate?: string
}

export const Datepicker: FC<DatepickerProps> = ({
  date, setDate, maxDays, maxDate, minDate,
}) => {
  const litepickerRef = useRef<any>()
  const widthScreen = document.body.clientWidth < 930

  useEffect(() => {
    // eslint-disable-next-line no-new
    new Litepicker({
      element: litepickerRef.current,
      maxDays,
      maxDate,
      minDate,
      singleMode: false,
      format: 'DD.MM.YYYY',
      startDate: date.startDate,
      endDate: date.endDate,
      numberOfMonths: widthScreen ? 1 : 2,
      numberOfColumns: widthScreen ? 1 : 2,
      lang: 'ru',
      resetButton: true,
      tooltipText: {
        one: 'день',
        other: 'дней',
        // @ts-ignore
        few: 'дня',
        many: 'дней',
      },
      setup: ((picker:any) => {
        picker.on('selected', (date1:any, date2:any) => {
          setDate({
            startDate: date1.format('DD.MM.YYYY'),
            endDate: date2.format('DD.MM.YYYY'),
          })
        })
      }),
    })
    // eslint-disable-next-line
  }, [])

  return (
    <input
      className={style.lightPickerContainer}
      type="text"
      ref={litepickerRef}
    />
  )
}

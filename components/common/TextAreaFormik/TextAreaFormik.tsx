import React, {
    FC, useCallback, useMemo, useState,
} from 'react'
import {useField} from 'formik'
import cn from 'classnames'

import style from './TextAreaFormik.module.scss'

type TextAreaFormikProps = {
    name: string
    placeholder?: string
    label?: string
    disabled?: boolean
    className?: string
}

export const TextAreaFormik: FC<TextAreaFormikProps> = ({
                                                            className,
                                                            name,
                                                            placeholder,
                                                            label,
                                                        }) => {
    const [field, meta] = useField(name)
    const [isFocused, setIsFocused] = useState<boolean>(false)
    const isError = useMemo(() => !isFocused && meta.error && meta.touched, [isFocused, meta.error, meta.touched])

    const handleFocus = useCallback(() => {
        setIsFocused(true)
    }, [])
    const handleBlur = useCallback((e) => {
        setIsFocused(false)
        field.onBlur(e)
    }, [field])

    return (
        <div className={cn(style.wrapper, className)}>
            <label> {label && <div className={style.label}>{label}</div>}
      <textarea
          {...field}
          className={cn(style.textArea, isError && style.textArea_error)}
          placeholder={placeholder}
          onFocus={handleFocus}
          onBlur={handleBlur}
      />
                {isError && <div className={style.error}>{meta.error}</div>}
            </label>
        </div>
    )
}

import React, {FC} from 'react';

import ItemBlock from "../ItemBlock";
import style from '/styles/CatalogList.module.scss';

type CatalogListTypes = {
  catalog: Data[]
}

export type Data = {
  id: number,
  title: string,
  description: string,
  image: string,
  newItem: boolean,
  recipe: boolean,
  child: boolean,
}
const CatalogList: FC<CatalogListTypes> = ({catalog}) => {
  return (
    <>
      {Array.isArray(catalog) && catalog.map((item) => {
          return (
            <ItemBlock
              key={item.id}
              id={item.id}
              description={item.description}
              title={item.title}
              image={item.image}
              classNames={style.itemSize}
              newItem={item.newItem}
              recipe={item.recipe}
              child={item.child}
            />)
        }
      )}
    </>
  );
};

export default CatalogList;

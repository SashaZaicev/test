import React, {useEffect} from 'react'
import cn from 'classnames'
import Image from 'next/image'

import style from '../../styles/MainScreen.module.scss'
import Arrow from '../../public/images/sliderBlock/arrow.svg'
import Test from '../../public/images/test.svg'
// Import Swiper React components
import {Swiper, SwiperSlide} from 'swiper/react'
import {Navigation} from 'swiper'

// Import Swiper styles
import 'swiper/css';
import AdvantageBlock from "../AdvantageBlock";
import Button from "../Button";
import Heading from "../Heading";
import SurveyAnketaBlock from "../SurveyAnketaBlock";
import NewsBlock from "../NewsBlock";
import ItemBlock from "../ItemBlock";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import {Modal} from "../Modal/Modal";
import ModalCallBack from "../Modals/ModalCallBack";
import {selectCatalogList} from "../../store/ducks/catalog/selectors";
import {useAppDispatch, useAppSelector} from "../../store/hooks";
import {getCatalog} from "../../store/ducks/catalog/thunks";

const newsArray = [
  {
    id: 1,
    description: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    title: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    date: '24.04.2022',
    imgUrl: '/images/3news.png',
    newItem: true,
    recipe: false,
    child: true,
  },
  {
    id: 2,
    description: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    title: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    date: '24.04.2022',
    imgUrl: '/images/1news.png'
  },
  {
    id: 3,
    description: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    title: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    date: '24.04.2022',
    imgUrl: '/images/2news.png'
  },
  {
    id: 4,
    description: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    title: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    date: '24.04.2022',
    imgUrl: '/images/2news.png'
  },
  {
    id: 5,
    description: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    title: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
    date: '24.04.2022',
    imgUrl: '/images/1news.png'
  },
]

const MainScreen = () => {
  const {t} = useTranslation('')
  const dispatch = useAppDispatch()

  const catalogArray = useAppSelector(selectCatalogList);

  useEffect(() => {
    dispatch(getCatalog())
  }, [dispatch])

  return (
    <div className={style.mainScreen}>
      <div className={style.sliderBlock}>
        <div className={style.container}>
          <Swiper
            modules={[Navigation]}
            navigation={{
              prevEl: '.navigation_left',
              nextEl: '.navigation_right',
            }}
            className={cn(style.swiperMain, 'main-slider')}
          >
            <SwiperSlide className={style.swiper_wrapper}>
              <div className={style.slideBlock}>
                <Image className={style.image} layout='fill' src="/images/sliderBlock/slide.png"
                       alt=""/>
                <div className={style.descriptionSlide}>
                  <div className={style.backImage}/>
                  <div className={style.title}>Доступность</div>
                  <div className={style.description}>
                    Лекарственные средства нашего производства можно найти во всех аптеках страны
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide className={style.swiper_wrapper}>
              <div className={style.slideBlock}>
                <Image className={style.image} layout='fill' src="/images/sliderBlock/slide.png"
                       alt=""/>
                <div className={style.descriptionSlide}>
                  <div className={style.backImage}/>
                  <div className={style.title}>Доступность</div>
                  <div className={style.description}>
                    Лекарственные средства нашего производства можно найти во всех аптеках страны
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <div className={cn('navigation_left', style.navigation_left)}>
              <Arrow/>
            </div>
            <div className={cn('navigation_right', style.navigation_right)}>
              <Arrow/>
            </div>
          </Swiper>
        </div>
        <div className={style.advantagesBlock}>
          <div className={style.container}>
            <AdvantageBlock
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et at proin et vitae et tortor."
              title="Производство"
              icon={<img src="/images/sliderBlock/process.svg" alt="symbol"/>}
              className={style.blockSize}
              classNameIcon={style.iconSize}
              classNameTextBlock={style.classNameTextBlock}
            />
            <AdvantageBlock
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et at proin et vitae et tortor."
              title="Политика в области качества"
              icon={<img src="/images/sliderBlock/certificate.svg"
                         alt="symbol"/>}
              className={style.blockSize}
              classNameIcon={style.iconSize}
              classNameTextBlock={style.classNameTextBlock}
            />
            <AdvantageBlock
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et at proin et vitae et tortor."
              title="Наши награды"
              icon={<img src="/images/sliderBlock/rewards.svg" alt="symbol"/>}
              className={style.blockSize}
              classNameIcon={style.iconSize}
              classNameTextBlock={style.classNameTextBlock}
            />
            <AdvantageBlock
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et at proin et vitae et tortor."
              title="Участки производства"
              icon={<img src="/images/sliderBlock/microscope.svg" alt="symbol"/>}
              className={style.blockSize}
              classNameIcon={style.iconSize}
              classNameTextBlock={style.classNameTextBlock}
            />
          </div>
        </div>
      </div>
      {catalogArray && <div className={style.production}>
        <Heading type={"secondary"} text='продукция'/>
        <div className={style.productionSlider}>
          <Swiper
            slidesPerView={5}
            modules={[Navigation]}
            navigation={{
              prevEl: '.navigation_second_left',
              nextEl: '.navigation_second_right',
            }}
            breakpoints={{
              320: {
                slidesPerView: 1.1,
                spaceBetween: 20
              },
              375: {
                slidesPerView: 1,
                centeredSlides: true,
                initialSlide: 1,
                spaceBetween: 20
              },
              671: {
                slidesPerView: 2,
                spaceBetween: 20
              },
              1024: {
                slidesPerView: 3,
                spaceBetween: 20
              },

              1220: {
                spaceBetween: 20,
                slidesPerView: 3,
              },
              1630: {
                slidesPerView: 5,
              },
            }}
            className={cn(style.swiperSecond, 'production-slider')}
          >
            {catalogArray.map((item) => {
              return (
                <SwiperSlide key={item.id}>
                  <ItemBlock
                    id={item.id}
                    description={item.description}
                    title={item.title}
                    image={item.image}
                    newItem={item.newItem}
                    recipe={item.recipe}
                    child={item.child}
                  />
                </SwiperSlide>
              )
            })}
            <div className={cn('navigation_second_left', style.navigation_second_left)}>
              <Arrow/>
            </div>
            <div className={cn('navigation_second_right', style.navigation_second_right)}>
              <Arrow/>
            </div>
          </Swiper>
        </div>
        <Link href={"/catalog"}>
          <a>
            <Button className={style.btnSize}>Посмотреть весь каталог</Button>
          </a>
        </Link>
      </div>}
      <div className={style.news}>
        <div className={style.container}>
          <div className={style.newsHeaderBlock}>
            <Heading type={"secondary"} text='Последние новости'/>
            <Link href={"/news"}>
              <a className={style.centerElement}>
                <Button className={cn(style.btnSize)}>Смотреть больше новостей</Button>
              </a>
            </Link>
          </div>
          <div className={style.newsBody}>
            {newsArray.map((newsItem) => (
              <NewsBlock
                id={newsItem.id}
                key={newsItem.id}
                description={newsItem.description}
                date={newsItem.date}
                backImage={newsItem.imgUrl}
                title={newsItem.title}
                className={cn(style['blockSize'])}/>)
            )}
          </div>
          <div className={cn(style.newsBody, style.mobileActive)}>
            <NewsBlock
              id={newsArray[0].id}
              key={newsArray[0].id}
              description={newsArray[0].description}
              date={newsArray[0].date}
              backImage={newsArray[0].imgUrl}
              title={newsArray[0].title}
              className={cn(style['blockSize'])}/>
          </div>
          <Link href={"/news"}>
            <a className={style.centerElement}>
              <Button className={cn(style.btnSize, style.newsHide)}>Смотреть больше новостей</Button>
            </a>
          </Link>
        </div>
      </div>
      <SurveyAnketaBlock icon={<Test/>}
                         text=" в опросе, чтобы помочь нам совершенствоваться"
                         colorText="Примите участие"
                         backImage={"/images/anketav4.png"}
      />
    </div>
  )
}

export default MainScreen

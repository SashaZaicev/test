import React, { useEffect, useRef, useState } from 'react'
import { useRouter } from 'next/router'
import useTranslation from 'next-translate/useTranslation'
import styles from '../../styles/LanguageSelect.module.scss'
import Image from'next/image'

export const LanguageSelect = () => {
  const [active, setActive] = useState(false)
  const router = useRouter()
  const sortRef = useRef<any>()
  const selectedItem = router.locale
  const { t } = useTranslation('menu')

  const handleActive = () => {
    setActive(!active)
  }
  const handleSelectId = (value: string) => {
    router.push(router.asPath, '/', { locale: value })
  }
  const handleOutsideClick = (e: any) => {
    if (!e.path.includes(sortRef.current)) {
      setActive(false)
    }
  }

  useEffect(() => {
    document.body.addEventListener('click', handleOutsideClick)
    return () => {
      document.body.removeEventListener('click', handleOutsideClick)
    }
  }, [])

  return (
    <div className={styles.select} onClick={handleActive} ref={sortRef}>
      <div className={styles.selected}>
        <Image width='20' height='15' src='/icons/ru.png' alt="flag" className={styles.flag} />
        {selectedItem}
      </div>
      <div className={styles.selectArrow} />
      {active
        ? (
          <div className={styles.selectContainer}>
            {router?.locales?.map((item) => (
              <div
                className={styles.selectItem}
                key={item}
                onClick={() => handleSelectId(item)}
              >
                {
                  // eslint-disable-next-line no-nested-ternary
                  (item === 'ru')
                    ? <Image width='20' height='15' src="/icons/ru.png" alt="flag" className={styles.flag} />
                    : (item === 'en')
                      ? <Image width='20' height='15' src="/icons/en.png" alt="flag" className={styles.flag} />
                      : <Image width='20' height='15' src="/icons/bel.png" alt="flag" className={styles.flag} />
                }
                {item}
              </div>
            ))}
          </div>
        )
        : <></>}
    </div>
  )
}

import Link from 'next/link'
import {FC} from 'react'
import cn from 'classnames'
import {ReactElement} from "react";

import Circle from '../../public/icons/circle.svg'

const CustomLink: FC<CustomLinkTypes> = ({
                                           backImage = true, text, href, icon, className,
                                         }) => (
  <Link href={href}>
    <a>
      <div>
        {backImage && <Circle/>}
        <span className={cn(className)}>
      {icon && icon}
          {text}
    </span>
      </div>
    </a>
  </Link>
)
export default CustomLink

interface CustomLinkTypes {
  text: string
  href: string
  icon?: ReactElement | null
  className?: string
  backImage?: boolean
}

import React, {ChangeEvent, FC, useState} from 'react';
import Search from '../../public/icons/search2.svg'
import style from '../../styles/Search.module.scss'
import cn from "classnames";

type SearchFieldTypes = {
    classNames?: string
    onClick?: any
}

const SearchField: FC<SearchFieldTypes> = ({onClick, classNames}) => {
    const [valueSearchField, setValueSearchField] = useState('')

    const findItem = (el: ChangeEvent<HTMLInputElement>) => {
        setValueSearchField(el.target.value)
    }
    console.log('valueSearchField', valueSearchField)
    return (
        <div className={cn(style.searchBlock, classNames)}>
            <div className={style.searchIcon}><Search/></div>
            <input onKeyDown={onClick} maxLength={50} onChange={findItem} value={valueSearchField} name="search"
                   type="search"
                   placeholder="Поиско по коду АТХ, МНН или наименованию товара"/>
        </div>
    );
};

export default SearchField;

import React, {FC} from 'react';
import style from '../../styles/SingleProductionAboutBlock.module.scss'
import cn from "classnames";

type SingleProductionAboutBlockTypes = {
  image: string
  title: string
  description: string
  className?: string
}

const SingleProductionAboutBlock: FC<SingleProductionAboutBlockTypes> = ({
                                                                           image,
                                                                           title,
                                                                           description,
                                                                           className
                                                                         }) => {
  return (
    <div className={cn(style.singleProductionAboutBlock, className)}>
      <img src={image} alt='alt name'/>
      <div className={style.title}>{title}</div>
      <div className={style.description}>{description}</div>
    </div>
  );
};

export default SingleProductionAboutBlock;

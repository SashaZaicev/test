import {FC} from 'react'
import cn from 'classnames'
import style from '../../styles/AdvantageBlock.module.scss'

const AdvantageBlock: FC<AdvantageBlockTypes> = ({
                                                   text,
                                                   title,
                                                   icon,
                                                   className,
                                                   classNameTextBlock,
                                                   classNameIcon,
                                                 }) => (
  <div className={cn(style.advantageBlockContainer, className)}>
    <div className={cn(style.icon, classNameIcon)}>{icon}</div>
    <div className={cn(style.textBlock, classNameTextBlock)}>
      <span className={style.title}>{title}</span>
      <span className={style.description}>{text}</span>
    </div>
  </div>
)
export default AdvantageBlock

interface AdvantageBlockTypes {
  text: string
  title: string
  icon?: any
  className?: string
  classNameTextBlock?: string
  classNameIcon?: string
}

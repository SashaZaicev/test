import React, {useMemo, useState} from 'react';
import style from './PharmacovigilanceForm.module.scss'
import {Form, Formik} from "formik";
import {FieldFormik} from "../FieldFormik/FieldFormik";
import {Select} from "../common/Select/Select";
import cn from "classnames";
import {RegisterDatepicker} from "../common/Datepicker/RegisterDatepicker";
import Button from "../Button";
import {TextAreaFormik} from "../common/TextAreaFormik/TextAreaFormik";
import {SelectFormik} from "../common/Select/SelectFormik";
import Download from "../common/Download/Download";
import * as Yup from "yup";
import {validateForm} from "../messages/validateForm";
import {phoneRegexp} from "../../lib/regex";

const PharmaFormSecond = () => {
    let dataPhoto = {}
    const [fileUrl, setFileUrl] = useState<any>(dataPhoto || null)
    const genderValue = [
        {value: 2, label: 'Ж'},
        {value: 1, label: 'М'},
        {value: 0, label: 'С'},
    ]
    const validationSchema = Yup.object().shape({
        fullName: Yup.string().required(validateForm.required),
        phone: Yup.string().matches(phoneRegexp, validateForm.phone)
            .required(validateForm.required),
        country: Yup.string().required(validateForm.required),
    })
    const [filterValueGender, setFilterValueGender] = useState(genderValue[2])

    const initialValues = useMemo(() => ({
        //user (Персональные данные)
        fullName: '',
        phone: '',
        gender: filterValueGender,
        age: '',
        country: '',
        email: '',
        medicinalProduct: '',
        series: '',
        situation: '',
        downloadFile: fileUrl,

    }), [fileUrl])
    const handleInputChange = (event: any) => {
        const imageFile = event.target.files[0]
        setFileUrl(imageFile)
    }
    return (
        <div className={style.pharmaFormSecondContainer}>
            <div className={style.fieldBlock}>
                <Formik
                    initialValues={initialValues}
                    onSubmit={(value) => {
                        console.log(value)
                    }}
                    validationSchema={validationSchema}
                >
                    {({values, setFieldValue}) => (
                        <Form>
                            <div className={style.title}>
                                Персональные данные
                            </div>
                            <div className={style.box_1}>
                                <FieldFormik
                                    className={style.inputLabel}
                                    placeholder="Ф.И.О"
                                    label="Укажите данные"
                                    name='fullName'
                                />
                                <FieldFormik
                                    className={style.inputLabel}
                                    placeholder="+375 (ХХ) ХХХ ХХ ХХ"
                                    mask="+{375}(00)000-00-00"
                                    label="Контактный телефон"
                                    name='phone'
                                />
                                <Select
                                    name={'gender'}
                                    options={genderValue}
                                    onChange={setFilterValueGender}
                                    // value={filterValue2}
                                    placeholder={"Пол"}
                                    label={'Выберите тип'}
                                    className={cn(style.select)}
                                />
                                <FieldFormik
                                    className={style.inputLabel}
                                    placeholder="Возраст"
                                    label="Укажите данные"
                                    name='age'
                                />

                                <FieldFormik
                                    className={style.inputLabel}
                                    placeholder="Страна"
                                    label="Укажите данные"
                                    name='country'
                                />
                                <FieldFormik
                                    className={style.inputLabel}
                                    placeholder="Email"
                                    label="Укажите данные"
                                    name='email'
                                />
                            </div>
                            <div className={style.title}>
                                Информация о подозреваемом лекарственном средстве
                            </div>
                            <div className={style.box_2}>
                                <TextAreaFormik
                                    name={'medicinalProduct'}
                                    label={'Торговое название лекарственного средства, которое Вы применяли (см. упаковку)*'}
                                    placeholder={'Название лекарственного средства'}
                                    className={cn(style.inputLabel, style.textAreaSize)}
                                />

                                <FieldFormik
                                    className={style.inputLabel}
                                    placeholder="Номер серии"
                                    label="Название серии (см. упаковку)*"
                                    name='series'
                                />
                            </div>
                            <div className={style.title}>
                                Информация о нежелательной реакции / отсутствии эффективности
                            </div>
                            <div className={style.box_3}>
                                <TextAreaFormik
                                    name={'situation'}
                                    label={'Описание случая нежелательной реакции / отсутствии эффективности*'}
                                    placeholder={'Опишите ситуацию'}
                                    className={cn(style.inputLabel, style.textAreaSize)}
                                />
                                <Download
                                    values={values}
                                    fileUrl={fileUrl}
                                    handleInputChange={handleInputChange}
                                    name={'downloadFile'}
                                    className={style.downloadSize}
                                    label={'Пожалуйста, приложите другие страницы, если это необходимо'}
                                />
                            </div>
                            <div className={`${style.btn}`}>
                                <Button
                                    // loading={isLoading}
                                    htmlType="submit"
                                    type="primary"
                                    className={style.btnSize}
                                >
                                    Отправить обращение
                                </Button>
                                <span className={style.privacy}>Отправляя данную форму, Вы принимаете нашу <a href="#">политику конфиденциальности.</a></span>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default PharmaFormSecond;

import React, {useMemo, useState} from 'react';
import style from './PharmacovigilanceForm.module.scss'
import {FieldFormik} from "../FieldFormik/FieldFormik";
import {Form, Formik} from "formik";
import {validateForm} from "../messages/validateForm";
import * as Yup from 'yup'
import cn from "classnames";

import Button from "../Button";
import {phoneRegexp} from "../../lib/regex";
import {Select} from "../common/Select/Select";
// import {RegisterDatepicker} from "../common/Datepicker/RegisterDatepicker";
import {TextAreaFormik} from "../common/TextAreaFormik/TextAreaFormik";
import {SelectFormik} from "../common/Select/SelectFormik";
import Download from "../common/Download/Download";
import dynamic from "next/dynamic";

const validationSchema = Yup.object().shape({
  fullName: Yup.string().required(validateForm.required),
  phone: Yup.string().matches(phoneRegexp, validateForm.phone)
    .required(validateForm.required),
  address: Yup.string().required(validateForm.required),
})
const RegisterDatepicker = dynamic(import('../common/Datepicker/RegisterDatepicker').then(mod => mod.RegisterDatepicker),
  {ssr: false}
)

const PharmaFormFirst = () => {
  // const dispatch = useDispatch<AppDispatch>()
  // const backError = useSelector(selectBackError)
  let dataPhoto = {}
  const [fileUrl, setFileUrl] = useState<any>(dataPhoto || null)
  const access = [
    {value: 2, label: 'Не лечится'},
    {value: 1, label: 'Лечится'},
    {value: 0, label: 'Лечение'},
  ]
  const access2 = [
    {value: 2, label: 'Не лечится'},
    {value: 1, label: 'Лечится'},
    {value: 0, label: 'Сообщение'},
  ]
  const genderValue = [
    {value: 2, label: 'Ж'},
    {value: 1, label: 'М'},
    {value: 0, label: 'С'},
  ]
  const patientComplainsValue = [
    {value: 2, label: 'Нарушение функции печени'},
    {value: 1, label: 'Нарушение функции сердца'},
    {value: 0, label: 'Тяжело дышать'},
  ]
  const patientComplainsSecondValue = [
    {value: 2, label: 'Нарушение функции печени'},
    {value: 1, label: 'Нарушение функции сердца'},
    {value: 0, label: 'Тяжело дышать'},
  ]

  const [medicalValue, setFilterValue] = useState(access[2])
  const [messageValue, setMessageValue] = useState(access2[2])
  const [filterValueGender, setFilterValueGender] = useState(genderValue[2])
  const [patientComplains, setPatientComplains] = useState(patientComplainsValue[2])
  const [patientComplainsSecond, setPatientComplainsSecond] = useState(patientComplainsSecondValue[2])


  const initialValues = useMemo(() => ({
    //worker (Медицинский или фармацевтический работник, сообщающий о нежелательной реакции)
    nameWorker: '',
    phoneWorker: '',
    institutionAddressWorker: '',
    positionPlaceWorker: '',
    medicalWorker: medicalValue,
    messageWorker: messageValue,
    //patient (Информация о пациенте)
    namePatient: '',
    cardNumberPatient: '',
    genderPatient: '',
    agePatient: '',
    weightPatient: '',
    disruptionOrgansPatientFirst: '',
    disruptionOrgansPatientSecond: '',
    allergyPatient: '',
    //suspectDrug (Подозреваемое лекарственное средство)
    internationalNameDrug: '',
    tradeNameDrug: '',
    manufacturerDrug: '',
    seriesDrug: '',
    indicationUseDrug: '',
    routeDrug: '',
    singleDoseDrug: '',
    multiplicityIntroductionDrug: '',
    startTherapyDrug: '',
    endTherapyDrug: '',
    //other concomitant medications 'OCM' (Другие одновременно принимаемые лекарственные средства)
    internationalNameOCM: '',
    indicationUseOCM: '',
    routeOCM: '',
    singleOCM: '',
    multiplicityOCM: '',
    startTherapyOCM: '',
    endTherapyOCM: '',
    //description adverse reaction
    descriptionAR: '',
    startTherapyAR: '',
    endTherapyAR: '',
    resultStoppingAR: '',
    riskFactors: '',
    evaluationCausalityAR: '',
    resultAR: '',
    measuresAR: '',
    drugUsedAR: '',
    otherMeasuresAR: '',
    seriousAR: '',
    relapseAR: '',
    suspectedDrugAR: '',
    //important additional
    dateIA: '',
    amnesticDataIA: '',
    suspectedIA: '',
    otherDrugsIA: '',
    downloadFile: fileUrl,
    completionIA: '',
    signatureIA: ''

  }), [fileUrl])


  // const handleSetDate = (name: string, newDate: string) => {
  //     setDate(name, newDate)
  // }
  const handleInputChange = (event: any) => {
    const imageFile = event.target.files[0]
    setFileUrl(imageFile)
  }
  return (
    <div className={style.pharmaFormFirstContainer}>
      <div className={style.fieldBlock}>
        <Formik
          initialValues={initialValues}
          onSubmit={(value) => {
            console.log(value)
          }}
          validationSchema={validationSchema}
        >
          {({values, setFieldValue}) => (
            <Form>
              <div className={style.title}>
                Медицинский или фармацевтический работник, сообщающий о нежелательной реакции
              </div>
              <div className={style.box_1}>
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Ф.И.О"
                  label="Укажите данные"
                  name='nameWorker'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="+375 (ХХ) ХХХ ХХ ХХ"
                  mask="+{375}(00)000-00-00"
                  label="Контактный телефон"
                  name='phoneWorker'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Адрес учереждения"
                  label="Укажите данные"
                  name='institutionAddressWorker'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Должность и место работы"
                  label="Укажите данные"
                  name='positionPlaceWorker'
                />
                <Select
                  options={access}
                  onChange={setFilterValue}
                  // value={filterValue}
                  placeholder={"Лечение"}
                  label={'Выберите тип'}
                  className={cn(style.select)}
                />
                <Select
                  options={access2}
                  onChange={setMessageValue}
                  // value={filterValue2}
                  placeholder={"Сообщение"}
                  label={'Выберите тип'}
                  className={cn(style.select)}
                />
              </div>
              {/*{backError && isString(backError) && <div className={style.error}>{backError}</div>}*/}
              <div className={style.title}>
                Информация о пациенте
              </div>
              <div className={style.box_2}>
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Ф.И.О"
                  label="Укажите данные"
                  name='namePatient'
                />
                <FieldFormik
                  className={style.inputLabel}
                  label="Номер медецинской карты"
                  name="cardNumberPatient"
                  placeholder="000000000000000000000"
                />
                <Select
                  name={'genderPatient'}
                  options={genderValue}
                  onChange={setFilterValueGender}
                  // value={filterValue2}
                  placeholder={"Укажите пол пацента"}
                  label={'Выбрать вариант'}
                  className={cn(style.select)}
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Прим: 36"
                  label="Укажите возраст пациента"
                  name='agePatient'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Вес"
                  label="Укажите данные в кг"
                  name='weightPatient'
                />
                <Select
                  options={patientComplainsValue}
                  onChange={setPatientComplains}
                  // value={filterValue2}
                  placeholder={"Нарушение функции печени"}
                  label={'Выбрать вариант'}
                  className={cn(style.select)}
                  name={'disruptionOrgansPatientFirst'}
                />
                <Select
                  options={patientComplainsSecondValue}
                  onChange={setPatientComplainsSecond}
                  // value={filterValue2}
                  placeholder={"Нарушение функции почек"}
                  label={'Выбрать вариант'}
                  className={cn(style.select)}
                  name={'disruptionOrgansPatientSecond'}
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Аллергия"
                  label="Укажите данные (Указать на что именно)"
                  name={'allergyPatient'}
                />
              </div>
              <div className={style.title}>
                Подозреваемое лекарственное средство
              </div>
              <div className={style.box_3}>
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Международное непатентованное название"
                  label="Укажите данные"
                  name='internationalNameDrug'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Торговое название"
                  label="Укажите данные"
                  name='tradeNameDrug'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Производитель"
                  label="Укажите данные"
                  name='manufacturerDrug'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Номер серии"
                  label="Укажите данные"
                  name='seriesDrug'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Показание к применению"
                  label="Укажите данные"
                  name='indicationUseDrug'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Путь введения"
                  label="Укажите данные"
                  name='routeDrug'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Разовая доза"
                  label="Укажите данные"
                  name='singleDoseDrug'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Кратность введения"
                  label="Укажите данные"
                  name='multiplicityIntroductionDrug'
                />
                <RegisterDatepicker
                  className={style.datePicker}
                  placeholder={'ДД.ММ.ГГГГ'}
                  label={"Дата начала терапии"}
                  setDate={setFieldValue}
                  name={"startTherapyDrug"}
                />
                <RegisterDatepicker
                  className={style.datePicker}
                  placeholder={'ДД.ММ.ГГГГ'}
                  label={"Дата окончания терапии"}
                  setDate={setFieldValue}
                  name={"endTherapyDrug"}
                />
              </div>
              <div className={style.title}>
                Другие одновременно принимаемые лекарственные средства
                <div className={style.title_sub}>(Укажите &quot;нет&quot;, если других лекарственных средств не
                  принимал)</div>
              </div>
              <div className={style.box_4pre}>
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Название лекарственного средства"
                  label="Международное непатентованное название или торговое название лекарственного средства"
                  name='internationalNameOCM'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Показание к применению"
                  label="Укажите данные"
                  name='indicationUseOCM'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Путь введения"
                  label="Укажите данные"
                  name='routeOCM'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Разовая доза"
                  label="Укажите данные"
                  name='singleOCM'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Кратность введения"
                  label="Укажите данные"
                  name='multiplicityOCM'
                />
                <RegisterDatepicker
                  className={style.datePicker}
                  placeholder={'ДД.ММ.ГГГГ'}
                  label={"Дата начала терапии"}
                  setDate={setFieldValue}
                  name={'startTherapyOCM'}
                />
                <RegisterDatepicker
                  className={style.datePicker}
                  placeholder={'ДД.ММ.ГГГГ'}
                  label={"Дата окончания терапии"}
                  setDate={setFieldValue}
                  name={'endTherapyOCM'}
                />
              </div>
              <Button type={"none"} className={style.addDrug}>Добавить еще один препарат</Button>
              <div className={style.box_4}>
                <TextAreaFormik
                  name={'descriptionAR'}
                  label={'Описание подозреваемой нежелательной реакции'}
                  placeholder={'Название лекарственного средства'}
                  className={cn(style.inputLabel, style.textAreaSize)}
                />
                <RegisterDatepicker
                  className={style.datePicker}
                  placeholder={'ДД.ММ.ГГГГ'}
                  label={"Дата начала"}
                  setDate={setFieldValue}
                  name={'startTherapyAR'}
                />
                <RegisterDatepicker
                  className={style.datePicker}
                  placeholder={'ДД.ММ.ГГГГ'}
                  label={"Дата окончания"}
                  setDate={setFieldValue}
                  name={'endTherapyAR'}
                />
                <SelectFormik
                  options={patientComplainsSecondValue}
                  // onChange={setPatientComplainsSecond}
                  // value={filterValue2}
                  placeholder={"Выберите вариант"}
                  label={'Результат прекращения приема подозреваемого лекарственного средства'}
                  name={'resultStoppingAR'}
                  className={cn(style.select)}
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Укажите заболевания"
                  label="Сопутствующие заболевания, иные состояния или факторы риска"
                  name='riskFactors'
                />
                <SelectFormik
                  options={patientComplainsSecondValue}
                  // onChange={setPatientComplainsSecond}
                  // value={filterValue2}
                  placeholder={"Выберите вариант"}
                  label={'Оценка причинно-следственной связи'}
                  name={'evaluationCausalityAR'}
                  className={cn(style.select)}
                />
                <SelectFormik
                  options={patientComplainsSecondValue}
                  // onChange={setPatientComplainsSecond}
                  // value={filterValue2}
                  placeholder={"Выберите вариант"}
                  label={'Исход'}
                  name={'resultAR'}
                  className={cn(style.select)}
                />
                <SelectFormik
                  options={patientComplainsSecondValue}
                  // onChange={setPatientComplainsSecond}
                  // value={filterValue2}
                  placeholder={"Выберите вариант"}
                  label={'Предпринятые меры'}
                  name={'measuresAR'}
                  className={cn(style.select)}
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Лекарственные средства"
                  label="Лекарственные средства, применяемые для купирования нежелательной реакции (если требовалось)"
                  name='drugUsedAR'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Укажите иные меры"
                  label="Другие предпринятые меры"
                  name='otherMeasuresAR'
                />
                <SelectFormik
                  options={patientComplainsSecondValue}
                  // onChange={setPatientComplainsSecond}
                  // value={filterValue2}
                  placeholder={"Выберите вариант"}
                  label={'Критерии отнесения к серьезным нежелательным реакциям(отметьте, если это подходит)'}
                  name={'seriousAR'}
                  className={cn(style.select)}
                />
                <SelectFormik
                  options={patientComplainsSecondValue}
                  // onChange={setPatientComplainsSecond}
                  // value={filterValue2}
                  placeholder={"Выберите вариант"}
                  label={'Отмечено ли повторение нежелательной реакции после повторного назначения лекарственного средства'}
                  name={'relapseAR'}
                  className={cn(style.select)}
                />
                <SelectFormik
                  options={patientComplainsSecondValue}
                  // onChange={setPatientComplainsSecond}
                  // value={filterValue2}
                  placeholder={"Выберите вариант"}
                  label={'Подозреваемое лекарственное средство применяется в'}
                  name={'suspectedDrugAR'}
                  className={cn(style.select)}
                />
              </div>
              <div className={style.title}>
                Важная дополнительная информация
                <div className={style.descriptionTitle}>
                  Данные клинических, лабораторных, рентгенологических исследований и аутопсии, включая
                  определение концентрации лекарственных средств в крови (тканях), если таковые имеются и
                  связаны с нежелательной реакцией (пожалуйста, приведите даты)
                </div>
              </div>

              <div className={style.box_5}>
                <RegisterDatepicker
                  className={style.datePicker}
                  placeholder={'ДД.ММ.ГГГГ'}
                  label={"Укажите дату"}
                  setDate={setFieldValue}
                  name={'dateIA'}
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Сопутствующие заболевания, амнестические данные"
                  label="Укажите данные"
                  name='amnesticDataIA'
                />
                <FieldFormik
                  className={style.inputLabel}
                  placeholder="Подозреваемые лекарственные взаимодействия"
                  label="Укажите данные"
                  name='suspectedIA'
                />
              </div>
              <div className={cn(style.descriptionTitle)}>
                Для врожденных аномалий указать все другие лекарственные средства, принимаемые во
                время беременности, а также дату последней менструации (укажите «нет» если такие
                средства отсутствуют)
              </div>
              <div className={style.box_6}>
                  <FieldFormik
                      className={style.inputLabel}
                      placeholder="Другие лекарственные средства"
                      label="Укажите “Нет”, если такие средства отсутствуют"
                      name='otherDrugsIA'
                  />
                  <Download
                      values={values}
                      fileUrl={fileUrl}
                      handleInputChange={handleInputChange}
                      name={'downloadFile'}
                      className={style.downloadSize}
                      label={'Пожалуйста, приложите другие страницы, если это необходимо'}
                  />
                  <RegisterDatepicker
                      className={style.datePicker}
                      placeholder={'ДД.ММ.ГГГГ'}
                      label={"Дата заполнения"}
                      setDate={setFieldValue}
                      name={'completionIA'}
                  />
                  <FieldFormik
                      className={style.inputLabel}
                      placeholder="Инициалы врача"
                      label="Подпись"
                      name='signatureIA'
                  />
              </div>
              <div className={`${style.btn}`}>
                <Button
                  // loading={isLoading}
                  htmlType="submit"
                  type="primary"
                  className={style.btnSize}
                >
                  Отправить обращение
                </Button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default PharmaFormFirst;



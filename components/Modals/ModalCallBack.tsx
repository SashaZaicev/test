import React, {useMemo, useState} from 'react';
import * as Yup from "yup";
import {Form, Formik} from "formik";

import style from "./Modals.module.scss";
import {validateForm} from "../messages/validateForm";
import {phoneRegexp} from "../../lib/regex";
import {FieldFormik} from "../FieldFormik/FieldFormik";
import Button from "../Button";
import {useAppDispatch} from "../../store/hooks";
import {setCallbackMsg} from "../../store/ducks/common/thunks";

const ModalCallBack = () => {
  const dispatch = useAppDispatch()

  const [successModal, setSuccessModal] = useState(false)

  const validationSchema = Yup.object().shape({
    fullName: Yup.string().required(validateForm.required),
    phone: Yup.string().matches(phoneRegexp, validateForm.phone)
      .required(validateForm.required),
  })

  const initialValues = useMemo(() => ({
    fullName: '',
    phone: ''
  }), [])

  return (
    <div className={"modalContainerCallBack"}>
      <div className={"titleCallBack"}>
        Заказать <span className={"green"}>обратный звонок</span>
      </div>
      <Formik
        initialValues={initialValues}
        onSubmit={(value) => {
          dispatch(setCallbackMsg(value.fullName, value.phone))
        }}
        validationSchema={validationSchema}
      >
        {() => (
          <Form>
            <FieldFormik
              className={style.inputLabel}
              placeholder="Как к Вам обратиться?"
              name='fullName'
            />
            <FieldFormik
              className={style.inputLabel}
              placeholder="+375 (ХХ) ХХХ ХХ ХХ"
              mask="+{375}(00)000-00-00"
              name='phone'
            />
            <div className={style.btnBlock}>
              <div className={style.privacy}>Ваши персональные данные находятся в безопасности</div>
              <Button htmlType={'submit'}
                      onClickFunc={() => setSuccessModal(!successModal)}
                      type={"none"} className={style.btnSize}>Заказать
                звонок</Button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default ModalCallBack;

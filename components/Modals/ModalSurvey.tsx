import React, {useMemo, useRef, useState} from 'react';
import * as Yup from "yup";

import style from "./Modals.module.scss";
import {validateForm} from "../messages/validateForm";
import {phoneRegexp} from "../../lib/regex";
import {Form, Formik} from "formik";
import Button from "../Button";
import cn from "classnames";
import {Select} from "../common/Select/Select";
import {useAppDispatch} from "../../store/hooks";
import {SelectFormik} from "../common/Select/SelectFormik";
import {setQuizInfoList} from "../../store/ducks/common/thunks";

const first = [
  {value: 'Да', label: 'Да'},
  {value: 'Нет', label: 'Нет'},
]
const second = [
  {value: '1', label: 'Да'},
  {value: '0', label: 'Нет'},
]
const third = [
  {value: '2', label: 'Знакомые'},
  {value: '1', label: 'Газета'},
  {value: '0', label: 'Интернет'},
]

const ModalSurvey = () => {
  const dispatch = useAppDispatch()
  const formRef = useRef<any>()

  const [specialist, setFirstValue] = useState(first[1])
  const [production, setSecondValue] = useState(second[1])
  const [howFindProduct, setThirdValue] = useState(third[1])

  const validationSchema = Yup.object().shape({
    specialist: Yup.object().nullable().required(validateForm.required),
    production: Yup.object().nullable().required(validateForm.required),
    howFindProduct: Yup.object().nullable().required(validateForm.required),
  })
  const initialValues = useMemo(() => ({
    specialist: null as null | { value: string, label: string },
    production: null as null | { value: string, label: string },
    howFindProduct: null as null | { value: string, label: string },
  }), [])

  return (
    <div className={"modalContainerSurvey"}>
      <div className={"titleCallBack"}>
        Пройдите <span className={"green"}>опрос</span>
      </div>
      <div className={'descriptionTitleSurvey'}>Примите участие в опросе, чтобы помочь нам совершенствоваться</div>
      <Formik
        initialValues={initialValues}
        onSubmit={(value) => {
          // console.log('valuevalue', value)
          dispatch(setQuizInfoList(value.specialist.label, value.production.label, value.howFindProduct.label))
        }}
        validationSchema={validationSchema}
        innerRef={formRef}
      >
        {() => (
          <Form>
            <SelectFormik
              name={'specialist'}
              options={first}
              placeholder={"Выбрать вариант"}
              label={'Являетесь ли вы специалистом в области здравоохранения?'}
              className={cn(style.select)}
            />
            <SelectFormik
              name={'production'}
              options={second}
              placeholder={"Выбрать вариант"}
              label={'Приобретаете ли вы продукцию ОАО "Экзон"?'}
              className={cn(style.select)}
            />
            <SelectFormik
              name={'howFindProduct'}
              options={third}
              placeholder={"Выбрать вариант"}
              label={'Откуда вы узнаете о нашей продукции?'}
              className={cn(style.select)}
            />
            <div className={style.btnBlock}>
              <div className={style.privacy}>Ваши персональные данные находятся в безопасности</div>
              <Button htmlType={'submit'}
                      type={"none"}
                      className={style.btnSize}>
                Отправить данные опроса
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default ModalSurvey;

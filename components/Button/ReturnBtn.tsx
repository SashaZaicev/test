import {FC, ReactNode} from 'react'
import cn from 'classnames'
// import { useHistory } from 'react-router-dom'

import style from '../../styles/Button.module.scss'

type ButtonProps = {
  className?: string
  outline?: boolean
  type?: 'primary' | 'secondary' | 'text' | 'textSecondary'
  disabled?: boolean
  icon?: ReactNode
  loading?: boolean
  url?: string
  children?: any
}

export const ReturnBtn: FC<ButtonProps> = ({
                                             children,
                                             className,
                                             disabled,
                                             outline,
                                             type = 'primary',
                                             icon,
                                             loading,
                                             url,
                                           }) => {
  // const history = useHistory()

  return (
    <button
      type="button"
      className={cn(style.btn, style.returnBtn, style[type], outline && style.outline, className)}
      disabled={disabled || loading}
      // onClick={!url ? () => history.goBack() : () => history.push(url)}
    >
      {icon || <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
        <path d="M13.2754 15.5518L8.72367 11L13.2754 6.4483" stroke="#006B54" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round"
              strokeLinejoin="round"/>
      </svg>
      }
      <div>{children}</div>
      {/*{loading && <Spinner className={style.spinner} />}*/}
    </button>
  )
}

import { FC } from 'react'
import cn from 'classnames'

import style from '../../styles/Button.module.scss'

type ButtonProps = {
  htmlType?: 'submit' | 'reset' | 'button'
  className?: string
  type?: 'primary' | 'secondary' | 'none' | 'cancel'
  disabled?: boolean
  icon?: any
  onClickFunc?: any
  loading?: boolean
  children?: any
}

const Button: FC<ButtonProps> = ({
  onClickFunc,
  children,
  htmlType,
  className,
  disabled,
  type = 'primary',
  icon,
  loading,
}) => (
  <button
    // eslint-disable-next-line react/button-has-type
    type={htmlType ?? 'button'}
    className={cn(style.btn, style[type], className)}
    disabled={disabled || loading}
    onClick={onClickFunc}
  >
    {icon}
    <div>{children}</div>
    {loading && 'SPINNER'}
  </button>
)
export default Button

import React, {FC} from 'react';
import styles from '../../styles/NewsBlock.module.scss'
import Link from "next/link";
import cn from "classnames";

type NewsBlockTypes = {
    id: number
    title: string
    description: string
    className?: any
    date?: string
    backImage: string
    href?: string
    key?: any
}

const NewsBlock: FC<NewsBlockTypes> = ({
                                           id,
                                           title,
                                           description,
                                           className,
                                           date,
                                           backImage,
                                           href
                                       }) => {
    return (
        <Link href={href || '/news/[item]'} as={`/news/${id}`}>
            <div className={cn(className, styles.wrapper)}>
                <a
                    className={cn(styles.news_block)}
                    style={{backgroundImage: `linear-gradient(180deg, rgba(0, 0, 0, 0.75) 0%, rgba(0, 107, 84, 0) 0.01%, rgba(0, 107, 84, 0.75) 100%), url('${backImage}')`}}
                >
                    <div className={styles["news-top"]}>
                        {date}
                    </div>
                    <div className={styles['news-bottom']}>
                        <div>
                            <div className={styles.data}>{title}</div>
                            <div className={styles.title}>
                                {description}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </Link>
    );
};

export default NewsBlock;

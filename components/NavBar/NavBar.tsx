import React, { useState, MouseEvent, FC } from 'react'
import classNames from 'classnames/bind'
import useTranslation from 'next-translate/useTranslation'
import cn from 'classnames'
import Image from 'next/image'

import styles from '../../styles/Navbar.module.scss'
import CustomLink from '../CustomLink'
import useDeviceDetect from '../CustomHooks/useDeviceDetect'

const cx = classNames.bind(styles)
type NavBarProps = {
  styleClass?: string
}

const NavBar: FC<NavBarProps> = ({ styleClass }) => {
  const { t } = useTranslation('')
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const { isMobile } = useDeviceDetect()

  const className = cx({
    close: !isMenuOpen,
    open: isMenuOpen,
  })

  const toggleMenu = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    setIsMenuOpen(!isMenuOpen)
  }

  return (
    <div className={cn(styles.menu, styleClass)}>
      <button
        type="button"
        className={`${styles.menuToggle} ${className}`}
        onClick={toggleMenu}
      >
        <span className="visually-hidden">Открыть меню</span>
      </button>
      {isMenuOpen && isMobile && (
      <>
        <div className={styles.link}>
          <CustomLink text={t('menu:catalog-menu-title')} href="/catalog" />
        </div>
        <div className={styles.link}>
          <CustomLink text={t('menu:company-menu-title')} href="/company" />
        </div>
        <div className={styles.link}>
          <CustomLink text={t('menu:news-menu-title')} href="/news" />
        </div>
        <div className={styles.link}>
          <CustomLink text={t('menu:partners-menu-title')} href="/partners" />
        </div>
        <div className={styles.link}>
          <CustomLink text={t('menu:contacts-menu-title')} href="/contacts" />
        </div>
      </>
      )}

      {/* DESKTOP */}

      {!isMobile && (
      <>
        <div className={styles.link}>
          <CustomLink
            icon={<Image width='9' height='16' src={t('menu:menuSymbol')} alt="symbol" />}
            className={styles.linkStyle}
            text={t('menu:catalog-menu-title')}
            href="/catalog"
          />
        </div>
        <div className={styles.link}>
          <CustomLink
            icon={<Image width='9' height='16' src={t('menu:menuSymbol')} alt="symbol" />}
            className={styles.linkStyle}
            text={t('menu:company-menu-title')}
            href="/company"
          />
        </div>
        <div className={styles.link}>
          <CustomLink
            icon={<Image width='9' height='16' src={t('menu:menuSymbol')} alt="symbol" />}
            className={styles.linkStyle}
            text={t('menu:news-menu-title')}
            href="/news"
          />
        </div>
        <div className={styles.link}>
          <CustomLink
            icon={<Image
                width='9' height='16'
                src={t('menu:menuSymbol')}
                alt="symbol" />
                }
            className={styles.linkStyle}
            text={t('menu:partners-menu-title')}
            href="/partners"
          />
        </div>
        <div className={styles.link}>
          <CustomLink
            icon={<Image width='9' height='16' src={t('menu:menuSymbol')} alt="symbol" />}
            className={styles.linkStyle}
            text={t('menu:contacts-menu-title')}
            href="/contacts"
          />
        </div>
      </>
      )}
    </div>
  )
}
export default NavBar

import React, {useState} from 'react';
import cn from "classnames";

import style from '../../styles/Accordion.module.scss'
import {CheckBoxFormik} from "../CheckBoxFormik/CheckBoxFormik";
import Arrow2 from "../../public/icons/arrow2.svg"

const Accordion = () => {
    const [categoryOne, setCategoryOne] = useState(false);
    const [categorySecond, setCategorySecond] = useState(false);
    const [categoryThird, setCategoryThird] = useState(false);
    const [categoryFour, setCategoryFour] = useState(false);

    const openCategory = (value) => {
        if (value === 'one') {
            setCategoryOne(!categoryOne);
        }
        if (value === 'two') {
            setCategorySecond(!categorySecond);
        }
        if (value === 'three') {
            setCategoryThird(!categoryThird);
        }
        if (value === 'four') {
            setCategoryFour(!categoryFour);
        }
    }
    return (
        <div className={style.accordionContainer}>
            <ul id="accordeon">
                <li>
                    <p onClick={() => openCategory('one')} className={cn('head', style.title, categoryOne && style.moveSvg)}>
                        <span>Категории</span><Arrow2/>
                    </p>
                    <div className={cn("hidden", categoryOne && "active")}>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Лекарственные препараты'} name='health'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Биологически активные добавки'} name='activeHealth'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Пищевая продукция'} name='foodProd'/>
                    </div>
                </li>
                <li>
                    <p onClick={() => openCategory('two')} className={cn('head', style.title, categorySecond && style.moveSvg)}>
                        <span>Форма выпуска</span><Arrow2/>
                    </p>
                    <div className={cn("hidden", categorySecond && "active")}>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Порошки'} name='poroshki'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Таблетки'} name='tabletki'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Капсулы'} name='caps'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Сиропы'} name='sirop'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Плитки (батончики)'} name='baton'/>
                    </div>
                </li>
                <li>
                    <p onClick={() => openCategory('three')} className={cn('head', style.title, categoryThird && style.moveSvg)}>
                        <span>Область применения</span><Arrow2/>
                    </p>
                    <div className={cn("hidden", categoryThird && "active")}>
                        <CheckBoxFormik className={style.checkBoxSize} label={'С рецептом'} name='recept'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Без рецепта'} name='notRecept'/>
                    </div>
                </li>
                <li>
                    <p onClick={() => openCategory('four')} className={cn('head', style.title, categoryFour && style.moveSvg)}>
                        <span>Прочее</span><Arrow2/>
                    </p>
                    <div className={cn("hidden", categoryFour && "active")}>
                        <CheckBoxFormik className={style.checkBoxSize} label={'При сахарном диабете'} name='sugar'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Для детей'} name='child'/>
                        <CheckBoxFormik className={style.checkBoxSize} label={'Новинки'} name='new'/>
                    </div>
                </li>
            </ul>
        </div>
    );
};

export default Accordion;

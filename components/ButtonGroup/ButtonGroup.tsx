import React, {FC, ReactNode} from 'react';
import style from "../../styles/Item.module.scss";
import cn from "classnames";

type ButtonGroupTypes = {
    children: ReactNode
    styleContainer?: string
}

const ButtonGroup: FC<ButtonGroupTypes> = ({children, styleContainer}) => {
    return (
        <div className={cn(style.buttonsBlock, styleContainer)}>
            {children}
        </div>
    );
};

export default ButtonGroup;

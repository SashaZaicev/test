import React, {FC} from 'react';
import style from '../../styles/Heading.module.scss'
import cn from "classnames";

const Heading: FC<HeadingTypes> = ({tag, text, className, type = 'primary',}) => {
    const Tag = tag || 'h1'
    return (
        <Tag className={cn(style[type], style.mainTitle, className)}>
            {text}
        </Tag>
    );
};

export default Heading;

type HeadingTypes = {
    tag?: any
    text: string
    className?: string
    type?: 'primary' | 'secondary' | 'subPrimary'
}

import React, {FC} from 'react';
import cn from "classnames";

import style from '../../styles/Heading.module.scss'

const HeadingIcon: FC<HeadingIconTypes> = ({className, icon}) => {
  return (
    <div className={cn(style.mainTitle, className)}>
      {icon}
    </div>
  );
};

export default HeadingIcon;

type HeadingIconTypes = {
  className?: string
  icon?: any
}

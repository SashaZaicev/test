import React, {FC, useCallback, useEffect, useState} from 'react'
import cn from 'classnames'
import {hasCookie} from "cookies-next";

import style from '../../styles/SurveyAnketaBlock.module.scss'
import Button from "../Button";
import {Modal} from "../Modal/Modal";
import ModalSurvey from "../Modals/ModalSurvey";
import {useAppDispatch, useAppSelector} from "../../store/hooks";
import {selectQuizBtnSuc, selectQuizInfoSuc} from "../../store/ducks/common/selectors";
import {setQuizBtn, setQuizInfo} from "../../store/ducks/common/reducer";


const SurveyAnketaBlock: FC<SurveyAnketaBlockTypes> = ({
                                                         text,
                                                         colorText,
                                                         icon,
                                                         className,
                                                         classNameTextBlock,
                                                         backImage,
                                                         classNameBackImage,
                                                       }) => {
  const dispatch = useAppDispatch()

  const [isOpen, setIsOpen] = useState(false)
  const resultQuizInfo = useAppSelector(selectQuizInfoSuc)
  const resultQuizBtn = useAppSelector(selectQuizBtnSuc)

  useEffect(() => {
    hasCookie('QuizInfo') && setQuizBtn(true)
  }, [])

  const handleCloseIsOpenCallBack = useCallback(() => {
    setIsOpen(false)
    dispatch(setQuizInfo(false))
  }, [dispatch])
  console.log('resultQuizBtn', resultQuizBtn)
  return (
    <div className={style.surveyAnketa}>
      <div className={style.backImage_2}>
        <img className={style.bottomV4} src="/images/bottomV4.png" alt="bottomv4"/>
        <img className={style.bottomV4Mobile} src="/images/bottomv4Mobile.svg" alt=""/>
      </div>
      <div className={style.container}>
        <img className={style.bottomV4} src="/images/anketa.png" alt=""/>
        <img className={style.bottomV4Mobile} src="/images/mobileAnketa.jpg" alt=""/>
        <div className={cn(style.surveyAnketaBlockContainer, className)}>
          <img className={cn(style.backImg, classNameBackImage)} src={backImage} alt="backImage"/>
          <div className={style.iconBox}>
            <div className={style.icon}>{icon}</div>
          </div>
          <div className={cn(style.textBlock, classNameTextBlock)}>
            <span className={style.green}>{colorText}</span>
            <span className={style.description}>{text}</span>
          </div>
          {!resultQuizBtn
            ? <Button onClickFunc={() => setIsOpen(!isOpen)} className={style.btnSize}>Пройти опрос</Button>
            : <div className={style.quizLabel}><span className={style.green}>Вы</span> уже прошли опрос</div>
          }
        </div>
      </div>
      {isOpen &&
        <Modal close={handleCloseIsOpenCallBack}
               isOpen={isOpen}
               styleModalWindow={'modalSizeSurvey'}>
          <ModalSurvey/>
        </Modal>
      }
      {resultQuizInfo &&
        <Modal isOpen={resultQuizInfo} close={handleCloseIsOpenCallBack} styleModalWindow={'modalSizeSuc'}>
          <div className={'sucMsg'}><span>Спасибо</span> за Ваши ответы!</div>
        </Modal>
      }

    </div>
  )
}
export default SurveyAnketaBlock

interface SurveyAnketaBlockTypes {
  text: string
  colorText?: string
  icon?: any
  className?: string
  classNameTextBlock?: string
  backImage?: string
  classNameBackImage?: string
}

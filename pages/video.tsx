import Breadcrumbs from 'nextjs-breadcrumbs';
import React, {useEffect, useState} from 'react';
import cn from "classnames";
import ReactPaginate from "react-paginate";
import useTranslation from "next-translate/useTranslation";

import style from '../styles/VideoPage.module.scss'
import Test from "../public/images/test.svg"
import MainContainer from '../components/MainContainer/MainContainer';
import SurveyAnketaBlock from "../components/SurveyAnketaBlock";
import Heading from "../components/Heading";
import HeadingIcon from "../components/Heading/HeadingIcon";
import {Fancybox} from "../components/FancyBox/Fancybox";
import {useAppDispatch, useAppSelector} from "../store/hooks";
import {selectVideoList} from "../store/ducks/video/selectors";
import {getVideoList} from "../store/ducks/video/thunks";

const items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];


const VideoPage = () => {
  const {t} = useTranslation('breadcrumbs')
  const dispatch = useAppDispatch()

  const videoArray = useAppSelector(selectVideoList)
  const itemsPerPage = 2
  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);

  useEffect(() => {
    dispatch(getVideoList())
  }, [dispatch])

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(items.slice(itemOffset, endOffset));
    setPageCount(Math.ceil(items.length / itemsPerPage));
  }, [itemOffset, itemsPerPage]);

  const handlePageClick = (event) => {
    const newOffset = event.selected * itemsPerPage % items.length;
    setItemOffset(newOffset);
  };

  return (
    <MainContainer title="Видео | ekzon.by">
      <div className='breadcrumbsContainer'>
        <Breadcrumbs useDefaultStyle={false}
                     activeItemClassName="activeBreadcrumbs"
                     inactiveItemClassName="noActiveBreadcrumbs"
                     listClassName="listClassName"
                     rootLabel={t('breadcrumbs:home')}
                     transformLabel={(title) => t(`${title}`)}
        />
      </div>
      <div className={style.videoContainer}>
        <div className={style.container}>
          <div className={style.headerBox}>
            <div className={style.iconHeader}>
              <HeadingIcon icon={<svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M39.0628 3.90623H10.9378C10.0121 3.89789 9.09404 4.07406 8.2372 4.42446C7.38037 4.77486 6.60193 5.29246 5.94735 5.94704C5.29277 6.60162 4.77517 7.38006 4.42477 8.2369C4.07437 9.09374 3.89819 10.0118 3.90653 10.9375V39.0625C3.89819 39.9882 4.07437 40.9062 4.42477 41.7631C4.77517 42.6199 5.29277 43.3983 5.94735 44.0529C6.60193 44.7075 7.38037 45.2251 8.2372 45.5755C9.09404 45.9259 10.0121 46.1021 10.9378 46.0937H39.0628C39.9885 46.1021 40.9065 45.9259 41.7634 45.5755C42.6202 45.2251 43.3986 44.7075 44.0532 44.0529C44.7078 43.3983 45.2254 42.6199 45.5758 41.7631C45.9262 40.9062 46.1024 39.9882 46.094 39.0625V10.9375C46.1024 10.0118 45.9262 9.09374 45.5758 8.2369C45.2254 7.38006 44.7078 6.60162 44.0532 5.94704C43.3986 5.29246 42.6202 4.77486 41.7634 4.42446C40.9065 4.07406 39.9885 3.89789 39.0628 3.90623ZM44.5315 10.9375V11.7187H40.9534L44.0784 8.59373C44.4032 9.33114 44.5581 10.1321 44.5315 10.9375ZM43.1565 7.37498L38.7347 11.7187H33.1409L39.3909 5.46873C40.1105 5.50631 40.815 5.68901 41.4621 6.00586C42.1092 6.32271 42.6856 6.76716 43.1565 7.31248V7.37498ZM25.3284 11.7187L31.5784 5.46873H37.1722L30.9222 11.7187H25.3284ZM17.5159 11.7187L23.7659 5.46873H29.3597L23.1097 11.7187H17.5159ZM9.70341 11.7187L15.9534 5.46873H21.5472L15.2972 11.7187H9.70341ZM10.9378 5.46873H13.7347L7.48466 11.7187H5.46903V10.9375C5.46065 10.217 5.59639 9.50209 5.86825 8.83481C6.14011 8.16752 6.54262 7.56131 7.05212 7.05182C7.56162 6.54232 8.16783 6.13981 8.83511 5.86794C9.50239 5.59608 10.2173 5.46035 10.9378 5.46873ZM39.0628 44.5312H10.9378C10.2173 44.5396 9.50239 44.4039 8.83511 44.132C8.16783 43.8601 7.56162 43.4576 7.05212 42.9481C6.54262 42.4386 6.14011 41.8324 5.86825 41.1651C5.59639 40.4979 5.46065 39.783 5.46903 39.0625V13.2812H44.5315V39.0625C44.5399 39.783 44.4042 40.4979 44.1323 41.1651C43.8605 41.8324 43.4579 42.4386 42.9484 42.9481C42.4389 43.4576 41.8327 43.8601 41.1655 44.132C40.4982 44.4039 39.7833 44.5396 39.0628 44.5312Z"
                  fill="#F0F7F3"/>
                <path
                  d="M10.7032 41.4063C10.3961 41.4063 10.0921 41.3455 9.8086 41.2276C9.52511 41.1096 9.26777 40.9367 9.05137 40.7188C8.83498 40.501 8.66382 40.2425 8.54774 39.9582C8.43167 39.674 8.37297 39.3695 8.37503 39.0625V26.2344C8.37503 26.0272 8.29272 25.8285 8.14621 25.682C7.9997 25.5354 7.80098 25.4531 7.59378 25.4531C7.38658 25.4531 7.18787 25.5354 7.04135 25.682C6.89484 25.8285 6.81253 26.0272 6.81253 26.2344V39.0625C6.81047 39.5747 6.90959 40.0823 7.10419 40.5562C7.29879 41.03 7.58505 41.4608 7.94653 41.8237C8.30801 42.1866 8.73761 42.4746 9.21066 42.6711C9.68371 42.8676 10.1909 42.9688 10.7032 42.9688C10.9104 42.9688 11.1091 42.8864 11.2556 42.7399C11.4021 42.5934 11.4844 42.3947 11.4844 42.1875C11.4844 41.9803 11.4021 41.7816 11.2556 41.6351C11.1091 41.4886 10.9104 41.4063 10.7032 41.4063ZM14.8438 41.4063H13.3907C13.1835 41.4063 12.9847 41.4886 12.8382 41.6351C12.6917 41.7816 12.6094 41.9803 12.6094 42.1875C12.6094 42.3947 12.6917 42.5934 12.8382 42.7399C12.9847 42.8864 13.1835 42.9688 13.3907 42.9688H14.8438C15.051 42.9688 15.2497 42.8864 15.3962 42.7399C15.5427 42.5934 15.625 42.3947 15.625 42.1875C15.625 41.9803 15.5427 41.7816 15.3962 41.6351C15.2497 41.4886 15.051 41.4063 14.8438 41.4063ZM32.4532 25.7813L21.875 19.2813C21.5121 19.0586 21.0962 18.9369 20.6705 18.9288C20.2448 18.9207 19.8246 19.0265 19.4534 19.2352C19.0822 19.4439 18.7735 19.7479 18.5592 20.1159C18.3449 20.4838 18.2328 20.9023 18.2344 21.3281V34.375C18.2344 35.0132 18.4879 35.6252 18.9392 36.0765C19.3904 36.5277 20.0025 36.7813 20.6407 36.7813C21.0786 36.7859 21.5081 36.6609 21.875 36.4219L32.5157 29.9375C32.8646 29.7219 33.1526 29.4206 33.3523 29.0624C33.552 28.7041 33.6569 28.3008 33.6569 27.8906C33.6569 27.4805 33.552 27.0771 33.3523 26.7189C33.1526 26.3606 32.8646 26.0594 32.5157 25.8438L32.4532 25.7813ZM31.625 28.5313L21 35.0313C20.8735 35.1102 20.7273 35.1521 20.5782 35.1521C20.429 35.1521 20.2828 35.1102 20.1563 35.0313C20.0264 34.9581 19.9186 34.8513 19.8444 34.7221C19.7701 34.5928 19.7321 34.4459 19.7344 34.2969V21.3281C19.7315 21.1811 19.7694 21.0361 19.8439 20.9092C19.9184 20.7823 20.0265 20.6786 20.1563 20.6094C20.2852 20.5373 20.4305 20.4996 20.5782 20.5C20.7277 20.5012 20.8739 20.5446 21 20.625L31.625 27.1094C31.7487 27.1843 31.8509 27.2898 31.9221 27.4157C31.9932 27.5415 32.0308 27.6836 32.0313 27.8281C32.0345 27.9713 31.9983 28.1126 31.9267 28.2366C31.855 28.3606 31.7507 28.4625 31.625 28.5313Z"
                  fill="#F0F7F3"/>
              </svg>
              }/>
            </div>
            <Heading className={style.styleHeading} text="Видеоматериалы"
                     type="primary"/>
          </div>
          <div className={style.fancyBlockVideo}>
            <Fancybox options={{infinite: false}}>
              {Array.isArray(videoArray) && videoArray.map((image) => (
                <a key={image.id} className={style.video} href={image.videoURL}
                   data-fancybox="video">
                  <div className={style.imgBlockPosition}>
                    <div className={style.gradient}>
                      <img
                        className="inline"
                        width="500"
                        alt=""
                        src={image.imageURL}
                      />
                    </div>
                    <div className={style.box_eye}>
                      <div className={style.hover}>
                        <svg width="62" height="60" viewBox="0 0 62 60" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                          <path fillRule="evenodd" clipRule="evenodd"
                                d="M30.984 0C14.448 0 1 13.454 1 29.992C1 46.528 14.45 59.976 30.984 59.976C47.518 59.976 60.98 46.526 60.98 29.992C60.98 13.454 47.52 0 30.984 0ZM30.984 57.824C15.636 57.824 3.15198 45.344 3.15198 29.992C3.15198 14.64 15.636 2.15198 30.984 2.15198C46.336 2.15198 58.828 14.64 58.828 29.992C58.828 45.344 46.336 57.824 30.984 57.824ZM44.504 27.152L25.73 16.73C23.172 15.308 20.722 16.816 20.722 19.68V40.298C20.722 42.404 21.932 43.822 23.73 43.822C24.378 43.822 25.054 43.63 25.734 43.252L44.504 32.826C45.746 32.138 46.462 31.104 46.462 29.994C46.462 28.884 45.746 27.848 44.504 27.152ZM43.458 30.942L24.692 41.368C24.336 41.564 24.008 41.668 23.732 41.668C22.99 41.668 22.876 40.812 22.876 40.296V19.68C22.876 19.168 22.99 18.312 23.732 18.312C24.01 18.312 24.338 18.418 24.692 18.612L43.458 29.034C43.982 29.326 44.31 29.694 44.31 29.992C44.308 30.286 43.98 30.652 43.458 30.942Z"
                                fill="#F0F7F3"/>
                        </svg>
                      </div>
                      <div className={style.descr}>Смотреть видео</div>
                    </div>

                  </div>
                  <figcaption>
                    <p className={style.descriptionImage}>{image.description}</p>
                  </figcaption>
                </a>
              ))}
            </Fancybox>
          </div>
          <div
            className={cn(style.paginationBlock, style.paginationBlockMobile)}>
            <ReactPaginate
              nextLabel=""
              onPageChange={handlePageClick}
              pageRangeDisplayed={2}
              marginPagesDisplayed={1}
              pageCount={pageCount}
              previousLabel=""
              pageClassName="page-item"
              pageLinkClassName={style.page_link}
              previousClassName="page-item"
              previousLinkClassName="page-link"
              nextClassName="page-item"
              nextLinkClassName="page-link"
              breakLabel="..."
              breakClassName={style.breakDots}
              breakLinkClassName="page-link"
              containerClassName="pagination"
              activeClassName={style.active}
              renderOnZeroPageCount={null}
              className={style.pagination}
            />
          </div>
        </div>
        <SurveyAnketaBlock icon={<Test/>}
                           text=" в опросе, чтобы помочь нам совершенствоваться"
                           colorText="Примите участие"
                           backImage={"/images/anketav4.png"}
        />
      </div>
    </MainContainer>
  );
};

export default VideoPage;

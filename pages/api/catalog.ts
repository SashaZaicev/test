// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type {NextApiRequest, NextApiResponse} from 'next'
import {api} from "./index";
import {CatalogType} from "../../types/catalog";

export const catalogApi = {
  getCatalog() {
    return [
      {
        id: 1,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-1.jpg',
        newItem: true,
        recipe: true,
        child: true,
      },
      {
        id: 2,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-2.jpg',
        newItem: true,
        recipe: false,
        child: true,
      },
      {
        id: 3,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-3.jpg',
        newItem: true,
        recipe: false,
        child: true,
      },
      {
        id: 4,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-2.jpg',
        newItem: true,
        recipe: true,
        child: false,
      },
      {
        id: 5,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-1.jpg',
        newItem: false,
        recipe: false,
        child: false,
      },
      {
        id: 6,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-3.jpg',
        newItem: false,
        recipe: true,
        child: true,
      },
      {
        id: 7,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-2.jpg',
        newItem: true,
        recipe: true,
        child: false,
      },
      {
        id: 8,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-1.jpg',
        newItem: false,
        recipe: false,
        child: false,
      },
      {
        id: 9,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-3.jpg',
        newItem: false,
        recipe: true,
        child: true,
      },
      {
        id: 10,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-3.jpg',
        newItem: false,
        recipe: true,
        child: true,
      },
      {
        id: 11,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-2.jpg',
        newItem: true,
        recipe: true,
        child: false,
      },
      {
        id: 12,
        title: 'Vitax гепар липоевая кислота+витамины В2, В6, В9',
        description: 'Дополнительный источник витаминов А, С, D, Е, группы В (В1, В2, В3, В9)',
        image: '/images/slider2-1.jpg',
        newItem: false,
        recipe: false,
        child: false,
      },
    ]
    // return api.get<CatalogType[]>('/v1/catalog', {})
  },
}
//
// export default function handler(
//     req: NextApiRequest,
//     res: NextApiResponse<Data[]>
// ) {
//     res.status(200).json(catalog)
// }

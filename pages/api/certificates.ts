export const certificatesApi = {
  getCertificates() {
   return [
     {
       id: 1,
       description: 'Сертификат соответствия системы менеджмента качества на соответствие СТБ ISO 9001 - 2015',
       imageURL: '/images/certificate.jpg'
     },
     {
       id: 2,
       description: 'Сертификат соответствия СУОТ на соответствие СТБ 18001 - 2009',
       imageURL: '/images/certificate.jpg'
     },
     {
       id: 3,
       description: 'Сертификат соответствия системы HACCP на соответствие СТБ 1470 - 2012',
       imageURL: '/images/certificate.jpg'
     },
     {
       id: 4,
       description: 'Сертификат соответствия производства "Гематогена и сиропов" на соответствие Регуле №852 Европарламента',
       imageURL: '/images/certificate.jpg'
     },
     {
       id: 5,
       description: 'Международный сертификат на систему менеджмента безопасности пищевых продуктов в соответствии с требованиями ISO 22000:2005',
       imageURL: '/images/certificate.jpg'
     }
   ]
    // return api.get<CertificatesType[]>('/v1/certificates', {})
  }
}

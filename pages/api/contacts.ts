// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export const contactsApi = {
  getContactLeaderships() {
    return [
      {
        id: 1,
        name: 'Жур Дмитрий Сергеевич',
        position: 'Директор',
        contacts: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 2 00 04'},
          {id: 2, type: 'mail', label: 'Электронная почта', value: 'directo@ekzon.by'}
        ]
      },
      {
        id: 2,
        name: 'Рапинчук Александр Васильевич',
        position: 'Главный инженер',
        contacts: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 2 48 40'},
          {id: 2, type: 'mail', label: 'Электронная почта', value: 'rapinchuk@ekzon.by'}
        ]
      },
      {
        id: 3,
        name: 'Моргун Анатолий Евгеньевич',
        position: 'Зам. директора по ком. вопросам',
        contacts: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 3 25 43'},
          {id: 2, type: 'mail', label: 'Электронная почта', value: 'zamdirector@ekzon.by'}
        ]
      },
    ]
    // return api.get<NewsType[]>('/v1/news', {})
  },
  getContactsOther() {
    return [
      {
        id: 1, title: 'Отдел продаж', phones: [
          {id: 1, type: 'phone', label: 'Контактный телефон', value: '+375 1644 3 25 01'},
          {id: 2, type: 'phone', label: 'Контактный телефон', value: '+375 1644 3 02 01'},
          {id: 3, type: 'phone', label: 'тел./факс', value: '+375 1644 2 00 03'},
          {id: 4, type: 'phone', label: 'Контактный телефон', value: '+375 1644 2 00 02'},
          {id: 5, type: 'mail', label: 'Электронная почта', value: 'bm@ekzon.by'}
        ]
      },
      {
        id: 2, title: 'Сектор маркетинга', phones: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 2 00 05'},
          {id: 2, type: 'mail', label: 'Электронная почта', value: 'reklama@ekzon.by'}
        ]
      },
      {
        id: 3, title: 'Бюро регистрации', phones: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 2 12 71'},
          {id: 2, type: 'mail', label: 'Электронная почта', value: 'pto@ekzon.by'}
        ]
      },
      {
        id: 4, title: 'Производственно-техническая лаборатория', phones: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 3 24 58'},
          {id: 2, type: 'mail', label: 'Электронная почта', value: 'lab@ekzon.by'}
        ]
      },
      {
        id: 5, title: 'Бухгалтерия', phones: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 2 12 61'},
        ]
      },
      {
        id: 6, title: 'Управление обеспечения и контроля качества', phones: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 2 12 63'},
          {id: 2, type: 'mail', label: 'Электронная почта', value: 'quality@ekzon.by'}
        ]
      },
      {
        id: 7, title: 'Отдел материально-технического снабжения', phones: [
          {id: 1, type: 'phone', label: 'Приемная', value: '+375 1644 2 12 60'},
          {id: 2, type: 'mail', label: 'Электронная почта', value: 'os@ekzon.by'}
        ]
      },
    ]
    // return api.get<NewsType[]>('/v1/news', {})
  },
}
//
// export default function handler(
//     req: NextApiRequest,
//     res: NextApiResponse<Data[]>
// ) {
//     res.status(200).json(catalog)
// }

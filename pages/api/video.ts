export const videoApi = {
  getVideo() {
    return [
      {
        id: 1,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: '/images/1news.png'
      },
      {
        id: 2,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: '/images/1news.png'
      },
      {
        id: 3,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: '/images/2news.png'
      },
      {
        id: 4,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: '/images/2news.png'
      },
      {
        id: 5,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: '/images/1news.png'
      },
      {
        id: 6,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: '/images/1news.png'
      },
      {
        id: 7,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: '/images/1news.png'
      },
      {
        id: 8,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: '/images/1news.png'
      },
      {
        id: 9,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est, aenean sit enim integer. Vel vestibulum netus suspendisse.',
        videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        imageURL: 'https://media.w3.org/2010/05/sintel/poster.png'
      },
    ]
    // return api.get<VideoType[]>('/v1/video', {})
  }
}

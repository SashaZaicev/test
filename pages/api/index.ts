import axios, {Method, ResponseType} from 'axios'
import {keysToCamelCase, keysToSnakeCase} from "../../lib/utils";

type CallApiType = {
  url: string
  data?: object
  params?: object
  headers?: object
  responseType?: ResponseType
  method: Method
}

const callApi = async ({
                         url, method, data, params = {}, headers = {}, responseType,
                       }: CallApiType) => {
  const config = {
    url,
    method,
    params: keysToSnakeCase(params),
    data: keysToSnakeCase(data),
    responseType,
    baseURL: `${process.env.REACT_APP_API_URL}/api`,
    headers: {
      ...axios.defaults.headers.common,
      'Content-Type': 'application/json',
      ...headers,
    },
  }

  try {
    const response = await axios(config)
    return keysToCamelCase(response.data)
  } catch (err) {
    // @ts-ignore
    if (err?.response?.status === 401) {
      // await getNewToken()

      /*  return new Promise((resolve, reject) => {
          axios.request(err.config).then((response) => {
            resolve(keysToCamelCase(response.data))
          }).catch((error) => {
            if (err?.response?.status === 401) {
              store.dispatch(logout())
            }
            reject(error)
          })
        }) */
    }
    return Promise.reject(err)
  }
}

export const api = {
  async get<T = any>(url: string, params?: object, headers?: object, responseType?: ResponseType): Promise<T> {
    return callApi({
      url, params, headers, responseType, method: 'GET',
    })
  },
  async post<T = any>(url: string, data?: object, params?: object, headers?: object): Promise<T> {
    return callApi({
      url, data, params, headers, method: 'POST',
    })
  },
  async put<T = any>(url: string, data?: object, params?: object, headers?: object): Promise<T> {
    return callApi({
      url, data, params, headers, method: 'PUT',
    })
  },
  async del<T = any>(url: string, params?: object, headers?: object): Promise<T> {
    return callApi({
      url, params, headers, method: 'DELETE',
    })
  },
}

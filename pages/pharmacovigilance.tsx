import React, {useState} from 'react';
import useTranslation from "next-translate/useTranslation";
import Breadcrumbs from "nextjs-breadcrumbs";
import style from "../styles/PharmacovigilanceContainer.module.scss";
import MainContainer from "../components/MainContainer";
import InfoField from "../components/InfoField/InfoField";

import Location from '../public/icons/location_ico.svg'
import Phone from '../public/icons/phone.svg'
import Mail from '../public/icons/mail.svg'
import Manager from '../public/icons/manager_ico.svg'
import Button from "../components/Button";
import cn from "classnames";
import PharmaFormFirst from "../components/PharmacovigilanceForm/pharmaFormFirst";
import PharmaFormSecond from "../components/PharmacovigilanceForm/pharmaFormSecond";

const pharma = {
  mainImage: {
    id: 1,
    imageUrl: '/images/mobile-farm.jpg'
  }
}

enum ButtonsType {
  specialist = 1,
  consumer = 2,
}

const buttons = [
  {
    title: 'specialist',
    type: ButtonsType.specialist
  },
  {
    title: 'consumer',
    type: ButtonsType.consumer
  },
]


const Pharmacovigilance = () => {
  const {t} = useTranslation('breadcrumbs')
  const [btnActive, setBtnActive] = useState<ButtonsType>(ButtonsType.specialist)

  console.log('btnActive', btnActive)
  return (
    <MainContainer title="Фармаконадзор | ekzon.by">
      <div className='breadcrumbsContainer'>
        <Breadcrumbs useDefaultStyle={false}
                     activeItemClassName="activeBreadcrumbs"
                     inactiveItemClassName="noActiveBreadcrumbs"
                     listClassName="listClassName"
                     rootLabel={t('breadcrumbs:home')}
                     transformLabel={(title) => t(`${title}`)}
        />
      </div>
      <div className={style.pharmacovigilanceContainer}>
        <div className={style.container}>
          <div className={style.pharmacovigilanceContainer_inner}>
            <div className={style.mainImage}
                 style={{backgroundImage: `url(${pharma.mainImage.imageUrl})`}}>
              <div className={style.pharmaTitle}>
                Фармаконадзор
              </div>
            </div>
            <div className={style.mainBlock}>
              <div className={style.mainBlock_textBlock}>
                {pharmacovigilanceArray.text.map((textEl) => (
                  <p key={textEl.id}>{textEl.text}</p>
                ))}
                <p>Перед заполнением формы просим Вас ознакомиться с <a href="">терминами и
                  определениями</a>т из ТКП
                  564-2015 (33050).</p>
              </div>
              <div className={style.mainBlock_contactBlock}>
                <div className={style.line}/>
                <div className={style.text}>
                  <div className={style['mainBlock_contactBlock-title']}>Контакты службы
                    фармаконадзора
                    ОАО «Экзон»
                  </div>
                  <div className={style.contactPharma}>
                    <InfoField
                      icon={<Location/>}
                      title={'225612, Республика Беларусь, Брестская обл., г.Дрогичин, ул.Ленина, 202'}
                      label={'Находимся по адресу'}
                      styleCustomIcon={style.infoIcon}
                      styleCustomTitle={style.infoTitle}
                      styleCustomLabel={style.infoLabel}
                      styleCustomContainer={style.infoContainer}
                    />
                    <InfoField
                      icon={<Phone/>}
                      title={'+375 1644 2 00 04'}
                      tel={'+375 1644 2 00 04'}
                      type={'phone'}
                      label={'Горячая линия'}
                      styleCustomIcon={style.infoIcon}
                      styleCustomTitle={style.infoTitlePhone}
                      styleCustomLabel={style.infoLabel}
                      styleCustomContainer={style.infoContainer}
                    />
                    <InfoField
                      icon={<Mail/>}
                      title={'pharmaconadzor@ekzon.by'}
                      mail={'pharmaconadzor@ekzon.by'}
                      type={'mail'}
                      label={'E-mail'}
                      styleCustomIcon={style.infoIcon}
                      styleCustomTitle={style.infoTitlePhone}
                      styleCustomLabel={style.infoLabel}
                      styleCustomContainer={style.infoContainer}
                    />
                    <InfoField
                      icon={<Manager/>}
                      title={'Анищенко Раиса Александровна'}
                      label={'Ведущий специалист фармаконадзора предприятия'}
                      styleCustomIcon={style.infoIcon}
                      styleCustomTitle={style.infoTitle}
                      styleCustomLabel={style.infoLabel}
                      styleCustomContainer={style.infoContainer}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={style.formBlock}>
          <div className={style.container}>
            <div className={style.formBlock_inner}>
              <div className={style.title}>Сообщить о проблемах, связанных с применением препарата</div>
              <div className={style.description}>Вы можете заполнить электронную форму сообщения о
                нежелательной реакции/отсутствии терапевтической эффективности.
              </div>
              <div className={style.btnBlock}>
                <Button
                  onClickFunc={() => {
                    setBtnActive(1)
                  }}
                  type={'none'}
                  className={cn(style.btnSize, btnActive === 1 && style.formBtnActive)}>
                  Специалист
                </Button>
                <Button
                  onClickFunc={() => {
                    setBtnActive(2)
                  }}
                  type={'none'}
                  className={cn(style.btnSize, btnActive === 2 && style.formBtnActive)}>
                  Потребитель
                </Button>
              </div>
              {
                btnActive === 1
                  ? <PharmaFormFirst/>
                  : <PharmaFormSecond/>
              }
            </div>
          </div>
        </div>
      </div>
    </MainContainer>
  )
    ;
};

export default Pharmacovigilance;
const pharmacovigilanceArray = {
  text: [
    {
      id: 1,
      text: 'Фармаконадзор (pharmacovigilance) - совокупность мероприятий, связанных с научными\n' +
        '                                исследованиями и деятельностью, направленными на выявление, оценку и понимание возможных\n' +
        '                                негативных последствий медицинского применения лекарственных препаратов, предупреждение\n' +
        '                                их возникновения и защиту пациентов. Для нас одной из ключевых задач является\n' +
        '                                обеспечение потребителей качественными, эффективными и безопасными лекарственными\n' +
        '                                средствами, польза от применения которых превышает возможные риски.\n' +
        '                            '
    },
    {
      id: 2,
      text: `Порядок осуществления деятельности по фармаконадзору устанавливает Технический кодекс
            установившейся практики ТКП 564-2015 (33050) «НАДЛЕЖАЩАЯ ПРАКТИКА ФАРМАКОНАДЗОРА. Good
            Pharmacovigilance Practice (GVP)», который введен в Республике Беларусь впервые и
            вступил в действие 15.08.2015.`
    },
    {
      id: 3,
      text: `Для осуществления мониторинга безопасности и эффективности наших лекарств мы создали
            отдел «Фармаконадзора». В соответствии с международными нормами ответственность за
            безопасность выпускаемых лекарственных средств несет производитель/держатель
            регистрационного удостоверения, поэтому он обязан осуществлять постоянный контроль за
            безопасностью своей продукции и с учетом новой информации регулярно проводить повторную
            оценку соотношения польза/риск.`
    },
    {
      id: 4,
      text: `Приоритетными задачами службы Фармаконадзора ОАО «ЭКЗОН» является предотвращение
            нежелательных последствий применения зарегистрированных лекарственных средств нашего
            производства, содействие защите здоровья пациентов и общественного здоровья. Именно поэтому
            сбор информации, связанной с безопасностью и эффективностью выпускаемых нами лекарственных
            средств – важная часть нашей работы.`
    },
    {
      id: 5,
      text: `При выявлении побочных явлений наши специалисты ищут причину их возникновения, а также
            делают все необходимое, чтобы минимизировать и предотвратить возникновение побочных явлений
            в будущем. Мы будем благодарны за предоставление любой информации по выявлению нетипичных
            проявлений или побочных явлений, или отсутствия эффективности при применении лекарственных
            средств нашего производства.`
    },
    {
      id: 6,
      text: `Если у Вас есть информация относительно возникшей (подозреваемой) нежелательной реакции
                                или отсутствии эффективности при применении лекарственных средств производства ОАО
                                «ЭКЗОН», просим Вас сообщить нам об этом по указанному телефону и электронной почте
                                и/или заполнив форму по ссылкам ниже.`
    },
    {
      id: 7,
      text: `Мы гарантируем соблюдение конфиденциальности информации, направленной Вами, за
            исключением случаев, установленных законодательством.`
    },
  ]
}

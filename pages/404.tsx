import React from 'react';
import useTranslation from "next-translate/useTranslation";
import Button from "../components/Button";
import style from '../styles/Error404.module.scss'
import MainContainer from "../components/MainContainer";
import Link from "next/link";

const Error = () => {
    const {t} = useTranslation('common')

    return (
        <MainContainer title="Ошибка 404 | ekzon.by">

            <div className={style.errorContainer}>
                <div className={style.container}>
                    <div className={style.image}>404</div>
                    <div className={style.title_404}>{t('error404')}</div>
                    <Link href='/'>
                        <a>
                            <Button className={style.btnSize}>Вернуться на главную</Button>
                        </a>
                    </Link>
                </div>
            </div>
        </MainContainer>
    );
};

export default Error;

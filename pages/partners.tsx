import React from 'react';
import style from '../styles/Partners.module.scss'
import useTranslation from "next-translate/useTranslation";
import Breadcrumbs from "nextjs-breadcrumbs";
import MainContainer from '../components/MainContainer';
import SurveyAnketaBlock from '../components/SurveyAnketaBlock';
import {Fancybox} from "../components/FancyBox/Fancybox";
import Test from "../public/images/test.svg";
import {patt} from '../lib/regex';

const arraySingleNews = {
  // news: {
  //     id: 1,
  //     description: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
  //     title: "Коллектив ОАО «ЭКЗОН» принял участие в областных соревнованиях санитарных дружин.",
  //     date: '24.04.2022',
  //     // imgUrl: '/images/3news.png'
  //     imgUrl: '/images/singleNewsBig.png'
  // },
  mainImage: {
    id: 1,
    imageUrl: '/images/mobile-farm.jpg'
  },
  imagesH4: [
    {
      id: 1,
      imageURL: '/images/h4.jpg'
    },
    {
      id: 2,
      imageURL: '/images/h4.jpg'
    },
    {
      id: 3,
      imageURL: '/images/h4.jpg'
    }
  ],
  imagesH6: [
    {
      id: 1,
      imageURL: '/images/h4.jpg'
    },
    {
      id: 2,
      imageURL: '/images/h4.jpg'
    },
    {
      id: 3,
      videoURL: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
      imageURL: '/images/partnersImage.png'
    }
  ],
}

const Partners = () => {
  const {t} = useTranslation('breadcrumbs')

  return (
    <MainContainer title="Партнерам | ekzon.by">
      <div className='breadcrumbsContainer'>
        <Breadcrumbs useDefaultStyle={false}
                     activeItemClassName="activeBreadcrumbs"
                     inactiveItemClassName="noActiveBreadcrumbs"
                     listClassName="listClassName"
                     rootLabel={t('breadcrumbs:home')}
                     transformLabel={(title) => t(`${title}`)}
        />
      </div>
      <div className={style.partnersContainer}>
        <div className={style.container}>
          <div className={style.partnersContainer_inner}>
            <div className={style.mainImage}
                 style={{backgroundImage: `url(${arraySingleNews.mainImage.imageUrl})`}}>
              <div className={style.partnersTitle}>
                Партнерам
              </div>
            </div>
            <div className={style.mainBlock}>
              <div className={style.mainBlock_left}>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Ultricies felis vitae sit porttitor eu. Vehicula tristique
                  quis
                  sapien quis dolor vitae fermentum viverra tortor. Ornare
                  habitant aliquam non iaculis. Augue ornare tincidunt sed
                  suspendisse enim. Massa, suscipit urna, faucibus diam non quis
                  sapien lorem vitae. Ut ornare <a href={'#'}>cсылка на другую
                    страницу</a>, tellus
                  pellentesque. Imperdiet orci, elit at diam quis fringilla nunc
                  eu. Venenatis, pellentesque in scelerisque potenti sed
                  accumsan
                  accumsan mauris. Molestie orci enim lorem accumsan parturient
                  blandit vulputate. Maecenas in eget at ullamcorper nibh. In
                  dolor quis in id turpis sed. Vitae eget et orci velit arcu
                  suspendisse viverra gravida viverra. Vulputate sit
                  pellentesque
                  pellentesque egestas.
                </p>
                <p>
                  Varius et proin ornare sagittis. Nibh at parturient id tellus
                  sit ultrices massa enim bibendum. Iaculis convallis donec
                  amet,
                  eget diam vulputate. Tristique tellus sed proin aliquet at vel
                  cursus risus. Massa, dui nisi feugiat enim viverra accumsan
                  sapien auctor. Tellus et praesent ullamcorper nec urna donec.
                  Morbi sagittis, diam ullamcorpe</p>
                <h2>Заголовок необходимого абзаца / h2</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Ultricies felis vitae sit porttitor eu. Vehicula tristique
                  quis
                  sapien quis dolor vitae fermentum viverra tortor. Ornare
                  habitant aliquam non iaculis. Augue ornare tincidunt sed
                  suspendisse enim. Massa, suscipit urna, faucibus diam non quis
                  sapien lorem vitae. Ut ornare <a href={'#'}>cсылка на другую
                    страницу</a>, tellus
                  pellentesque. Imperdiet orci, elit at diam quis fringilla nunc
                  eu. Venenatis, pellentesque in scelerisque potenti sed
                  accumsan
                  accumsan mauris. Molestie orci enim lorem accumsan parturient
                  blandit vulputate. Maecenas in eget at ullamcorper nibh. In
                  dolor quis in id turpis sed. Vitae eget et orci velit arcu
                  suspendisse viverra gravida viverra. Vulputate sit
                  pellentesque
                  pellentesque egestas.
                </p>
                <p>
                  Varius et proin ornare sagittis. Nibh at parturient id tellus
                  sit ultrices massa enim bibendum. Iaculis convallis donec
                  amet,
                  eget diam vulputate. Tristique tellus sed proin aliquet at vel
                  cursus risus. Massa, dui nisi feugiat enim viverra accumsan
                  sapien auctor. Tellus et praesent ullamcorper nec urna donec.
                  Morbi sagittis, diam ullamcorpe</p>
                <span>Специальные возможности</span>
                <ul>
                  <li>Перечень в виде списка с маркером и дополнительной
                    ссылкой.
                  </li>
                  <li>Перечень в виде списка с маркером и дополнительной
                    ссылкой.
                  </li>
                  <li>Перечень в виде списка с маркером и дополнительной
                    ссылкой.
                  </li>
                </ul>
                <h3>Заголовок необходимого абзаца / h3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Ultricies felis vitae sit porttitor eu. Vehicula tristique
                  quis
                  sapien quis dolor vitae fermentum viverra tortor. Ornare
                  habitant aliquam non iaculis. Augue ornare tincidunt sed
                  suspendisse enim. Massa, suscipit urna, faucibus diam non quis
                  sapien lorem vitae. Ut ornare <a href={'#'}>cсылка на другую
                    страницу</a>, tellus
                  pellentesque. Imperdiet orci, elit at diam quis fringilla nunc
                  eu. Venenatis, pellentesque in scelerisque potenti sed
                  accumsan
                  accumsan mauris. Molestie orci enim lorem accumsan parturient
                  blandit vulputate. Maecenas in eget at ullamcorper nibh. In
                  dolor quis in id turpis sed. Vitae eget et orci velit arcu
                  suspendisse viverra gravida viverra. Vulputate sit
                  pellentesque
                  pellentesque egestas.
                </p>
                <p>
                  Varius et proin ornare sagittis. Nibh at parturient id tellus
                  sit ultrices massa enim bibendum. Iaculis convallis donec
                  amet,
                  eget diam vulputate. Tristique tellus sed proin aliquet at vel
                  cursus risus. Massa, dui nisi feugiat enim viverra accumsan
                  sapien auctor. Tellus et praesent ullamcorper nec urna donec.
                  Morbi sagittis, diam ullamcorpe</p>
                <span>Специальные возможности</span>
                <ol>
                  <li>Перечень в виде списка с маркером и <a>дополнительной
                    ссылкой.</a>
                  </li>
                  <li>Перечень в виде списка с маркером и <a>дополнительной
                    ссылкой.</a>
                  </li>
                  <li>Перечень в виде списка с маркером и <a>дополнительной
                    ссылкой.</a>
                  </li>
                </ol>
                <h4>Заголовок необходимого абзаца / h4</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  arcu suspendisse viverra gravida viverra. Vulputate sit
                  Ultricies felis vitae sit porttitor eu. Vehicula tristique
                  quis sapien quis dolor vitae fermentum viverra tortor. Ornare
                  habitant aliquam non iaculis. Augue ornare tincidunt sed
                  suspendisse enim. Massa, suscipit urna, faucibus diam non quis
                  sapien lorem vitae. Ut ornare venenatis sagittis vel, tellus
                  pellentesque. Imperdiet orci, elit at diam quis fringilla nunc
                  eu. Venenatis, pellentesque in scelerisque potenti sed
                  accumsan accumsan mauris. Molestie orci enim lorem accumsan
                  parturientblandit vulputate. Maecenas in eget at ullamcorper
                  nibh. In dolor quis in id turpis sed. Vitae eget et orci velit
                  pellentesque pellentesque egestas.</p>
                <div className={style.fancyBlock}>
                  <Fancybox options={{infinite: false}}>
                    {arraySingleNews.imagesH4.map((image) => (
                      <a key={image.id} data-fancybox="galleryH4" href={image.imageURL}>
                        <img alt="" src={image.imageURL}/>
                        <div className={style.box_eye}>
                          <div className={style.hover}>
                            <svg width="28" height="28" viewBox="0 0 28 28"
                                 xmlns="http://www.w3.org/2000/svg">
                              <path
                                d="M25.6704 20.9429L21.1856 16.4581C20.628 15.9005 19.7456 15.8666 19.1395 16.3466L18.165 15.372C19.0183 14.0145 19.5177 12.4193 19.5177 10.7029C19.5177 5.83991 15.5613 1.88354 10.6983 1.88354C5.83527 1.88354 1.87891 5.83506 1.87891 10.6981C1.87891 15.5611 5.83527 19.5175 10.6983 19.5175C12.4147 19.5175 14.0098 19.0181 15.3674 18.1648L16.3419 19.1393C15.8619 19.7454 15.8959 20.6229 16.4535 21.1854L20.9383 25.6702C21.2389 25.9708 21.6365 26.1211 22.0292 26.1211C22.4219 26.1211 22.8195 25.9708 23.1201 25.6702L25.6656 23.1248C25.9565 22.8338 26.1165 22.446 26.1165 22.0338C26.1165 21.6217 25.9613 21.2338 25.6704 20.9429ZM2.8486 10.6981C2.8486 6.36839 6.3686 2.84839 10.6983 2.84839C15.028 2.84839 18.548 6.36839 18.548 10.6981C18.548 15.0278 15.028 18.5478 10.6983 18.5478C6.3686 18.5478 2.8486 15.0278 2.8486 10.6981ZM17.5977 16.172L18.4413 17.0157L17.0159 18.4411L16.1722 17.5975C16.7007 17.1805 17.1807 16.7005 17.5977 16.172ZM24.9819 22.4363L22.4365 24.9817C22.2135 25.2048 21.8498 25.2048 21.6268 24.9817L17.1419 20.4969C16.9189 20.2738 16.9189 19.9102 17.1419 19.6872L19.6874 17.1417C19.7989 17.0302 19.9444 16.972 20.0947 16.972C20.2401 16.972 20.3904 17.0302 20.5019 17.1417L24.9868 21.6266C25.0934 21.7332 25.1565 21.8787 25.1565 22.0338C25.1565 22.189 25.0935 22.3296 24.9819 22.4363Z"/>
                              <path
                                d="M13.3602 10.2133H11.1833V8.03636C11.1833 7.7697 10.9651 7.55151 10.6984 7.55151C10.4318 7.55151 10.2136 7.7697 10.2136 8.03636V10.2133H8.03661C7.76994 10.2133 7.55176 10.4315 7.55176 10.6982C7.55176 10.9648 7.76994 11.183 8.03661 11.183H10.2136V13.36C10.2136 13.6267 10.4318 13.8448 10.6984 13.8448C10.9651 13.8448 11.1833 13.6267 11.1833 13.36V11.183H13.3602C13.6269 11.183 13.8451 10.9648 13.8451 10.6982C13.8451 10.4315 13.6269 10.2133 13.3602 10.2133Z"/>
                            </svg>
                          </div>
                        </div>
                      </a>
                    ))}
                  </Fancybox>
                </div>
                <h5>Заголовок необходимого абзаца / h5</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Ultricies felis vitae sit porttitor eu. Vehicula tristique
                  quis sapien quis dolor vitae fermentum viverra tortor. Ornare
                  habitant aliquam non iaculis. Augue ornare tincidunt sed
                  suspendisse enim. Massa, suscipit urna, faucibus diam non quis
                  sapien lorem vitae.</p>
                <img className={style.imageH5} src="/images/partnersImage.png" alt=""/>
                {/*<img className={style.mobileImageH5} src="/images/partnersImage.png" alt=""/>*/}
                <h6>Заголовок необходимого абзаца / h6</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ultricies felis vitae sit
                  porttitor eu. Vehicula tristique quis sapien quis dolor vitae fermentum viverra
                  tortor. Ornare habitant aliquam non iaculis. Augue ornare tincidunt sed suspendisse
                  enim. Massa, suscipit urna, faucibus diam non quis sapien lorem vitae.</p>
                <div className={style.fancyBlockVideo}>
                  <Fancybox options={{infinite: false}}>
                    {arraySingleNews.imagesH6.map((image) => {
                      return (image.imageURL.match(patt).toString() === '.jpg')
                        ? (<a className={style.image} key={image.id} data-fancybox="galleryH6"
                              href={image.imageURL}>
                          <img alt="" src={image.imageURL}/>
                          <div className={style.box_eye}>
                            <div className={style.hover}>
                              <svg width="28" height="28" viewBox="0 0 28 28"
                                   xmlns="http://www.w3.org/2000/svg">
                                <path
                                  d="M25.6704 20.9429L21.1856 16.4581C20.628 15.9005 19.7456 15.8666 19.1395 16.3466L18.165 15.372C19.0183 14.0145 19.5177 12.4193 19.5177 10.7029C19.5177 5.83991 15.5613 1.88354 10.6983 1.88354C5.83527 1.88354 1.87891 5.83506 1.87891 10.6981C1.87891 15.5611 5.83527 19.5175 10.6983 19.5175C12.4147 19.5175 14.0098 19.0181 15.3674 18.1648L16.3419 19.1393C15.8619 19.7454 15.8959 20.6229 16.4535 21.1854L20.9383 25.6702C21.2389 25.9708 21.6365 26.1211 22.0292 26.1211C22.4219 26.1211 22.8195 25.9708 23.1201 25.6702L25.6656 23.1248C25.9565 22.8338 26.1165 22.446 26.1165 22.0338C26.1165 21.6217 25.9613 21.2338 25.6704 20.9429ZM2.8486 10.6981C2.8486 6.36839 6.3686 2.84839 10.6983 2.84839C15.028 2.84839 18.548 6.36839 18.548 10.6981C18.548 15.0278 15.028 18.5478 10.6983 18.5478C6.3686 18.5478 2.8486 15.0278 2.8486 10.6981ZM17.5977 16.172L18.4413 17.0157L17.0159 18.4411L16.1722 17.5975C16.7007 17.1805 17.1807 16.7005 17.5977 16.172ZM24.9819 22.4363L22.4365 24.9817C22.2135 25.2048 21.8498 25.2048 21.6268 24.9817L17.1419 20.4969C16.9189 20.2738 16.9189 19.9102 17.1419 19.6872L19.6874 17.1417C19.7989 17.0302 19.9444 16.972 20.0947 16.972C20.2401 16.972 20.3904 17.0302 20.5019 17.1417L24.9868 21.6266C25.0934 21.7332 25.1565 21.8787 25.1565 22.0338C25.1565 22.189 25.0935 22.3296 24.9819 22.4363Z"/>
                                <path
                                  d="M13.3602 10.2133H11.1833V8.03636C11.1833 7.7697 10.9651 7.55151 10.6984 7.55151C10.4318 7.55151 10.2136 7.7697 10.2136 8.03636V10.2133H8.03661C7.76994 10.2133 7.55176 10.4315 7.55176 10.6982C7.55176 10.9648 7.76994 11.183 8.03661 11.183H10.2136V13.36C10.2136 13.6267 10.4318 13.8448 10.6984 13.8448C10.9651 13.8448 11.1833 13.6267 11.1833 13.36V11.183H13.3602C13.6269 11.183 13.8451 10.9648 13.8451 10.6982C13.8451 10.4315 13.6269 10.2133 13.3602 10.2133Z"/>
                              </svg>
                            </div>
                          </div>
                        </a>)
                        : (<a key={image.id} className={style.video} href={image.videoURL}
                              data-fancybox="galleryH6">
                          <img
                            className="inline"
                            width="500"
                            alt=""
                            src={image.imageURL}
                          />
                          <div className={style.box_eye}>
                            <div className={style.hover}>
                              <svg width="62" height="60" viewBox="0 0 62 60" fill="none"
                                   xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd"
                                      d="M30.984 0C14.448 0 1 13.454 1 29.992C1 46.528 14.45 59.976 30.984 59.976C47.518 59.976 60.98 46.526 60.98 29.992C60.98 13.454 47.52 0 30.984 0ZM30.984 57.824C15.636 57.824 3.15198 45.344 3.15198 29.992C3.15198 14.64 15.636 2.15198 30.984 2.15198C46.336 2.15198 58.828 14.64 58.828 29.992C58.828 45.344 46.336 57.824 30.984 57.824ZM44.504 27.152L25.73 16.73C23.172 15.308 20.722 16.816 20.722 19.68V40.298C20.722 42.404 21.932 43.822 23.73 43.822C24.378 43.822 25.054 43.63 25.734 43.252L44.504 32.826C45.746 32.138 46.462 31.104 46.462 29.994C46.462 28.884 45.746 27.848 44.504 27.152ZM43.458 30.942L24.692 41.368C24.336 41.564 24.008 41.668 23.732 41.668C22.99 41.668 22.876 40.812 22.876 40.296V19.68C22.876 19.168 22.99 18.312 23.732 18.312C24.01 18.312 24.338 18.418 24.692 18.612L43.458 29.034C43.982 29.326 44.31 29.694 44.31 29.992C44.308 30.286 43.98 30.652 43.458 30.942Z"
                                      fill="#F0F7F3"/>
                              </svg>
                            </div>
                            <div className={style.descr}>Смотреть видео</div>
                          </div>

                        </a>)

                    })}
                  </Fancybox>
                </div>
              </div>
            </div>
          </div>
          <SurveyAnketaBlock icon={<Test/>}
                             text=" в опросе, чтобы помочь нам совершенствоваться"
                             colorText="Примите участие"
                             backImage={"/images/anketav4.png"}
          />
        </div>
      </div>
    </MainContainer>
  );
};

export default Partners;

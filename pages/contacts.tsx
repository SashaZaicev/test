import React, {useEffect} from 'react';
import useTranslation from "next-translate/useTranslation";
import Breadcrumbs from "nextjs-breadcrumbs";

import Location from "../public/icons/location_ico.svg";
import LocationSmall from "../public/icons/location30.svg";
import Phone from "../public/icons/phone.svg";
import Mail from "../public/icons/mail.svg";
import Web from "../public/icons/website.svg";
import style from "../styles/Contacts.module.scss";

import MainContainer from "../components/MainContainer";
import Heading from "../components/Heading";
import InfoField from "../components/InfoField/InfoField";
import {useAppDispatch, useAppSelector} from "../store/hooks";
import {selectLeadershipsList, selectOtherList} from "../store/ducks/contacts/selectors";
import {getContactLeadershipsList, getContactsOtherList} from "../store/ducks/contacts/thunks";

const Contacts = () => {
  const {t} = useTranslation('breadcrumbs')
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(getContactLeadershipsList())
    dispatch(getContactsOtherList())
  }, [dispatch])

  const contactLeaderships = useAppSelector(selectLeadershipsList);
  const contactOther = useAppSelector(selectOtherList);


  return (
    <MainContainer title="Контакты | ekzon.by">
      <div className='breadcrumbsContainer'>
        <Breadcrumbs useDefaultStyle={false}
                     activeItemClassName="activeBreadcrumbs"
                     inactiveItemClassName="noActiveBreadcrumbs"
                     listClassName="listClassName"
                     rootLabel={t('breadcrumbs:home')}
                     transformLabel={(title) => t(`${title}`)}
        />
      </div>
      <div className={style.contactsContainer}>
        <div className={style.container}>
          <Heading className={style.styleHeading} text="Контакты"
                   type="primary"/>
          <div className={style.contactsContainer_inner}>
            <div className={style.mapContainer}>
              <div className={style.mapContainer_description}>
                <div className={style.subName}>
                  <div className={style.subName_title}>ОАО «ЭКЗОН»</div>
                  <InfoField
                    icon={<Location/>}
                    title={'225612, Республика Беларусь, Брестская обл., г.Дрогичин, ул.Ленина, 202'}
                    label={'Находимся по адресу'}
                    styleCustomIcon={style.infoIcon}
                    styleCustomTitle={style.infoTitle}
                    styleCustomLabel={style.infoLabel}
                    styleCustomContainer={style.infoContainer}
                  />
                  <InfoField
                    icon={<Phone/>}
                    title={'+375 1644 2 00 04'}
                    tel={'+375 1644 2 00 04'}
                    type={'phone'}
                    label={'Приемная'}
                    styleCustomIcon={style.infoIcon}
                    styleCustomTitle={style.infoTitlePhone}
                    styleCustomLabel={style.infoLabel}
                    styleCustomContainer={style.infoContainer}
                  />
                </div>
                <div className={style.workTime}>
                  <div className={style.workTime_title}>Режим работы</div>
                  <InfoField
                    title={'С 08:00 до 17:00'}
                    label={'Понедельник-пятница:'}
                    styleCustomTitle={style.infoTitle}
                    styleCustomLabel={style.infoLabel}
                    styleCustomContainer={style.infoContainer}
                  />
                  <InfoField
                    title={'С 12:00 до 13:00'}
                    label={'Обеденный перерыв:'}
                    styleCustomTitle={style.infoTitle}
                    styleCustomLabel={style.infoLabel}
                    styleCustomContainer={style.infoContainer}
                  />
                </div>
                <div className={style.details}>
                  <div className={style.details_title}>Реквизиты</div>
                  <span
                    className={style.details_info}><span>Открытое акционерное общество «Экзон»</span></span>
                  <span className={style.details_info}><span>УНП: </span>200433278</span>
                  <span className={style.details_info}><span>р/с: </span>BY47 AKBB 3012 1056 1001 5130 0000 в ЦБУ № 108 </span>
                  <span
                    className={style.details_info}>ОАО АСБ «Беларусбанк» в г.Дрогичине, код<span> AKBBBY2X</span></span>
                </div>
              </div>
              <div className={style.mapContainer_map}>
                <iframe
                  src="https://yandex.ru/map-widget/v1/?um=constructor%3A626bfd4601fbe5836fcdcf0d9075002d05de3776b26d285cc36c51d1eff1eb67&amp;source=constructor"
                  width="890" height="600" frameBorder="0"></iframe>
              </div>
            </div>
            <div className={style.leadershipContainer}>
              <div className={style.leadership}>
                <div className={style.leadership_title}>Руководство</div>
                <div className={style.leadership_inner}>
                  {Array.isArray(contactLeaderships) && contactLeaderships.map((block) => (
                      <div key={block.id} className={style.liederSingleBlock}>
                        <InfoField
                          title={block.name}
                          label={block.position}
                          styleCustomTitle={style.infoTitleLieder}
                          styleCustomLabel={style.infoLabelLieder}
                          styleCustomContainer={style.infoContainer}
                        />
                        {block.contacts?.map((contact) =>
                          contact.type === 'phone'
                            ? <InfoField
                              key={contact.id}
                              icon={<Phone/>}
                              title={contact.value}
                              tel={contact.value}
                              type={"phone"}
                              label={contact.label}
                              styleCustomIcon={style.infoIconDesc}
                              styleCustomTitle={style.infoTitlePhoneDesc}
                              styleCustomLabel={style.infoLabelDesc}
                              styleCustomContainer={style.infoContainer}
                            />
                            : <InfoField
                              key={contact.id}
                              icon={<Mail/>}
                              title={contact.value}
                              mail={contact.value}
                              type={'mail'}
                              label={'Электронная почта'}
                              styleCustomIcon={style.infoIconDesc}
                              styleCustomTitle={style.infoTitlePhoneDesc}
                              styleCustomLabel={style.infoLabelDesc}
                              styleCustomContainer={style.infoContainer}
                            />
                        )}
                      </div>
                    )
                  )}
                </div>
              </div>
              <div className={style.otherContacts}>
                {Array.isArray(contactOther) && contactOther.map((contactBlocks) =>
                  <div className={style.contactBlock} key={contactBlocks.id}>
                    <div className={style.titleDepartment}>{contactBlocks.title}</div>
                    {contactBlocks.phones.map((el) => (
                      el.type === 'phone'
                        ? <InfoField
                          key={el.id}
                          icon={<Phone/>}
                          title={el.value}
                          tel={el.value}
                          type={"phone"}
                          label={el.label}
                          styleCustomIcon={style.infoIconDesc}
                          styleCustomTitle={style.infoTitlePhoneDesc}
                          styleCustomLabel={style.infoLabelDesc}
                          styleCustomContainer={style.infoContainer}
                        />
                        : <InfoField
                          key={el.id}
                          icon={<Mail/>}
                          title={el.value}
                          mail={el.value}
                          type={'mail'}
                          label={el.label}
                          styleCustomIcon={style.infoIconDesc}
                          styleCustomTitle={style.infoTitlePhoneDesc}
                          styleCustomLabel={style.infoLabelDesc}
                          styleCustomContainer={style.infoContainer}
                        />
                    ))}
                  </div>
                )}


              </div>
            </div>
          </div>
        </div>
        <div className={style.bottomBlock}>
          <div className={style.bottomBlock_left}>
            Республиканское унитарное предприятие «Управляющая компания холдинга «Белфармпром»
          </div>
          <div className={style.bottomBlock_right}>
            <InfoField
              icon={<LocationSmall/>}
              title={'г. Минск, пр. Машерова, 10'}
              label={'Находимся по адресу'}
              styleCustomIcon={style.infoIconBottom}
              styleCustomTitle={style.infoTitleBottom}
              styleCustomLabel={style.infoLabelBottom}
              styleCustomContainer={style.infoContainer}
            />
            <InfoField
              icon={<Mail/>}
              title={'belpharmprom@belpharmprom.by'}
              mail={'belpharmprom@belpharmprom.by'}
              type={'mail'}
              label={'E-mail'}
              styleCustomIcon={style.infoIconBottom}
              styleCustomTitle={style.infoTitlePhoneBottom}
              styleCustomLabel={style.infoLabelBottom}
              styleCustomContainer={style.infoContainer}
            />
            <InfoField
              icon={<Phone/>}
              title={'+375 17 395 50 32'}
              tel={'+375 17 395 50 32'}
              type={'phone'}
              label={'Номер телефона'}
              styleCustomIcon={style.infoIconBottom}
              styleCustomTitle={style.infoTitlePhoneBottom}
              styleCustomLabel={style.infoLabelBottom}
              styleCustomContainer={style.infoContainer}
            />
            <InfoField
              icon={<Web/>}
              title={'belpharmprom.by'}
              link={'https://belpharmprom.by/'}
              label={'Официальный сайт'}
              type={'link'}
              styleCustomIcon={style.infoIconBottom}
              styleCustomTitle={style.infoTitlePhoneBottom}
              styleCustomLabel={style.infoLabelBottom}
              styleCustomContainer={style.infoContainer}
            />
          </div>
        </div>
      </div>
    </MainContainer>
  );
};

export default Contacts;

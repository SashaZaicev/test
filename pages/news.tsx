import React, {useCallback, useEffect, useState} from 'react';
import Breadcrumbs from "nextjs-breadcrumbs";
import cn from "classnames";
import ReactPaginate from "react-paginate";
import useTranslation from "next-translate/useTranslation";

import style from "../styles/NewsPage.module.scss";
import MainContainer from "../components/MainContainer";
import NewsBlock from "../components/NewsBlock";
import Heading from "../components/Heading";
import ButtonGroup from "../components/ButtonGroup/ButtonGroup";
import Button from "../components/Button";
import MailingList from "../components/MailingList/MailingList";
import {getNewsArray} from "../store/ducks/news/thunks";
import {useAppDispatch, useAppSelector} from "../store/hooks";
import {selectNewsList, selectSubscribe} from "../store/ducks/news/selectors";
import {Modal} from "../components/Modal/Modal";
import {setSubscribe} from "../store/ducks/news/reducer";

const items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];

enum ButtonsType {
  allNews = 1,
  consumers = 2,
  shareholders = 3,
}

const buttons = [
  {
    title: 'allNews',
    type: ButtonsType.allNews
  },
  {
    title: 'consumers',
    type: ButtonsType.consumers
  },
  {
    title: 'shareholders',
    type: ButtonsType.shareholders
  },
];

const News = () => {
  const {t} = useTranslation('news')
  const dispatch = useAppDispatch()
  const [btnActive, setBtnActive] = useState<ButtonsType>(ButtonsType.allNews)
  const newsArray = useAppSelector(selectNewsList);
  const subscribeSuc = useAppSelector(selectSubscribe)

  const itemsPerPage = 2
  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(items.slice(itemOffset, endOffset));
    setPageCount(Math.ceil(items.length / itemsPerPage));
  }, [itemOffset, itemsPerPage]);
  useEffect(() => {
    dispatch(getNewsArray())
  }, [dispatch])

  const handlePageClick = (event) => {
    const newOffset = event.selected * itemsPerPage % items.length;
    setItemOffset(newOffset);
  };
  const handleCloseIsOpenCallBack = useCallback(() => {
    dispatch(setSubscribe(false))
  }, [dispatch])

  console.log('subscribeSuc', subscribeSuc)
  return (
    <MainContainer title="Новости компании | ekzon.by">
      <div className='breadcrumbsContainer'>
        <Breadcrumbs useDefaultStyle={false}
                     activeItemClassName="activeBreadcrumbs"
                     inactiveItemClassName="noActiveBreadcrumbs"
                     listClassName="listClassName"
                     rootLabel={t('news:home')}
                     transformLabel={(title) => t(`${title}`)}
        />
      </div>

      <div className={style.newsContainer}>
        <div className={style.container}>
          <div className={style.headerBox}>
            <Heading className={style.styleHeading} text="Новости компании"
                     type="primary"/>
            <ButtonGroup styleContainer={style.styleContainerBtnGroup}>
              <Button onClickFunc={() => {
                setBtnActive(1)
              }}
                      className={cn(style.filterBtn, 'btn', btnActive === 1 && style.filterBtnActive)}
                      type={"none"}
              >
                Все новости
              </Button>
              <Button onClickFunc={() => {
                setBtnActive(2)
              }}
                      className={cn(style.filterBtn, 'btn', btnActive === 2 && style.filterBtnActive)}
                      type={"none"}>Потребителям</Button>
              <Button onClickFunc={() => {
                setBtnActive(3)
              }}
                      className={cn(style.filterBtn, 'btn', btnActive === 3 && style.filterBtnActive)}
                      type={"none"}>Акционерам</Button>
            </ButtonGroup>
          </div>

          <div className={style.newsContainer__inner}>
            <div className={style.top_gallery_news}>
              {Array.isArray(newsArray) && newsArray.map((newsItem) => (
                <NewsBlock
                  id={newsItem.id}
                  key={newsItem.id}
                  description={newsItem.description}
                  date={newsItem.date}
                  backImage={newsItem.imgUrl}
                  title={newsItem.title}
                  className={cn(style['blockSize'])}/>)
              )}
            </div>
            <div
              className={cn(style.paginationBlock, style.paginationBlockMobile)}>
              <ReactPaginate
                nextLabel=""
                onPageChange={handlePageClick}
                pageRangeDisplayed={2}
                marginPagesDisplayed={1}
                pageCount={pageCount}
                previousLabel=""
                pageClassName="page-item"
                pageLinkClassName={style.page_link}
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakLabel="..."
                breakClassName={style.breakDots}
                breakLinkClassName="page-link"
                containerClassName="pagination"
                activeClassName={style.active}
                renderOnZeroPageCount={null}
                className={style.pagination}
              />
            </div>
          </div>
        </div>
        <MailingList
          backImage={"/images/mailSend.jpg"}
          logoImage={"/images/mailingLogo.png"}
        />
        {subscribeSuc &&
          <Modal isOpen={subscribeSuc} close={handleCloseIsOpenCallBack} styleModalWindow={'modalSizeSuc'}>
            <div className={'sucMsg'}><span>Спасибо</span> Вы успешно подписались на рассылку!</div>
          </Modal>}
      </div>
    </MainContainer>
  );
};

export default News;

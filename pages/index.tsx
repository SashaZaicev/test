import type { NextPage } from 'next'
import MainContainer from '../components/MainContainer'
import MainScreen from '../components/MainScreen'

const Home: NextPage = () => (
  <MainContainer title="ОАО «Экзон» | ekzon.by">
    <MainScreen />
  </MainContainer>
)

export default Home

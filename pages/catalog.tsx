import React, {useEffect, useState} from 'react';
import ReactPaginate from 'react-paginate';
import Breadcrumbs from 'nextjs-breadcrumbs';
// import {GetStaticProps} from "next";
import {useRouter} from "next/router";
import useTranslation from "next-translate/useTranslation";
import cn from "classnames";

import style from '/styles/Catalog.module.scss'
import Heading from "../components/Heading";
import MainContainer from "../components/MainContainer";
import SurveyAnketaBlock from "../components/SurveyAnketaBlock";
import Accordion from "../components/Accordion/Accordion";
import Button from "../components/Button";
import SearchField from "../components/SearchField/SearchField";
import Test from "../public/images/test.svg";
import Filter from '../public/icons/filter.svg'
import Close from '../public/icons/close.svg'
import CloseGradient from '../public/icons/closeMenu.svg'
import {getCatalog} from "../store/ducks/catalog/thunks";
import {GetServerSideProps} from "next";
import {wrapper} from "../store";
import {useAppDispatch, useAppSelector} from "../store/hooks";
import {selectCatalogList} from "../store/ducks/catalog/selectors";
import CatalogList from "../components/CatalogList";

const items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];

const Catalog = () => {
  const router = useRouter()
  const dispatch = useAppDispatch()
  console.log('router', router.query)
  const {t} = useTranslation('catalog')
  const itemsPerPage = 2
  // We start with an empty list of items.
  const [currentItems, setCurrentItems] = useState(null);
  const [filterPage, setFilterPage] = useState(false);
  const [pageCount, setPageCount] = useState(0);
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);

  const catalogArray = useAppSelector(selectCatalogList);

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(items.slice(itemOffset, endOffset));
    setPageCount(Math.ceil(items.length / itemsPerPage));
  }, [itemOffset, itemsPerPage]);

  useEffect(() => {
    dispatch(getCatalog())
  }, [dispatch])

  const handlePageClick = (event) => {
    const newOffset = event.selected * itemsPerPage % items.length;
    setItemOffset(newOffset);
  };

  const handleClick = () => {
    setFilterPage(!filterPage)
    if (!filterPage) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflowY = 'scroll'
    }
  }

  return (
    <MainContainer title="Каталог продукции | ekzon.by">
      <div className='breadcrumbsContainer'>
        <Breadcrumbs useDefaultStyle={false}
                     activeItemClassName="activeBreadcrumbs"
                     inactiveItemClassName="noActiveBreadcrumbs"
                     listClassName="listClassName"
                     rootLabel={t('catalog:home')}
                     transformLabel={(title) => t(`${title}`)}
        />
      </div>
      <div className={style.catalogContainer}>
        <div className={style.container}>
          <Heading text="Каталог продукции" type="primary"/>
          <div className={style['catalogContainer-inner']}>
            <div className={style.block}>
              <div className={cn(style.block_sortingMenu, style.none)}>
                <Accordion />
                <Button className={style.btnSizeFirst} icon={<Filter/>}>Фильтровать товары</Button>
                <Button className={style.btnSize} type={"none"} icon={<Close/>}>Сбросить
                  фильтры</Button>
              </div>
              <div className={cn(style.mobileMenuBlock, filterPage && style.active)}>
                <div className={cn(style.mobileMenu)}>
                  <Button onClickFunc={() => handleClick()}
                          className={cn(style.btnSize, style.closeMobileSize)} type={"none"}
                          icon={<CloseGradient/>}/>
                  <div className={style.title}>Фильтрация товаров</div>
                  <Accordion/>
                  <Button className={style.btnSizeFirst} icon={<Filter/>}>Фильтровать товары</Button>
                  <Button className={style.btnSize} type={"none"} icon={<Close/>}>Сбросить
                    фильтры</Button>
                </div>
              </div>
              <div className={style.block_catalog}>
                <SearchField classNames={style.searchSize}/>
                <Button onClickFunc={() => handleClick()}
                        className={cn(style.btnSizeFirst, style.mobileBtnNone)}
                        icon={<Filter/>}>Фильрация товаров</Button>
                <div className={style.catalogList}>
                  <CatalogList catalog={catalogArray}/>
                </div>
                <div className={style.paginationBlock}>
                  <ReactPaginate
                    nextLabel=""
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={3}
                    marginPagesDisplayed={2}
                    pageCount={pageCount}
                    previousLabel=""
                    pageClassName="page-item"
                    pageLinkClassName={style.page_link}
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakLabel="..."
                    breakClassName={style.breakDots}
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName={style.active}
                    renderOnZeroPageCount={null}
                    className={style.pagination}
                  />
                </div>
                <div className={cn(style.paginationBlock, style.paginationBlockMobile)}>
                  <ReactPaginate
                    nextLabel=""
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={2}
                    marginPagesDisplayed={1}
                    pageCount={pageCount}
                    previousLabel=""
                    pageClassName="page-item"
                    pageLinkClassName={style.page_link}
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakLabel="..."
                    breakClassName={style.breakDots}
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName={style.active}
                    renderOnZeroPageCount={null}
                    className={style.pagination}
                  />
                </div>
              </div>
            </div>
            <SurveyAnketaBlock icon={<Test/>}
                               text=" в опросе, чтобы помочь нам совершенствоваться"
                               colorText="Примите участие"
                               backImage={"/images/anketav4.png"}
            />
          </div>
        </div>
      </div>
    </MainContainer>
  );
};
export const getServerSideProps: GetServerSideProps =
  wrapper.getServerSideProps(({dispatch}) => async (ctx) => {
    const catalogList = dispatch(getCatalog())
    console.log('catalogList', catalogList)

    return {props: {}};
  })
export default Catalog;


// export const getStaticProps: GetStaticProps = async () => {
//     // const start = +page === 1 ? 0 : (+page - 1) * 3
//     const URI = 'http://localhost:3000';
//     const response = await fetch(`${URI}/api/catalog/`);
//     // const response = await fetch(`http://localhost:3000/api/catalog?_limit=3_start=${start}`);
//     debugger
//     const data = await response.json();
//
//     if (!data) {
//         return {
//             notFound: true,
//         }
//     }
//
//     return {
//         props: {
//             catalog: data,
//             // page: +page
//         },
//     }
// };

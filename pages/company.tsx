import React, {useState} from 'react';
import useTranslation from "next-translate/useTranslation";
import Breadcrumbs from "nextjs-breadcrumbs";
import style from "../styles/Company.module.scss";
import Heading from "../components/Heading";
import MainContainer from "../components/MainContainer";
import SurveyAnketaBlock from "../components/SurveyAnketaBlock";
import Test from "../public/images/test.svg";
import SingleProductionAboutBlock from "../components/SingleProductionAboutBlock/SingleProductionAboutBlock";
import cn from "classnames";
import {Swiper, SwiperSlide} from 'swiper/react'
// Import Swiper styles
import 'swiper/css';
import PartnerShipSlide from "../components/PartnerShipSlide/PartnerShipSlide";
import {Pagination} from 'swiper';

const companyArray = {
  eventCompany: [
    {
      id: 1,
      title: "24 августа 1995 г.",
      descr: " на основании приказа № 45 Комитета по фармацевтической и микробиологической промышленности при Совете Министров РБ запатентован товарный знак и присвоено название «Экзон».",
    },
    {
      id: 2,
      title: "В 1996 г.",
      descr: " Правительством принимается решение о реструктуризации строящегося завода. ",
    },
    {
      id: 3,
      title: "В этом же 1996 году",
      descr: " было освоено производство первого вида продукции - Гематоген детский. Осенью 1997 года введена в строй линия производства сиропов и начат выпуск сиропа шиповника.",
    },
    {
      id: 4,
      title: "В апреле 2002 года",
      descr: " в связи с экономической целесообразностью произошло разделение нашего предприятия на две самостоятельные единицы: РУП «Экзон» и РУПП «Экзон - Глюкоза».",
    },
    {
      id: 5,
      title: "В декабре 2010 года ",
      descr: "Республиканское унитарное предприятие «Экзон» было преобразовано в Открытое аккционерное общество  «Экзон»",
    },
  ],
  productionHistory: [
    {
      id: 1,
      icon: '/icons/company/chocolate.svg',
      title: '21 000 000',
      description: 'Плиток гематогена',
    },
    {
      id: 2,
      icon: '/icons/company/tablets.svg',
      title: '5 500 000',
      description: 'Упаковок таблеток',
    },
    {
      id: 3,
      icon: '/icons/company/medicine-bottle.svg',
      title: '1 000 000',
      description: 'Флаконов сиропов',
    },
    {
      id: 4,
      icon: '/icons/company/package.svg',
      title: '1 500 000',
      description: 'Упаковок порошков и гранул',
    },
    {
      id: 5,
      icon: '/icons/company/herbal-medicine.svg',
      title: '300 000',
      description: 'Прочая продукция',
    },
  ],
  partnerShips: [
    {
      id: 1,
      image: "/images/partnerships/1.png",
    },
    {
      id: 2,
      image: "/images/partnerships/2.png",
    },
    {
      id: 3,
      image: "/images/partnerships/3.png",
    },
    {
      id: 4,
      image: "/images/partnerships/4.png",
    },
    {
      id: 5,
      image: "/images/partnerships/5.png",
    },
    {
      id: 6,
      image: "/images/partnerships/6.png",
    },
    {
      id: 7,
      image: "/images/partnerships/7.png",
    },
    {
      id: 8,
      image: "/images/partnerships/8.png",
    },
    {
      id: 9,
      image: "/images/partnerships/4.png",
    },
    {
      id: 10,
      image: "/images/partnerships/5.png",
    },
    {
      id: 11,
      image: "/images/partnerships/6.png",
    },
    {
      id: 12,
      image: "/images/partnerships/7.png",
    },
    {
      id: 13,
      image: "/images/partnerships/8.png",
    },
  ]
}
const Company = () => {
  const {t} = useTranslation('company')
  const [show, setShow] = useState(false)

  return (
    <MainContainer title="О компании  | ekzon.by">
      <div className='breadcrumbsContainer'>
        <Breadcrumbs useDefaultStyle={false}
                     activeItemClassName="activeBreadcrumbs"
                     inactiveItemClassName="noActiveBreadcrumbs"
                     listClassName="listClassName"
                     rootLabel={t('company:home')}
                     transformLabel={(title) => t(`${title}`)}
        />
      </div>

      <div className={style.companyContainer}>
        <div className={style.container}>
          <div className={style.headerBox}>
            <div className={style.left}>
              <Heading className={style.styleHeading} text="О компании" type="primary"/>
              <p>Открытое акционерное общество «Экзон» работает на отечественном фармацевтическом рынке с
                1995
                года. Предприятие производит продукцию, отвечающую всем стандартам качества. Коллектив
                предприятия заинтересован в сотрудничестве с отечественными и зарубежными партнерами.
              </p>
              <p>Предприятие производит продукцию европейского качества, используя высококачественное
                сырье.
                Вся продукция, производимая предприятием, зарегистрирована Министерством здравоохранения
                Республики Беларусь.
              </p>
              <p>
                На предприятии действует собственная служба обеспечения и контроля качества.
                Высококвалифицированный персонал выполняет работы по входному, промежуточному контролю
                сырья
                и материалов, а также осуществляет в ходе техпроцесса контроль качества готовой
                продукции,
                контролирует хранение и реализацию.
              </p>
            </div>
            <div className={style.right}>
              <img className={style.mainImage} src="/images/aboutCompany1.png" alt="about company image"/>
              <img className={style.topCircle} src="/images/aboutCompany2.png"
                   alt="about company second image"/>
              <img className={style.bottomCircle} src="/images/aboutCompany2.png"
                   alt="about company third image"/>
            </div>

          </div>
          <div className={style.companyContainer__inner}>
            <div className={style.about}>
              <div className={style.left}>
                <div className={style.left_title}>
                  В 80-х годах медицинская промышленность СССР, ветеринария и сельское хозяйство
                  испытывали недостаток в биохимических препаратах.
                </div>
                <div className={style.left_description}>
                  <p>Решением правительства бывшего СССР в г. Дрогичине Брестской области в 1986 году
                    началось строительство завода медицинских препаратов и изделий, получившего
                    название Биохимический завод. Завод предназначался для выпуска антибиотиков и
                    биологических средств для защиты растений. В составе завода предусматривалось
                    строительство ремонтно - механического производства союзного значения для нужд
                    медицинской промышленности.
                  </p>
                  <p>
                    Строительство продолжалось до 1990 г., затем было принято решение о
                    перепрофилировании завода на выпуск антибиотиков и витаминов.
                  </p>
                </div>

              </div>
              <ul className={cn(style.right, style.timeLineContainer)}>
                <div className={style.timeLineContainer_gradient}>
                  {companyArray.eventCompany.map((event) => (
                    <li key={event.id}>{event.title}<span>{event.descr}</span></li>
                  ))}
                </div>
              </ul>
            </div>
            <div className={style.production}>
              <div className={style.production_title}>2020</div>
              <div className={style.production_subTitle}>
                <span>В 2020 году</span>
                <br/>
                мы произвели:
              </div>
              <div className={style.production_productionHistoryBlock}>
                {companyArray.productionHistory.map((plate) => (
                    <SingleProductionAboutBlock className={style.singleProductionSize} key={plate.id} image={plate.icon} title={plate.title}
                                                description={plate.description}/>
                  )
                )}
              </div>
            </div>
            <div className={style.partnershipsContainer}>
              <Heading className={style.styleHeading} text="Наши партнеры" type="secondary"/>
              <Swiper
                modules={[Pagination]}
                slidesPerView={8}
                spaceBetween={86}
                loop
                autoplay
                pagination={{
                  clickable: true
                }}
                breakpoints={{
                  320: {
                    slidesPerView: 1,
                    spaceBetween: 30,
                    centeredSlides: true
                  },
                  671: {
                    slidesPerView: 2.5,
                    spaceBetween: 30,
                    centeredSlides: true
                  },
                  867: {
                    slidesPerView: 4,
                  },
                  1025: {
                    slidesPerView: 6,
                  },
                  1285: {
                    slidesPerView: 7,
                  }
                }}
                className={cn(style.swiperMain, 'partnership-slider')}
              >
                {companyArray.partnerShips.map((partner) => (
                  <SwiperSlide key={partner.id} className={style.swiper_wrapper}>
                    <PartnerShipSlide image={partner.image}/>
                  </SwiperSlide>
                ))}
              </Swiper>
            </div>
            <div className={style.anotherBlock}>
              <div className={style.anotherBlock_left}>
                <div className={style.anotherBlock_left_title}>
                  Политика в области качества
                </div>
                <div className={style.anotherBlock_left_description}>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae bibendum aliquam
                    varius egestas pellentesque elementum dapibus vel. Morbi quisque gravida aliquet
                    cras pulvinar arcu diam. Ut vestibulum faucibus proin diam a suspendisse donec
                    nibh egestas. Vitae in et sit lobortis ultrices. Arcu, sodales sit commodo,
                    nisi, consectetur rhoncus. Vitae elementum urna eget pharetra, volutpat nunc a
                    porttitor. Imperdiet id magnis risus ut est luctus faucibus. Ac sed aliquet
                    aliquam quis ut pretium morbi faucibus. Magna sapien tristique vulputate eu.
                    Diam convallis elementum vel malesuada amet ligula amet. Orci sed at enim amet
                    facilisi dui, consequat nam. Netus nisl massa malesuada volutpat.
                  </p>
                  <p> Id aliquet massa pharetra, nibh imperdiet duis mi. Lectus sed diam tincidunt
                    molestie. Dolor sagittis nibh non etiam tortor, tristique. Pulvinar viverra odio
                    nullam dignissim ut id posuere scelerisque est. Diam tortor elit id amet,
                    tristique
                    molestie ornare praesent porttitor. Commodo suscipit quis accumsan vulputate.
                    Orci
                    eget non.
                  </p>
                </div>
                <div className={style.other_block}>
                  <button
                    type="button"
                    className={style.other_functions}
                    onClick={() => {
                      setShow(!show)
                    }}
                  >
                    {!show ? 'Читать полностью' : 'Скрыть текст'}
                  </button>
                  <svg width="8" height="4" viewBox="0 0 8 4" fill="none"
                       xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd"
                          d="M0.116797 0.132968C0.0417575 0.204802 -1.61718e-07 0.300084 -1.57388e-07 0.399148C-1.53057e-07 0.498212 0.0417576 0.593494 0.116797 0.665328L3.42246 3.77915C3.49533 3.84885 3.58288 3.90436 3.68008 3.94228C3.77728 3.98021 3.88212 3.99976 3.98789 3.99976C4.09366 3.99976 4.19831 3.98021 4.29551 3.94228C4.39271 3.90436 4.48026 3.84885 4.55313 3.77915L7.8832 0.642789C8.0372 0.497334 8.03911 0.261694 7.88711 0.114058C7.8509 0.0783144 7.80708 0.0497219 7.7582 0.0300514C7.70933 0.0103809 7.65658 5.23558e-05 7.60313 -0.000288819C7.54967 -0.000629755 7.49664 0.00902 7.44746 0.0280649C7.39828 0.0471098 7.35395 0.0751402 7.31719 0.110418L4.27031 2.9806C4.23388 3.01548 4.19002 3.04325 4.14141 3.06223C4.09279 3.08121 4.04061 3.09101 3.9877 3.09101C3.93478 3.09101 3.88241 3.08121 3.83379 3.06223C3.78517 3.04325 3.74132 3.01548 3.70488 2.9806L0.682031 0.133334C0.645631 0.0984652 0.601709 0.0707001 0.553125 0.0517246C0.50454 0.0327494 0.452293 0.0229604 0.399414 0.0229604C0.346535 0.0229604 0.294288 0.0327494 0.245703 0.0517247C0.197118 0.0707002 0.153197 0.0984652 0.116797 0.133334"
                          fill="url(#paint0_linear_709_67)"/>
                    <defs>
                      <linearGradient id="paint0_linear_709_67" x1="3.99994" y1="3.99976"
                                      x2="3.99994" y2="-0.000297245"
                                      gradientUnits="userSpaceOnUse">
                        <stop stopColor="#006B54"/>
                        <stop offset="1" stopColor="#18AD89"/>
                      </linearGradient>
                    </defs>
                  </svg>

                </div>
              </div>
              <div className={style.anotherBlock_right}>
                <div className={style.anotherBlock_right_title}>
                  Участки производства
                </div>
                <div className={style.anotherBlock_right_description}>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae bibendum aliquam
                    varius egestas pellentesque elementum dapibus vel. Morbi quisque gravida aliquet
                    cras pulvinar arcu diam. Ut vestibulum faucibus proin diam a suspendisse donec
                    nibh egestas. Vitae in et sit lobortis ultrices. Arcu, sodales sit commodo,
                    nisi, consectetur rhoncus. Vitae elementum urna eget pharetra, volutpat nunc a
                    porttitor. Imperdiet id magnis risus ut est luctus faucibus. Ac sed aliquet
                    aliquam quis ut pretium morbi faucibus. Magna sapien tristique vulputate eu.
                    Diam convallis elementum vel malesuada amet ligula amet. Orci sed at enim amet
                    facilisi dui, consequat nam. Netus nisl massa malesuada volutpat.
                  </p>
                  <p> Id aliquet massa pharetra, nibh imperdiet duis mi. Lectus sed diam tincidunt
                    molestie. Dolor sagittis nibh non etiam tortor, tristique. Pulvinar viverra odio
                    nullam dignissim ut id posuere scelerisque est. Diam tortor elit id amet,
                    tristique
                    molestie ornare praesent porttitor. Commodo suscipit quis accumsan vulputate.
                    Orci
                    eget non.
                  </p>
                </div>
                <div className={style.other_block}>
                  <button
                    type="button"
                    className={style.other_functions}
                    onClick={() => {
                      setShow(!show)
                    }}
                  >
                    {!show ? 'Читать полностью' : 'Скрыть текст'}
                  </button>
                  <svg width="8" height="4" viewBox="0 0 8 4" fill="none"
                       xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd"
                          d="M0.116797 0.132968C0.0417575 0.204802 -1.61718e-07 0.300084 -1.57388e-07 0.399148C-1.53057e-07 0.498212 0.0417576 0.593494 0.116797 0.665328L3.42246 3.77915C3.49533 3.84885 3.58288 3.90436 3.68008 3.94228C3.77728 3.98021 3.88212 3.99976 3.98789 3.99976C4.09366 3.99976 4.19831 3.98021 4.29551 3.94228C4.39271 3.90436 4.48026 3.84885 4.55313 3.77915L7.8832 0.642789C8.0372 0.497334 8.03911 0.261694 7.88711 0.114058C7.8509 0.0783144 7.80708 0.0497219 7.7582 0.0300514C7.70933 0.0103809 7.65658 5.23558e-05 7.60313 -0.000288819C7.54967 -0.000629755 7.49664 0.00902 7.44746 0.0280649C7.39828 0.0471098 7.35395 0.0751402 7.31719 0.110418L4.27031 2.9806C4.23388 3.01548 4.19002 3.04325 4.14141 3.06223C4.09279 3.08121 4.04061 3.09101 3.9877 3.09101C3.93478 3.09101 3.88241 3.08121 3.83379 3.06223C3.78517 3.04325 3.74132 3.01548 3.70488 2.9806L0.682031 0.133334C0.645631 0.0984652 0.601709 0.0707001 0.553125 0.0517246C0.50454 0.0327494 0.452293 0.0229604 0.399414 0.0229604C0.346535 0.0229604 0.294288 0.0327494 0.245703 0.0517247C0.197118 0.0707002 0.153197 0.0984652 0.116797 0.133334"
                          fill="url(#paint0_linear_709_67)"/>
                    <defs>
                      <linearGradient id="paint0_linear_709_67" x1="3.99994" y1="3.99976"
                                      x2="3.99994" y2="-0.000297245"
                                      gradientUnits="userSpaceOnUse">
                        <stop stopColor="#006B54"/>
                        <stop offset="1" stopColor="#18AD89"/>
                      </linearGradient>
                    </defs>
                  </svg>
                </div>
              </div>
            </div>
          </div>
          <SurveyAnketaBlock icon={<Test/>}
                             text=" в опросе, чтобы помочь нам совершенствоваться"
                             colorText="Примите участие"
                             backImage={"/images/anketav4.png"}
          />
        </div>
      </div>
    </MainContainer>

  )
    ;
};

export default Company;

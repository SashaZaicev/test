import type { AppProps } from 'next/app'
import {FC} from "react";
import '../styles/globals.scss'

import {wrapper} from "../store";

const WrappedApp: FC<AppProps> = ({ Component, pageProps}) =>
  <Component {...pageProps} />;

export default wrapper.withRedux(WrappedApp);

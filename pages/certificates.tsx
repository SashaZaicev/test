import Breadcrumbs from "nextjs-breadcrumbs";
import React, {useEffect} from "react";
import useTranslation from "next-translate/useTranslation";
import Image from "next/image";

import style from "../styles/Certificates.module.scss";
import Test from "../public/images/test.svg";
import MainContainer from "../components/MainContainer/MainContainer";
import SurveyAnketaBlock from "../components/SurveyAnketaBlock";
import Heading from "../components/Heading";
import HeadingIcon from "../components/Heading/HeadingIcon";
import {Fancybox} from "../components/FancyBox/Fancybox";
import {getCertificatesList} from "../store/ducks/certificates/thunks";
import {useAppDispatch, useAppSelector} from "../store/hooks";
import {selectCertificatesList} from "../store/ducks/certificates/selectors";

const CertificatesPage = () => {
  const {t} = useTranslation('breadcrumbs')
  const dispatch = useAppDispatch()
  const certificatesArray = useAppSelector(selectCertificatesList)

  useEffect(() => {
    dispatch(getCertificatesList())
  }, [dispatch])

  return (
    <MainContainer title="Сертификаты | ekzon.by">
      <div className='breadcrumbsContainer'>
        <Breadcrumbs useDefaultStyle={false}
                     activeItemClassName="activeBreadcrumbs"
                     inactiveItemClassName="noActiveBreadcrumbs"
                     listClassName="listClassName"
                     rootLabel={t('breadcrumbs:home')}
                     transformLabel={(title) => t(`${title}`)}
        />
      </div>
      <div className={style.certificatesContainer}>
        <div className={style.container}>
          <div className={style.headerBox}>
            <div className={style.iconHeader}>
              <HeadingIcon icon={<Image width='50' height='50' className={style.imgCertificates} src="/icons/certificate_ico.svg" alt="symbol"/>}/>
            </div>
            <Heading className={style.styleHeading} text="Сертификаты соответствия"
                     type="subPrimary"/>
          </div>
          <div className={style.fancyBlockCertificates}>
            <Fancybox options={{infinite: false}}>
              {Array.isArray(certificatesArray) && certificatesArray.map((image) => (
                <a className={style.image} key={image.id} data-fancybox="certificates"
                   href={image.imageURL}>
                  <div className={style.circle}>
                    <div className={style.gradient}>
                      <img alt="" src={image.imageURL}/>
                    </div>
                    <div className={style.box_eye}>
                      <div className={style.hover}>
                        <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M29.6161 24.1002L24.3838 18.8679C23.7333 18.2174 22.7038 18.1778 21.9967 18.7378L20.8598 17.6008C21.8553 16.017 22.438 14.156 22.438 12.1536C22.438 6.48002 17.8222 1.86426 12.1487 1.86426C6.47513 1.86426 1.85938 6.47436 1.85938 12.1479C1.85938 17.8214 6.47513 22.4372 12.1487 22.4372C14.1511 22.4372 16.0121 21.8546 17.5959 20.859L18.7329 21.996C18.1729 22.703 18.2125 23.7269 18.863 24.383L24.0953 29.6154C24.446 29.9661 24.9099 30.1414 25.3681 30.1414C25.8262 30.1414 26.2901 29.9661 26.6408 29.6154L29.6105 26.6457C29.9499 26.3063 30.1365 25.8538 30.1365 25.3729C30.1365 24.8921 29.9555 24.4396 29.6161 24.1002ZM2.99069 12.1479C2.99069 7.09658 7.09735 2.98991 12.1487 2.98991C17.2 2.98991 21.3066 7.09658 21.3066 12.1479C21.3066 17.1992 17.2 21.3059 12.1487 21.3059C7.09735 21.3059 2.99069 17.1992 2.99069 12.1479ZM20.198 18.5342L21.1822 19.5184L19.5192 21.1814L18.5349 20.1972C19.1515 19.7107 19.7115 19.1507 20.198 18.5342ZM28.8129 25.8424L25.8432 28.8121C25.583 29.0723 25.1588 29.0723 24.8986 28.8121L19.6662 23.5798C19.406 23.3196 19.406 22.8954 19.6662 22.6352L22.6359 19.6655C22.766 19.5354 22.9357 19.4675 23.1111 19.4675C23.2808 19.4675 23.4561 19.5354 23.5862 19.6655L28.8186 24.8978C28.943 25.0222 29.0165 25.1919 29.0165 25.3729C29.0165 25.554 28.943 25.718 28.8129 25.8424Z"
                            fill="url(#paint0_linear_261_4183)"/>
                          <path
                            d="M15.2551 11.5822H12.7153V9.04243C12.7153 8.73132 12.4607 8.47678 12.1496 8.47678C11.8385 8.47678 11.584 8.73132 11.584 9.04243V11.5822H9.04417C8.73306 11.5822 8.47852 11.8368 8.47852 12.1479C8.47852 12.459 8.73306 12.7135 9.04417 12.7135H11.584V15.2533C11.584 15.5645 11.8385 15.819 12.1496 15.819C12.4607 15.819 12.7153 15.5645 12.7153 15.2533V12.7135H15.2551C15.5662 12.7135 15.8207 12.459 15.8207 12.1479C15.8207 11.8368 15.5662 11.5822 15.2551 11.5822Z"
                            fill="url(#paint1_linear_261_4183)"/>
                          <defs>
                            <linearGradient id="paint0_linear_261_4183" x1="1.85938" y1="16.0028" x2="30.1365" y2="16.0028"
                                            gradientUnits="userSpaceOnUse">
                              <stop stopColor="#006B54"/>
                              <stop offset="1" stopColor="#18AD89"/>
                            </linearGradient>
                            <linearGradient id="paint1_linear_261_4183" x1="8.47852" y1="12.1479" x2="15.8207" y2="12.1479"
                                            gradientUnits="userSpaceOnUse">
                              <stop stopColor="#006B54"/>
                              <stop offset="1" stopColor="#18AD89"/>
                            </linearGradient>
                          </defs>
                        </svg>
                      </div>
                    </div>
                  </div>
                  <p className={style.descriptionImage}>{image.description}</p>
                </a>
              ))}
            </Fancybox>
          </div>
        </div>
        <SurveyAnketaBlock icon={<Test/>}
                           text=" в опросе, чтобы помочь нам совершенствоваться"
                           colorText="Примите участие"
                           backImage={"/images/anketav4.png"}
        />
      </div>
    </MainContainer>
  );
};

export default CertificatesPage;

import {createWrapper, HYDRATE} from "next-redux-wrapper";
import {
  Action,
  AnyAction,
  combineReducers,
  configureStore,
  ThunkAction,
} from "@reduxjs/toolkit";

import {catalogReducer} from "./ducks/catalog/reducer";
import {newsReducer} from "./ducks/news/reducer";
import {contactsReducer} from "./ducks/contacts/reducer";
import {commonReducer} from "./ducks/common/reducer";
import {videoReducer} from "./ducks/video/reducer";
import {certificatesReducer} from "./ducks/certificates/reducer";


const combinedReducer = combineReducers({
  catalog: catalogReducer,
  news: newsReducer,
  contacts: contactsReducer,
  common: commonReducer,
  video: videoReducer,
  certificates: certificatesReducer,
})

const reducer = (state: ReturnType<typeof combinedReducer>, action: AnyAction) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    };
    return nextState;
  } else {
    return rootReducer(state, action);
  }
};

const rootReducer = (state: any, action: any) => {
  let tmpState = state
  return combinedReducer(tmpState, action)
}

const makeStore = () => (
  configureStore({
    reducer,
  })
)

type Store = ReturnType<typeof makeStore>

export type AppDispatch = Store['dispatch'];

export type RootState = ReturnType<Store['getState']>
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
  RootState,
  unknown,
  Action<string>>;

// export an assembled wrapper
export const wrapper = createWrapper<Store>(makeStore, {debug: true});

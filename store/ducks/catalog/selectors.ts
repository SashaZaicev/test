import {RootState} from "../../index";
import {CatalogStateType} from "./reducer";

export const selectCatalog = (state: RootState): CatalogStateType => state.catalog
export const selectCatalogList = (state: RootState) => selectCatalog(state).catalogList

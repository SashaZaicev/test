import {createSlice} from "@reduxjs/toolkit";
import {CatalogType} from "../../../types/catalog";
import {HYDRATE} from "next-redux-wrapper";
// import {HYDRATE} from "next-redux-wrapper";

export const initialState = {
  catalogList: [] as CatalogType[],
}

export const catalogSlice = createSlice({
  name: 'catalog',
  initialState,
  reducers: {
    setCatalog(state, {payload}) {
      state.catalogList = payload
    },
  },
  // extraReducers: {
  //   [HYDRATE]: (state, action) => {
  //     console.log('HYDRATE', state, action.payload);
  //     return {
  //       ...state,
  //       ...action.payload,
  //     };
  //   },
  // },
})
export type CatalogStateType = typeof initialState
export const {
  setCatalog,
} = catalogSlice.actions
export const catalogReducer = catalogSlice.reducer


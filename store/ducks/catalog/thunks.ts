import {setCatalog} from "./reducer";
import {catalogApi} from "../../../pages/api/catalog";
import {AppDispatch} from "../../index";

export const getCatalog = () => async (dispatch: AppDispatch) => {
  try {
    const result = catalogApi.getCatalog()
    dispatch(setCatalog(result))
  } catch (err) {
    console.error(err)
  }
}

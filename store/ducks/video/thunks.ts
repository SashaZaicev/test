import {setVideo} from "./reducer";
import {AppDispatch} from "../../index";
import {videoApi} from "../../../pages/api/video";

export const getVideoList = () => async (dispatch: AppDispatch) => {
  try {
    const result = videoApi.getVideo()
    dispatch(setVideo(result))
  } catch (err) {
    console.error(err)
  }
}

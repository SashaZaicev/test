import {RootState} from "../../index";
import {VideoStateType} from "./reducer";

export const selectVideo = (state: RootState): VideoStateType => state.video
export const selectVideoList = (state: RootState) => selectVideo(state).videoList

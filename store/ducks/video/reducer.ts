import {createSlice} from "@reduxjs/toolkit";
import {HYDRATE} from "next-redux-wrapper";
import {VideoType} from "../../../types/video";
// import {HYDRATE} from "next-redux-wrapper";

export const initialState = {
  videoList: [] as VideoType[],
}

export const videoSlice = createSlice({
  name: 'video',
  initialState,
  reducers: {
    setVideo(state, {payload}) {
      state.videoList = payload
    },
  },
  // extraReducers: {
  //   [HYDRATE]: (state, action) => {
  //     console.log('HYDRATE', state, action.payload);
  //     return {
  //       ...state,
  //       ...action.payload,
  //     };
  //   },
  // },
})
export type VideoStateType = typeof initialState
export const {
  setVideo,
} = videoSlice.actions
export const videoReducer = videoSlice.reducer


import {RootState} from "../../index";
import {ContactsStateType} from "./reducer";

export const contactsNews = (state: RootState): ContactsStateType => state.contacts
export const selectLeadershipsList = (state: RootState) => contactsNews(state).contactLeaderships
export const selectOtherList = (state: RootState) => contactsNews(state).contactsOther

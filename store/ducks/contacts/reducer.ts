import {createSlice} from "@reduxjs/toolkit";
import {ContactsLeadershipsType, ContactsOtherType} from "../../../types/contacts";

export const initialState = {
  contactLeaderships: [] as ContactsLeadershipsType[],
  contactsOther: [] as ContactsOtherType[],
}

export const contactsSlice = createSlice({
  name: 'contacts',
  initialState,
  reducers: {
    setContactLeaderships(state, {payload}) {
      state.contactLeaderships = payload
    },
    setContactsOther(state, {payload}) {
      state.contactsOther = payload
    },
  },
})
export type ContactsStateType = typeof initialState
export const {
  setContactLeaderships,
  setContactsOther
} = contactsSlice.actions
export const contactsReducer = contactsSlice.reducer


import {setContactLeaderships, setContactsOther} from "./reducer";

import {AppDispatch} from "../../index";
import {contactsApi} from "../../../pages/api/contacts";

export const getContactLeadershipsList = () => async (dispatch: AppDispatch) => {
  try {
    const result = contactsApi.getContactLeaderships()
    dispatch(setContactLeaderships(result))
  } catch (err) {
    console.error(err)
  }
}
export const getContactsOtherList = () => async (dispatch: AppDispatch) => {
  try {
    const result = contactsApi.getContactsOther()
    dispatch(setContactsOther(result))
  } catch (err) {
    console.error(err)
  }
}

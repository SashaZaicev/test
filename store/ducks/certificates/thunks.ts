import {setCertificates} from "./reducer";
import {AppDispatch} from "../../index";
import {certificatesApi} from "../../../pages/api/certificates";

export const getCertificatesList = () => async (dispatch: AppDispatch) => {
  try {
    const result = certificatesApi.getCertificates()
    dispatch(setCertificates(result))
  } catch (err) {
    console.error(err)
  }
}

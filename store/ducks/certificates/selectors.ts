import {RootState} from "../../index";
import {CertificatesStateType} from "./reducer";

export const selectCertificates = (state: RootState): CertificatesStateType => state.certificates
export const selectCertificatesList = (state: RootState) => selectCertificates(state).certificatesList

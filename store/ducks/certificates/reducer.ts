import {createSlice} from "@reduxjs/toolkit";
import {HYDRATE} from "next-redux-wrapper";
import {VideoType} from "../../../types/video";
// import {HYDRATE} from "next-redux-wrapper";

export const initialState = {
  certificatesList: [] as VideoType[],
}

export const certificatesSlice = createSlice({
  name: 'certificates',
  initialState,
  reducers: {
    setCertificates(state, {payload}) {
      state.certificatesList = payload
    },
  },
  // extraReducers: {
  //   [HYDRATE]: (state, action) => {
  //     console.log('HYDRATE', state, action.payload);
  //     return {
  //       ...state,
  //       ...action.payload,
  //     };
  //   },
  // },
})
export type CertificatesStateType = typeof initialState
export const {
  setCertificates,
} = certificatesSlice.actions
export const certificatesReducer = certificatesSlice.reducer


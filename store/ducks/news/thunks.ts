import {setNews, setSubscribe} from "./reducer";

import {AppDispatch} from "../../index";
import {newsApi} from "../../../pages/api/news";

export const getNewsArray = () => async (dispatch: AppDispatch) => {
  try {
    const result = newsApi.getNews()
    dispatch(setNews(result))
  } catch (err) {
    console.error(err)
  }
}
export const postMailSubscribe = (mail: string) => async (dispatch: AppDispatch) => {
  try {
    const result = newsApi.postMailSub(mail)
    dispatch(setSubscribe(result))
  } catch (err) {
    console.error(err)
  }
}

import {createSlice} from "@reduxjs/toolkit";
import {NewsType} from "../../../types/news";

export const initialState = {
  newsList: [] as NewsType[],
  mailSubscribe: false,
}

export const newsSlice = createSlice({
  name: 'news',
  initialState,
  reducers: {
    setNews(state, {payload}) {
      state.newsList = payload
    },
    setSubscribe(state, {payload}) {
      state.mailSubscribe = payload
    },
  },
})
export type NewsStateType = typeof initialState
export const {
  setNews,
  setSubscribe,
} = newsSlice.actions
export const newsReducer = newsSlice.reducer


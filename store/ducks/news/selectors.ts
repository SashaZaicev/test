import {RootState} from "../../index";
import {NewsStateType} from "./reducer";

export const selectNews = (state: RootState): NewsStateType => state.news
export const selectNewsList = (state: RootState) => selectNews(state).newsList
export const selectSubscribe = (state: RootState) => selectNews(state).mailSubscribe

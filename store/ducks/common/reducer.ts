import {createSlice} from "@reduxjs/toolkit";
import {useState} from "react";

export const initialState = {
  callback: false,
  quizInfo: false,
  quizBtn: false,
  callbackForm: false
}

export const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setCallback(state, {payload}) {
      state.callback = payload
    },
    setQuizInfo(state, {payload}) {
      state.quizInfo = payload
    },
    setQuizBtn(state, {payload}) {
      state.quizBtn = payload
    },
    setCallbackForm(state, {payload}) {
      state.callbackForm = payload
    },
  },
})
export type CommonStateType = typeof initialState
export const {
  setCallback,
  setQuizInfo,
  setQuizBtn,
  setCallbackForm,
} = commonSlice.actions
export const commonReducer = commonSlice.reducer


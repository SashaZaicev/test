import {RootState} from "../../index";
import {CommonStateType} from "./reducer";

export const selectCommon = (state: RootState): CommonStateType => state.common
export const selectCallbackSuc = (state: RootState) => selectCommon(state).callback
export const selectCallbackForm = (state: RootState) => selectCommon(state).callbackForm
export const selectQuizInfoSuc = (state: RootState) => selectCommon(state).quizInfo
export const selectQuizBtnSuc = (state: RootState) => selectCommon(state).quizBtn

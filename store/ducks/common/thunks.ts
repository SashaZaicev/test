import {AppDispatch} from "../../index";
import {setCallback, setQuizBtn, setQuizInfo} from "./reducer";
import {commonApi} from "../../../pages/api/common";
import {setCookie} from 'cookies-next';

export const setCallbackMsg = (fullName: string, phone: string) => async (dispatch: AppDispatch) => {
  try {
    const result = commonApi.postCallbackInfo(fullName, phone)
    dispatch(setCallback(result))
  } catch (err) {
    console.error(err)
  }
}
export const setQuizInfoList = (specialist: string, production: string, howFindProduct: string) => async (dispatch: AppDispatch) => {
  try {
    const result = commonApi.postQuizInfo(specialist, production, howFindProduct)
    dispatch(setQuizInfo(result))
    dispatch(setQuizBtn(result))
    setCookie('QuizInfo', {'specialist': specialist, 'production': production, 'howFindProduct': howFindProduct})
  } catch (err) {
    console.error(err)
  }
}

export type VideoType = {
  id: any,
  description: string,
  videoURL: string,
  imageURL: string,
}

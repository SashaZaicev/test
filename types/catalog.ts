export type CatalogType = {
  id: any,
  title: string,
  description: string,
  image: string,
  newItem: false,
  recipe: true,
  child: true,
}

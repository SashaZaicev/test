export type ContactsLeadershipsType = {
  id: any,
  name: string,
  position: string,
  contacts: contactType[],
}
type contactType = {
  id: any,
  type: string,
  label: string,
  value: string,
}

export type ContactsOtherType = {
  id: any,
  title: string,
  phones: phoneType[],
}
type phoneType = {
  id: any,
  type: string,
  label: string,
  value: string,
}

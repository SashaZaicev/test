export type CertificatesType = {
  id: any,
  description: string,
  videoURL: string,
  imageURL: string,
}

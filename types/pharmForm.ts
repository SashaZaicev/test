
type InputTypes = {
    type: 'input'
    placeholder: string
    label: string
    name: string
}
type PhoneTypes = {
    type: 'phone'
    placeholder: string
    label: string
    name: string
    mask: string
}

export type PharmaFormType = InputTypes | PhoneTypes

export type NewsType = {
  id: any,
  title: string,
  description: string,
  imgUrl: string,
  date: string,
}

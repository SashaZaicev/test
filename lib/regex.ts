export const phoneRegexp = /^(\+375)\((29|25|44|33)\)(\d{3})-(\d{2})-(\d{2})$/
// eslint-disable-next-line max-len
export const email = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
// отображение расширения файла
export const patt = /\.([0-9a-z]+)(?=[?#])|(\.)(?:[\w]+)$/gmi
